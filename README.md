# Mamut CO Backend

## Poetry (Package manager)
- Follow the instructions to install Poetry: https://python-poetry.org/docs/

### Create environment and install packages
- poetry install

### Update packages
- poetr``y update

### Activate environment````
- poetry shell

## Run the API

### Set an environment to load the configuration of the API
- Windows:
    - SET MAMUT_CONFIG_ENVIRONMENT=staging
- Linux:
    - export MAMUT_CONFIG_ENVIRONMENT=staging

### Run the API with hotreload
- uvicorn mamut.main:app --reload --host=0.0.0.0

## Run tests
- pytest

# Build docker image
docker build -t mamut-co-backend-image .

# Run API docker container
docker run -d -p 8000:80 -e MODULE_NAME="mamut.main" -e MAMUT_CONFIG_ENVIRONMENT="betoksoft" -e PGDB_URL="postgresql://host.docker.internal:5432/db_mamut_co?user=postgres&password=mypassword" -e REDIS_URL="redis://:mypassword@host.docker.internal:6379/0" mamut-co-backend-image

# Run rabbitmq dependency (Celery broker)
docker run -d -p 5672:5672 rabbitmq

# Run redis
docker run -d -p 6379:6379 redis

# Run metabase dependency
docker run -d -p 3000:3000 -e "MB_DB_TYPE=postgres" -e "MB_DB_DBNAME=metabase" -e "MB_DB_PORT=5432" -e "MB_DB_USER=postgres" -e "MB_DB_PASS=password" -e "MB_DB_HOST=host.docker.internal" --name metabase metabase/metabase

# Run workers docker container
docker run -d -e MAMUT_CONFIG_ENVIRONMENT="betoksoft" -e PGDB_URL="postgresql://host.docker.internal:5432/db_mamut_co?user=postgres&password=mypassword" -e REDIS_URL="redis://:mypassword@host.docker.internal:6379/0" -e RABBIT_MQ_URL="amqp://host.docker.internal:5672" -it mamut-co-backend-image celery worker --app mamut.celery_processing -Ofair --loglevel=info --without-mingle --without-gossip

# Run workers beat docker container
docker run -d -e MAMUT_CONFIG_ENVIRONMENT="betoksoft" -e PGDB_URL="postgresql://host.docker.internal:5432/db_mamut_co?user=postgres&password=mypassword" -e REDIS_URL="redis://:mypassword@host.docker.internal:6379/0" -e RABBIT_MQ_URL="amqp://host.docker.internal:5672" -it mamut-co-backend-image celery beat --app mamut.celery_processing --loglevel=info
