import datetime
from typing import Any, List
from databases import Database
from aioredis import Redis
from mamut.conn.redis.redis_utils import get_redis_database
from mamut.core.utils import get_response
from fastapi import APIRouter, Body, Depends, Query
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR
from mamut.conn.postgresql.postgresql_utils import get_pg_database
from mamut.models.response_model import ResponseModel
from mamut.models.token import Token
from mamut.models.user import UserInSelect, UserTokenInResponse, UserTokenInCreate, UserInUpdate, UserTokenInSelect
from mamut.crud.user import select_users, validate_credentials, update_user, select_users_for_signup
from mamut.core.jwt import create_access_token, CheckToken
from mamut.core.utils import create_aliased_response
from mamut.core.config import TOKEN_MINUTES_EXPIRATION
from datetime import datetime, timedelta
from mamut.bl.send_email import SendEmail


router = APIRouter()


@router.post("/user/create_session", response_model=ResponseModel[UserTokenInResponse], tags=['user'])
async def create_session(
    auth: UserTokenInCreate = Body(..., embed=True)
    , conn: Database = Depends(get_pg_database)
    , redis: Redis = Depends(get_redis_database)
):
    needs_retry_sign_process: bool = False
    try:
        users_by_name_result = await select_users(conn, UserInSelect(name=auth.name))

        if len(users_by_name_result) > 0:
            user_by_name = users_by_name_result[0]
            user = await validate_credentials(conn, UserTokenInSelect(**auth.dict()))

            if user is None:
                # Update consecutive failed login attempts
                await update_user(conn, UserInUpdate(user_id=user_by_name.user_id, consecutive_failed_login_attempts=(user_by_name.consecutive_failed_login_attempts + 1), last_modified_by=user_by_name.user_id))
                return get_response(None, 'INVALID_CREDENTIALS')

            access_token = create_access_token(data=create_aliased_response(Token(**user.dict())))

            # Calculate seconds until expire
            seconds_when_generated = datetime.utcnow().timestamp()
            seconds_until_expire = (datetime.utcnow() + timedelta(minutes=TOKEN_MINUTES_EXPIRATION)).timestamp()

            # Reset consecutive failed login attempts
            await update_user(conn, UserInUpdate(user_id=user.user_id, consecutive_failed_login_attempts=0, last_modified_by=user.user_id))

            return get_response(UserTokenInResponse(token=access_token, permissions=user.permissions, seconds_when_generated=seconds_when_generated, seconds_until_expire=seconds_until_expire, need_retry_sign_process=needs_retry_sign_process), 'TOKEN_GENERATED')
        return get_response(None, 'INVALID_CREDENTIALS')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.put("/user/refresh_session", response_model=ResponseModel[UserTokenInResponse], tags=['user'])
async def refresh_session(
    token: Token = Depends(CheckToken())
):
    try:
        access_token = create_access_token(data=create_aliased_response(token))

        # Calculate seconds until expire
        seconds_when_generated = datetime.utcnow().timestamp()
        seconds_until_expire = (datetime.utcnow() + timedelta(minutes=TOKEN_MINUTES_EXPIRATION)).timestamp()

        return get_response(UserTokenInResponse(token=access_token, seconds_when_generated=seconds_when_generated, seconds_until_expire=seconds_until_expire), 'TOKEN_GENERATED')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.post("/user/send_forgot_password_email", response_model=ResponseModel[Any], tags=['user'])
async def send_forgot_password_email(
    username_or_email: str = Query(..., alias='usernameOrEmail')
    , conn: Database = Depends(get_pg_database)
):
    try:
        users_by_name_or_email_result = await select_users_for_signup(conn, UserInSelect(name=username_or_email, email=username_or_email))
        if len(users_by_name_or_email_result) > 0:
            user_found = users_by_name_or_email_result[0]
            access_token = create_access_token(data=create_aliased_response(Token(**user_found.dict())))
            send_email_handler = SendEmail()
            await send_email_handler.send_change_password([user_found.email], 'es', access_token)
            return get_response(data=None, code='OBTAINED_SUCCESSFULLY')
        return get_response(data=None, code='OBTAINED_UNSUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.put("/user/change_password", response_model=ResponseModel[Any], tags=['user'])
async def change_password(
    token: Token = Depends(CheckToken())
    , password: str = Query(..., alias='password')
    , conn: Database = Depends(get_pg_database)
):
    try:
        await update_user(conn, UserInUpdate(user_id=token.user_id, password=password))
        return get_response(data=None, code='UPDATED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)
