from typing import List
from databases import Database
from mamut.core.utils import get_response
from fastapi import APIRouter, Body, Depends, Query, Request
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR
from mamut.conn.postgresql.postgresql_utils import get_pg_database
from mamut.crud.profile import select_profiles
from mamut.models.profile import ProfileInSelect, ProfileInDb
from mamut.models.response_model import ResponseModel
from mamut.core.jwt import CheckToken
from mamut.models.token import Token
from mamut.models.user import SignupInCreate, UserInDb, UserInSelect, UserInInsert, UserTokenInResponse, UserInUpdate
from mamut.core.jwt import create_access_token
from mamut.core.utils import create_aliased_response
from mamut.crud.user import select_users, insert_user, update_user, select_users_for_signup
from mamut.bl.send_email import SendEmail

router = APIRouter()


@router.post("/signup", response_model=ResponseModel[UserInDb], tags=['signup'])
async def signup(
        user: SignupInCreate = Body(..., embed=True)
        , conn: Database = Depends(get_pg_database)
):
    try:
        # Check if the username is already taken
        users = await select_users_for_signup(conn, UserInSelect(name=user.name, email=user.email))

        async with conn.transaction():
            if len(users) == 0 or users[0].is_confirmed is False:
                profiles: List[ProfileInDb] = await select_profiles(conn, ProfileInSelect(name='Cliente', is_active=True))
                if len(profiles) > 0:
                    user.profile_id = profiles[len(profiles) - 1].profile_id
                    # Insert the user
                    user_id = await insert_user(conn, UserInInsert(**user.dict(), last_modified_by=1, is_confirmed=False))
                    # Get the new user
                    users = await select_users(conn, UserInSelect(user_id=user_id))
            else:
                if users[0].is_confirmed:
                    return get_response(None, 'ALREADY_EXISTS')

            access_token = create_access_token(data=create_aliased_response(Token(**users[0].dict())))

            send_email_handler = SendEmail()
            await send_email_handler.send_account_confirmation([users[0].email], 'es', access_token, user.name)

        return get_response(None, 'INSERTED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.get("/confirm_account", response_model=ResponseModel, tags=['signup'])
async def confirm_account(
        conn: Database = Depends(get_pg_database),
        token: str = Query(..., alias='token')
):
    try:
        check_token = CheckToken()
        token = check_token.check_token('Bearer ' + token)
        await update_user(conn, UserInUpdate(user_id=token.user_id, is_confirmed=True))
        return get_response(None, 'UPDATED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)
