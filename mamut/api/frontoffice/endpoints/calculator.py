from loguru import logger
from aioredis import Redis
from databases import Database
from fastapi import APIRouter, Body, Depends, Query
from mamut.conn.postgresql.postgresql_utils import get_pg_database
from mamut.models.calculator import CalculatorInReponse
from mamut.core.utils import get_response
from mamut.models.calculator import CalculatorInCreate, CalculatorConfigurationInResponse
from mamut.bl.calculator import get_loan_calculation_formatted
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR
from mamut.bl.loans_configuration import LoanConfigurationHandler
from mamut.conn.redis.redis_utils import get_redis_database

router = APIRouter()


@router.get('/calculator/configuration', response_model=CalculatorConfigurationInResponse, tags=['calculator'])
async def get_calculator_configuration(*
    , conn: Database = Depends(get_pg_database)
    , redis: Redis = Depends(get_redis_database)
    , supplier_id: int = Query(None, alias='supplierId')
    ):
    try:
        loan_config_handler = LoanConfigurationHandler(conn, redis)

        supplier_id = await loan_config_handler.get_default_supplier_id(supplier_id)

        active_loans_configuration = await loan_config_handler.get_active_loan_configuration(supplier_id)

        if active_loans_configuration is None:
            return get_response(data=None, code='OBTAINED_UNSUCCESSFULLY', exception='Active loan configuration was not found.')
        else:
            calculator_config = CalculatorConfigurationInResponse(**active_loans_configuration.dict())
            return get_response(data=calculator_config, code='OBTAINED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.get('/calculator/details', response_model=CalculatorInReponse, tags=['calculator'])
async def get_calculation_details(*
    , conn: Database = Depends(get_pg_database)
    , redis: Redis = Depends(get_redis_database)
    , amount: float
    , payments_quantity: int = Query(None, alias='paymentsQuantity')
    , term_in_days: int = Query(None, alias='termInDays')
    , supplier_id: int = Query(None, alias='supplierId')
    ):
    try:
        loan_config_handler = LoanConfigurationHandler(conn, redis)
        supplier_id = await loan_config_handler.get_default_supplier_id(supplier_id)

        active_loans_configuration = await loan_config_handler.get_active_loan_configuration(supplier_id)

        if active_loans_configuration is None:
            return get_response(data=None, code='OBTAINED_UNSUCCESSFULLY', exception='Active loan configuration was not found.')
        else:
            loan_calculation = get_loan_calculation_formatted(active_loans_configuration, CalculatorInCreate(amount=amount, payments_quantity=payments_quantity, term_in_days=term_in_days))
            return get_response(data=loan_calculation, code='OBTAINED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)
