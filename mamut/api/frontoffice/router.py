from fastapi import APIRouter

from mamut.api.frontoffice.endpoints.calculator import router as calculator_router
from mamut.api.frontoffice.endpoints.signup import router as signup_router
from mamut.api.frontoffice.endpoints.user import router as user_router


router = APIRouter()
router.include_router(calculator_router)
router.include_router(signup_router)
router.include_router(user_router)
