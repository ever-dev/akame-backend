from fastapi import APIRouter
from mamut.api.frontoffice.router import router as frontoffice_router
from mamut.api.backoffice.router import router as backoffice_router

router = APIRouter()
router.include_router(frontoffice_router, prefix='/frontoffice')
router.include_router(backoffice_router, prefix='/backoffice')
