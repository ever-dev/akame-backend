from fastapi import APIRouter

from mamut.api.backoffice.endpoints.loans_configuration import router as loans_configuration_router
from mamut.api.backoffice.endpoints.user import router as user_router
from mamut.api.backoffice.endpoints.profile import router as profile_router
from mamut.api.backoffice.endpoints.suppliers import router as suppliers_router

router = APIRouter()
router.include_router(loans_configuration_router)
router.include_router(user_router)
router.include_router(profile_router)
router.include_router(suppliers_router)
