from loguru import logger
from typing import List
from databases import Database
from fastapi import APIRouter, Body, Depends, Query
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR

from mamut.core.jwt import CheckToken
from mamut.crud.suppliers import get_suppliers_db, get_suppliers_accounts_db, get_suppliers_accounts_identifications_db, insert_suppliers, update_suppliers, insert_suppliers_accounts, update_suppliers_accounts, insert_suppliers_accounts_identifications, update_suppliers_accounts_identifications
from mamut.models.suppliers import Suppliers, SuppliersInSelect, SuppliersInCreate, SuppliersInInsert, SuppliersInDb, SuppliersInUpdate, SuppliersAccounts, SuppliersAccountsInSelect, SuppliersAccountsInCreate, SuppliersAccountsInInsert, SuppliersAccountsInDb, SuppliersAccountsInUpdate, SuppliersAccountsIdentification, SuppliersAccountsIdentificationInSelect, SuppliersAccountsIdentificationInCreate, SuppliersAccountsIdentificationInInsert, SuppliersAccountsIdentificationInDb, SuppliersAccountsIdentificationInUpdate
from mamut.conn.postgresql.postgresql_utils import get_pg_database
from mamut.core.utils import get_response
from mamut.models.response_model import ResponseModel
from mamut.models.token import Token

router = APIRouter()


@router.get("/suppliers/get", response_model=ResponseModel[List[Suppliers]], tags=['suppliers'])
async def get_suppliers(*,
                        db: Database = Depends(get_pg_database),
                        token: Token = Depends(CheckToken()),
                        supplier_id=Query(None, alias='supplierId'),
                        name=Query(None, alias='name')
                        ):
    try:
        suppliers = SuppliersInSelect(
            supplier_id=supplier_id,
            name=name
        )
        logger.info(f'CALL get suppliers with params: {suppliers}')
        result: List[Suppliers] = await get_suppliers_db(db, suppliers)

        return get_response(data=result, code='OBTAINED_SUCCESSFULLY')
    except Exception as ex:
        print(ex)
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=ex, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.post("/suppliers/insert", response_model=ResponseModel[SuppliersInDb], tags=['suppliers'])
async def suppliers_insert( supplier: SuppliersInCreate = Body(..., embed=True)
                            , conn: Database = Depends(get_pg_database)
                            , token: Token = Depends(CheckToken())
                            ):
    try:
        # Check if the supplier is already taken
        suppliers_found = await get_suppliers_db(conn, SuppliersInSelect(name=supplier.name))
        if len(suppliers_found) > 0:
            return get_response(None, 'ALREADY_EXISTS')

        # Insert the supplier
        supplier_id = await insert_suppliers(conn, SuppliersInInsert(**supplier.dict(), last_modified_by=token.user_id))

        # Get the new supplier
        supplier = await get_suppliers_db(conn, SuppliersInSelect(supplier_id=supplier_id))

        # Return the new inserted supplier
        return get_response(supplier[0], 'INSERTED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.put("/suppliers/update", response_model=ResponseModel[SuppliersInDb], tags=['suppliers'])
async def suppliers_update(supplier: SuppliersInUpdate = Body(..., embed=True)
                            , conn: Database = Depends(get_pg_database)
                            , token: Token = Depends(CheckToken())
                            ):
    try:
        # Check if the supplier is already taken
        if supplier.name is not None:
            supplier_found = await get_suppliers_db(conn, SuppliersInSelect(name=supplier.name))
            if len(supplier_found) > 0 and supplier_found[0].supplier_id != supplier.supplier_id:
                return get_response(None, 'ALREADY_EXISTS')

        # Update the supplier
        await update_suppliers(conn, SuppliersInUpdate(**supplier.dict()))

        # Get the new supplier
        supplier = await get_suppliers_db(conn, SuppliersInSelect(supplier_id=supplier.supplier_id))

        # Return the updated supplier
        return get_response(supplier[0], 'UPDATED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.delete("/suppliers/delete", response_model=ResponseModel, tags=['suppliers'])
async def suppliers_delete(supplier_id: int = Query(..., alias='supplierId')
                            , conn: Database = Depends(get_pg_database)
                            , token: Token = Depends(CheckToken())
                            ):
    try:
        # Update the supplier
        await update_suppliers(conn, SuppliersInUpdate(supplier_id=supplier_id, is_active=False, is_deleted=True, last_modified_by=token.user_id))

        return get_response(None, 'DELETED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)

@router.get("/suppliersAccounts/get", response_model=ResponseModel[List[SuppliersAccounts]], tags=['suppliersAccounts'])
async def get_suppliers_accounts(*,
                                 db: Database = Depends(get_pg_database),
                                 token: Token = Depends(CheckToken()),
                                 supplier_account_id: int = Query(None, alias='supplierAccountId'),
                                 account_id: int = Query(None, alias='accountId'),
                                 supplier_id: int = Query(None, alias='supplierId')
                                 ):
    try:
        suppliers_accounts = SuppliersAccountsInSelect(
            supplier_account_id=supplier_account_id,
            account_id=account_id,
            supplier_id=supplier_id
        )
        logger.info(f'CALL get suppliers accounts with params: {suppliers_accounts}')
        result: List[SuppliersAccounts] = await get_suppliers_accounts_db(db, suppliers_accounts)

        return get_response(data=result, code='OBTAINED_SUCCESSFULLY')
    except Exception as ex:
        print(ex)
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=ex, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.post("/suppliersAccounts/insert", response_model=ResponseModel[SuppliersAccountsInDb], tags=['suppliersAccounts'])
async def suppliers_accounts_insert(supplier_account: SuppliersAccountsInCreate = Body(..., embed=True)
                                    , conn: Database = Depends(get_pg_database)
                                    , token: Token = Depends(CheckToken())
                                    ):
    try:
        # Check if the supplier account is already taken
        supplier_accounts_found = await get_suppliers_accounts_db(conn, SuppliersAccountsInSelect(account_id=supplier_account.account_id, supplier_id=supplier_account.supplier_id))
        if len(supplier_accounts_found) > 0:
            return get_response(None, 'ALREADY_EXISTS')

        # Insert the supplier account
        supplier_account_id = await insert_suppliers_accounts(conn, SuppliersAccountsInInsert(**supplier_account.dict(), last_modified_by=token.user_id))

        # Get the new supplier account
        supplier_account = await get_suppliers_accounts_db(conn, SuppliersAccountsInSelect(supplier_account_id=supplier_account_id))

        # Return the new inserted supplier account
        return get_response(supplier_account[0], 'INSERTED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.put("/suppliersAccounts/update", response_model=ResponseModel[SuppliersAccountsInDb], tags=['suppliersAccounts'])
async def suppliers_accounts_update(
                                    supplier_account: SuppliersAccountsInUpdate = Body(..., embed=True)
                                    , conn: Database = Depends(get_pg_database)
                                    , token: Token = Depends(CheckToken())
                                    ):
    try:
        # Check if the supplier account is already taken
        if supplier_account.account_id is not None:
            supplier_account_found = await get_suppliers_accounts_db(conn, SuppliersAccountsInSelect(account_id=supplier_account.account_id))
            if len(supplier_account_found) > 0 and supplier_account_found[0].supplier_account_id != supplier_account.supplier_account_id:
                return get_response(None, 'ALREADY_EXISTS')

        # Update the supplier account
        supplier_account.last_modified_by = token.user_id
        await update_suppliers_accounts(conn, SuppliersAccountsInUpdate(**supplier_account.dict()))

        # Get the new supplier account
        supplier_account = await get_suppliers_accounts_db(conn, SuppliersAccountsInSelect(supplier_account_id=supplier_account.supplier_account_id))

        # Return the updated supplier account
        return get_response(supplier_account[0], 'UPDATED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)

@router.delete("/suppliersAccounts/delete", response_model=ResponseModel, tags=['suppliersAccounts'])
async def suppliers_accounts_delete(
                                supplier_account_id: int = Query(..., alias='supplierAccountId')
                                , conn: Database = Depends(get_pg_database)
                                , token: Token = Depends(CheckToken())
                                ):
    try:
        # Update the supplier account
        await update_suppliers_accounts(conn, SuppliersAccountsInUpdate(supplier_account_id=supplier_account_id, is_active=False, is_deleted=True, last_modified_by=token.user_id))

        return get_response(None, 'DELETED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)

@router.get("/suppliersAccountsIdentification/get", response_model=ResponseModel[List[SuppliersAccountsIdentification]], tags=['suppliersAccountsIdentification'])
async def get_supplier_account_identification(*,
                                                 db: Database = Depends(get_pg_database),
                                                 token: Token = Depends(CheckToken()),
                                                 supplier_account_identification_id: int = Query(None, alias='supplierAccountIdentificationId'),
                                                 identification: int = Query(None, alias='identification'),
                                                 identification_type_id: int = Query(None, alias='identificationTypeId'),
                                                 supplier_id: int = Query(None, alias='supplierId')
                                                 ):
    try:
        suppliers_accounts_identification = SuppliersAccountsIdentificationInSelect(
            supplier_account_identification_id=supplier_account_identification_id,
            identification=identification,
            identification_type_id=identification_type_id,
            supplier_id=supplier_id
        )
        logger.info(
            f'CALL get suppliers accounts identification with params: {suppliers_accounts_identification}')
        result: List[SuppliersAccounts] = await get_suppliers_accounts_identifications_db(db, suppliers_accounts_identification)

        return get_response(data=result, code='OBTAINED_SUCCESSFULLY')
    except Exception as ex:
        print(ex)
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=ex, status_code=HTTP_500_INTERNAL_SERVER_ERROR)

@router.post("/suppliersAccountsIdentification/insert", response_model=ResponseModel[SuppliersAccountsIdentificationInDb], tags=['suppliersAccountsIdentification'])
async def supplier_account_identification_insert(supplier_accounts_identification: SuppliersAccountsIdentificationInCreate = Body(..., embed=True)
                                    , conn: Database = Depends(get_pg_database)
                                    , token: Token = Depends(CheckToken())
                                    ):
    try:
        # Check if the supplier account identification is already taken
        supplier_accounts_identification_found = await get_suppliers_accounts_identifications_db(conn, SuppliersAccountsIdentificationInSelect(identification=supplier_accounts_identification.identification, supplier_id=supplier_accounts_identification.supplier_id, identification_type_id=supplier_accounts_identification.identification_type_id))
        if len(supplier_accounts_identification_found) > 0:
            return get_response(None, 'ALREADY_EXISTS')

        # Insert the supplier account identification
        supplier_account_identification_id = await insert_suppliers_accounts_identifications(conn, SuppliersAccountsIdentificationInInsert(**supplier_accounts_identification.dict(), last_modified_by=token.user_id))

        # Get the new supplier account identification
        supplier_accounts_identification = await get_suppliers_accounts_identifications_db(conn, SuppliersAccountsIdentificationInSelect(supplier_account_identification_id=supplier_account_identification_id))

        # Return the new inserted supplier account identification
        return get_response(supplier_accounts_identification[0], 'INSERTED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)

@router.put("/suppliersAccountsIdentification/update", response_model=ResponseModel[SuppliersAccountsIdentificationInDb], tags=['suppliersAccountsIdentification'])
async def suppliers_accounts_identification_update(
    supplier_accounts_identification: SuppliersAccountsIdentificationInUpdate = Body(..., embed=True)
    , conn: Database = Depends(get_pg_database)
    , token: Token = Depends(CheckToken())
    ):
    try:
        # Check if the supplier account identification is already taken
        if supplier_accounts_identification.identification is not None:
            supplier_accounts_identification_found = await get_suppliers_accounts_identifications_db(conn, SuppliersAccountsIdentificationInSelect(identification=supplier_accounts_identification.identification))
            if len(supplier_accounts_identification_found) > 0 and supplier_accounts_identification_found[0].identification != supplier_accounts_identification.identification:
                return get_response(None, 'ALREADY_EXISTS')

        # Update the supplier account identification
        supplier_accounts_identification.last_modified_by = token.user_id
        await update_suppliers_accounts_identifications(conn, SuppliersAccountsIdentificationInUpdate(**supplier_accounts_identification.dict()))

        # Get the new supplier account identification
        supplier_accounts_identification = await get_suppliers_accounts_identifications_db(conn, SuppliersAccountsIdentificationInSelect(supplier_account_identification_id=supplier_accounts_identification.supplier_account_identification_id))

        # Return the updated supplier account identification
        return get_response(supplier_accounts_identification[0], 'UPDATED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)

@router.delete("/suppliersAccountsIdentification/delete", response_model=ResponseModel, tags=['suppliersAccountsIdentification'])
async def suppliers_accounts_identification_delete(
                                    supplier_account_identification_id: int = Query(..., alias='supplierAccountIdentificationId')
                                    , conn: Database = Depends(get_pg_database)
                                    , token: Token = Depends(CheckToken())
                                    ):
    try:
        # Update the supplier account identification 
        await update_suppliers_accounts_identifications(conn, SuppliersAccountsIdentificationInUpdate(supplier_account_identification_id=supplier_account_identification_id, is_active=False, is_deleted=True, last_modified_by=token.user_id))

        return get_response(None, 'DELETED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)
