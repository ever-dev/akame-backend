from typing import List
from loguru import logger
from aioredis import Redis
from databases import Database
from fastapi import APIRouter, Body, Depends, Query, Request
from mamut.models.response_model import ResponseModel
from mamut.conn.postgresql.postgresql_utils import get_pg_database
from mamut.core.utils import get_response
from mamut.crud.loans_configuration import select_loans_configuration, insert_loans_configuration, \
    update_loans_configuration
from mamut.models.loans_configuration import LoansConfigurationInCreate, LoansConfigurationInDb, \
    LoansConfigurationInInsert, LoansConfigurationInUpdate, LoansConfigurationInSelect
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR
from mamut.bl.loans_configuration import get_tem, get_ted
from mamut.models.token import Token
from mamut.core.jwt import CheckToken
from mamut.conn.redis.redis_utils import get_redis_database
from mamut.bl.loans_configuration import LoanConfigurationHandler

router = APIRouter()


@router.get("/loans_configurations/get", response_model=ResponseModel[List[LoansConfigurationInDb]], tags=['loans_configuration'])
async def get_loan_configuration(*,
                                 conn: Database = Depends(get_pg_database)
                                 , token: Token = Depends(CheckToken())
                                 , request: Request
                                 , loans_configuration_id: int = Query(None, alias='loansConfigurationId')
                                 , is_active: bool = Query(None, alias='isActive')
                                 , supplier_id: int = Query(None, alias='supplierId')
                                 ):
    try:
        loans_configurations = await select_loans_configuration(conn, LoansConfigurationInSelect(**dict(request.query_params)))
        if loans_configurations is not None:
            return get_response(data=loans_configurations, code='OBTAINED_SUCCESSFULLY')
        else:
            return get_response(data=None, code='OBTAINED_UNSUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e,
                            status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.post("/loans_configurations/insert", response_model=ResponseModel[LoansConfigurationInDb], tags=['loans_configuration'])
async def create_loan_configuration(
        loans_configuration: LoansConfigurationInCreate = Body(..., embed=True)
        , conn: Database = Depends(get_pg_database)
        , redis: Redis = Depends(get_redis_database)
        , token: Token = Depends(CheckToken())
):
    try:
        async with conn.transaction():
            # Disable all the loans configuration
            await update_loans_configuration(conn, LoansConfigurationInUpdate(is_active=False, supplier_id=loans_configuration.supplier_id))

            loans_configuration_dict = loans_configuration.dict()
            loans_configuration_dict['tem_interest'] = get_tem(loans_configuration.tea_interest)
            loans_configuration_dict['ted_interest'] = get_ted(loans_configuration.tea_interest)

            loans_configuration_dict['tem_mora'] = get_tem(loans_configuration.tea_mora)
            loans_configuration_dict['ted_mora'] = get_ted(loans_configuration.tea_mora)

            loans_configuration_for_insert = LoansConfigurationInInsert(**loans_configuration_dict,
                                                                        last_modified_by=token.user_id)

            loan_config_handler = LoanConfigurationHandler(conn, redis)
            loan_config_in_db = await loan_config_handler.save_loan_configuration(loans_configuration_for_insert)

            if loan_config_in_db is not None:
                return get_response(data=loan_config_in_db, code='INSERTED_SUCCESSFULLY')
            else:
                return get_response(data=None, code='INSERTED_UNSUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e,
                            status_code=HTTP_500_INTERNAL_SERVER_ERROR)
