from loguru import logger
from typing import List
from databases import Database
from mamut.core.utils import get_response
from fastapi import APIRouter, Body, Depends, Query, Request
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR
from mamut.conn.postgresql.postgresql_utils import get_pg_database
from mamut.models.response_model import ResponseModel
from mamut.models.token import Token
from mamut.models.user import UserInCreate, UserInDb, UserInSelect, UserInInsert, UserTokenInResponse, UserTokenInCreate, UserInUpdate, UserTokenInSelect
from mamut.crud.user import select_users, insert_user, validate_credentials, update_user
from mamut.core.jwt import create_access_token
from mamut.core.utils import create_aliased_response
from mamut.core.jwt import CheckToken
from mamut.core.config import TOKEN_MINUTES_EXPIRATION
from datetime import datetime, timedelta


router = APIRouter()


@router.post("/user/create_session", response_model=ResponseModel[UserTokenInResponse], tags=['user'])
async def create_session(
    auth: UserTokenInCreate = Body(..., embed=True)
    , conn: Database = Depends(get_pg_database)
):
    try:
        users_by_name_result = await select_users(conn, UserInSelect(name=auth.name))

        if len(users_by_name_result) > 0:
            user_by_name = users_by_name_result[0]

            user = await validate_credentials(conn, UserTokenInSelect(**auth.dict()))

            if user.profile_name != 'MASTER':
                return get_response(None, 'NOT_AUTHORIZED')

            if user is None:
                # Update consecutive failed login attempts
                await update_user(conn, UserInUpdate(user_id=user_by_name.user_id, consecutive_failed_login_attempts=(user_by_name.consecutive_failed_login_attempts + 1), last_modified_by=user_by_name.user_id))
                return get_response(None, 'INVALID_CREDENTIALS')

            access_token = create_access_token(data=create_aliased_response(Token(**user.dict())))

            # Calculate seconds until expire
            seconds_when_generated = datetime.utcnow().timestamp()
            seconds_until_expire = (datetime.utcnow() + timedelta(minutes=TOKEN_MINUTES_EXPIRATION)).timestamp()

            # Reset consecutive failed login attempts
            await update_user(conn, UserInUpdate(user_id=user_by_name.user_id, consecutive_failed_login_attempts=0, last_modified_by=user_by_name.user_id))
            return get_response(UserTokenInResponse(token=access_token, permissions=user.permissions, seconds_when_generated=seconds_when_generated, seconds_until_expire=seconds_until_expire), 'TOKEN_GENERATED')
        return get_response(None, 'INVALID_CREDENTIALS')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.put("/user/refresh_session", response_model=ResponseModel[UserTokenInResponse], tags=['user'])
async def refresh_session(
    token: Token = Depends(CheckToken())
):
    try:
        access_token = create_access_token(data=create_aliased_response(token))

        # Calculate seconds until expire
        seconds_when_generated = datetime.utcnow().timestamp()
        seconds_until_expire = (datetime.utcnow() + timedelta(minutes=TOKEN_MINUTES_EXPIRATION)).timestamp()

        return get_response(UserTokenInResponse(token=access_token, seconds_when_generated=seconds_when_generated, seconds_until_expire=seconds_until_expire), 'TOKEN_GENERATED')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.get("/user/get", response_model=ResponseModel[List[UserInDb]], tags=['user'])
async def user_get(*,
    conn: Database = Depends(get_pg_database)
    , token: Token = Depends(CheckToken())
    , request: Request
    , user_id: int = Query(None, alias='userId')
    , profile_id: int = Query(None, alias='profileId')
    , name: str = Query(None, alias='name')
    ):
    try:
        users = await select_users(conn, UserInSelect(**dict(request.query_params)))
        return get_response(users, 'OBTAINED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.post("/user/insert", response_model=ResponseModel[UserInDb], tags=['user'])
async def user_insert(
    user: UserInCreate = Body(..., embed=True)
    , conn: Database = Depends(get_pg_database)
    , token: Token = Depends(CheckToken())
    ):
    try:
        # Check if the username is already taken
        users_found = await select_users(conn, UserInSelect(name=user.name))
        if len(users_found) > 0:
            return get_response(None, 'ALREADY_EXISTS')

        # Insert the user
        user_id = await insert_user(conn, UserInInsert(**user.dict(), last_modified_by=token.user_id, is_confirmed=True))

        # Get the new user
        user = await select_users(conn, UserInSelect(user_id=user_id))

        # Return the new inserted user
        return get_response(user[0], 'INSERTED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.put("/user/update", response_model=ResponseModel[UserInDb], tags=['user'])
async def user_update(
    user: UserInUpdate = Body(..., embed=True)
    , conn: Database = Depends(get_pg_database)
    , token: Token = Depends(CheckToken())
    ):
    try:
        # Check if the username is already taken
        if user.name is not None:
            users_found = await select_users(conn, UserInSelect(name=user.name))
            if len(users_found) > 0 and users_found[0].user_id != user.user_id:
                return get_response(None, 'ALREADY_EXISTS')

        # Update the user
        user.last_modified_by = token.user_id
        await update_user(conn, UserInUpdate(**user.dict()))

        # Get the new user
        user = await select_users(conn, UserInSelect(user_id=user.user_id))

        # Return the updated user
        return get_response(user[0], 'UPDATED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.delete("/user/delete", response_model=ResponseModel, tags=['user'])
async def user_delete(
    user_id: int = Query(..., alias='userId')
    , conn: Database = Depends(get_pg_database)
    , token: Token = Depends(CheckToken())
    ):
    try:
        # Update the user
        await update_user(conn, UserInUpdate(user_id=user_id, is_deleted=True, last_modified_by=token.user_id))

        return get_response(None, 'DELETED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.get("/user/check_token", response_model=ResponseModel[Token], tags=['user'])
async def check_token_endpoint(
    token: Token = Depends(CheckToken())
):
    try:
        return get_response(token, 'OBTAINED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)
