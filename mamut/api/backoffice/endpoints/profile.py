from loguru import logger
from typing import List
from databases import Database
from mamut.core.utils import get_response
from fastapi import APIRouter, Body, Depends, Query, Request
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR
from mamut.conn.postgresql.postgresql_utils import get_pg_database
from mamut.models.response_model import ResponseModel
from mamut.models.token import Token
from mamut.core.jwt import create_access_token
from mamut.core.jwt import CheckToken
from mamut.models.profile import ProfileInSelect, ProfileInDb, ProfileInCreate, ProfileInInsert, ProfileInUpdate
from mamut.crud.profile import select_profiles, insert_profile, update_profile


router = APIRouter()


@router.get("/profile/get", response_model=ResponseModel[List[ProfileInDb]], tags=['profile'])
async def profile_get(*,
    conn: Database = Depends(get_pg_database)
    , token: Token = Depends(CheckToken())
    , request: Request
    , profile_id: int = Query(None, alias='profileId')
    , name: str = Query(None, alias='name')
    ):
    try:
        profiles = await select_profiles(conn, ProfileInSelect(**dict(request.query_params)))
        return get_response(profiles, 'OBTAINED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.post("/profile/insert", response_model=ResponseModel[ProfileInDb], tags=['profile'])
async def profile_insert(
    profile: ProfileInCreate = Body(..., embed=True)
    , conn: Database = Depends(get_pg_database)
    , token: Token = Depends(CheckToken())
    ):
    try:
        # Check if the username is already taken
        profiles_found = await select_profiles(conn, ProfileInSelect(name=profile.name))
        if len(profiles_found) > 0:
            return get_response(None, 'ALREADY_EXISTS')
        
        # Insert the profile
        profile_id = await insert_profile(conn, ProfileInInsert(**profile.dict(), last_modified_by=token.user_id))

        # Get the new profile
        profile = await select_profiles(conn, ProfileInSelect(profile_id=profile_id))

        # Return the new inserted profile
        return get_response(profile[0], 'INSERTED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.put("/profile/update", response_model=ResponseModel[ProfileInDb], tags=['profile'])
async def profile_update(
    profile: ProfileInUpdate = Body(..., embed=True)
    , conn: Database = Depends(get_pg_database)
    , token: Token = Depends(CheckToken())
    ):
    try:
        # Check if the name is already taken
        if profile.name is not None:
            profiles_found = await select_profiles(conn, ProfileInSelect(name=profile.name))
            if len(profiles_found) > 0 and profiles_found[0].profile_id != profile.profile_id:
                return get_response(None, 'ALREADY_EXISTS')
        
        # Update the profile
        profile.last_modified_by = token.user_id
        await update_profile(conn, ProfileInUpdate(**profile.dict()))

        # Get the new profile
        profile = await select_profiles(conn, ProfileInSelect(profile_id=profile.profile_id))

        # Return the updated profile
        return get_response(profile[0], 'UPDATED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)


@router.delete("/profile/delete", response_model=ResponseModel, tags=['profile'])
async def profile_delete(
    profile_id: int = Query(..., alias='profileId')
    , conn: Database = Depends(get_pg_database)
    , token: Token = Depends(CheckToken())
    ):
    try:
        # Update the profile
        await update_profile(conn, ProfileInUpdate(profile_id=profile_id, is_deleted=True, last_modified_by=token.user_id))

        return get_response(None, 'DELETED_SUCCESSFULLY')
    except Exception as e:
        return get_response(data=None, code='GENERAL_EXCEPTION', exception=e, status_code=HTTP_500_INTERNAL_SERVER_ERROR)
