from loguru import logger
from databases import Database
from mamut.core.config import POSTGRESQL_URL, MIN_CONNECTIONS_COUNT, MAX_CONNECTIONS_COUNT
from mamut.conn.postgresql.postgresql_utils import pgdb


async def connect_pg_database(for_celery: bool=False) -> Database:
    logger.info('Creating PG instance.')
    pg_conn = Database(url=POSTGRESQL_URL, min_size=MIN_CONNECTIONS_COUNT, max_size=MAX_CONNECTIONS_COUNT)
    logger.info('PG instance created successfully.')

    try:
        logger.info('Connecting to PG.')
        if not for_celery:
            pgdb.db = pg_conn
            await pgdb.db.connect()
        else:
            await pg_conn.connect()
            return pg_conn

        logger.info('Connected to PG successfully.')
    except Exception as e:
        logger.error('Some error has ocurred while trying to connect to PostgreSQL. ' + str(e))



async def close_pg_connection(db: Database=None):
    logger.info('Disconnecting from PG.')
    if db is not None:
        await db.disconnect()
    else:
        await pgdb.db.disconnect()
    logger.info('Disconnected from PG.')
