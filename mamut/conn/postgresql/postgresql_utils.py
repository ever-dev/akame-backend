from databases import Database


class PostgreSQLDataBase:
    db: Database = None


pgdb = PostgreSQLDataBase()


async def get_pg_database() -> Database:
    return pgdb.db
