from aioredis import Redis


class RedisDataBase:
    db: Redis = None


redis = RedisDataBase()


async def get_redis_database() -> Redis:
    return redis.db
