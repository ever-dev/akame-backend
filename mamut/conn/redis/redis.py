import aioredis
from loguru import logger
from aioredis import Redis, RedisConnection
from mamut.core.config import REDIS_URL
from mamut.conn.redis.redis_utils import redis


async def connect_redis_database(new_instance: bool = False) -> Redis:
    logger.info('Creating Redis instance.')
    redis_conn = await aioredis.create_redis_pool(REDIS_URL)

    if not new_instance:
        redis.db = redis_conn
    else:
        return redis_conn
    logger.info('Redis instance created successfully.')


async def close_redis_connection(conn: Redis = None):
    logger.info('Disconnecting from Redis.')

    if conn is not None:
        conn.close()
        await conn.wait_closed()
    else:
        redis.db.close()
        await redis.db.wait_closed()
    logger.info('Disconnected from Redis.')
