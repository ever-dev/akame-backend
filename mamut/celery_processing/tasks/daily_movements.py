import asyncio
from datetime import datetime
from mamut.celery_processing.celery import app
from mamut.bl.loan_handler import LoanHandler
from mamut.conn.postgresql.postgresql import connect_pg_database, close_pg_connection
from mamut.conn.redis.redis import connect_redis_database, close_redis_connection

@app.task
def perform_daily_movements():
    asyncio.set_event_loop(asyncio.new_event_loop())

    pg_conn = asyncio.get_event_loop().run_until_complete(connect_pg_database(True))
    redis_conn = asyncio.get_event_loop().run_until_complete(connect_redis_database(True))
    try:
        loan_handler = LoanHandler(pg_conn, None, redis_conn)
        asyncio.get_event_loop().run_until_complete(loan_handler.update_payment_plan())
        asyncio.get_event_loop().run_until_complete(loan_handler.update_today_balance())
    finally:
        asyncio.get_event_loop().run_until_complete(close_pg_connection(pg_conn))
        asyncio.get_event_loop().run_until_complete(close_redis_connection(redis_conn))

    now_date = datetime.utcnow().strftime('%Y-%m-%d-%H:%M:%S')
    print(f'Daily movements executed successfully {now_date}.')
    return True
