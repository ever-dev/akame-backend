import celery
from mamut.celery_processing.celeryconfig import CeleryConfig

app = celery.Celery('mamut-co-workers')
app.config_from_object(CeleryConfig)
