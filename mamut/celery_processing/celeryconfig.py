# Celery configuration
# http://docs.celeryproject.org/en/latest/configuration.html
import os
from mamut.core.config import RABBIT_MQ_URL
from mamut.core.config import REDIS_URL
from celery.schedules import crontab

class CeleryConfig:
    RABBIT_MQ_URL_FROM_ENV = os.environ.get('RABBIT_MQ_URL', None)
    REDIS_URL_FROM_ENV = os.environ.get('REDIS_URL', None)
    broker_url = RABBIT_MQ_URL if RABBIT_MQ_URL_FROM_ENV is None else RABBIT_MQ_URL_FROM_ENV
    result_backend = REDIS_URL if REDIS_URL_FROM_ENV is None else REDIS_URL_FROM_ENV

    # json serializer is more secure than the default pickle
    task_serializer = 'json'
    accept_content = ['json']
    result_serializer = 'json'

    # Maximum retries per task
    task_annotations = {'*': {'max_retries': 3}}

    imports = ('mamut.celery_processing.tasks')

    # https://docs.celeryproject.org/en/stable/userguide/periodic-tasks.html
    # Execute every minute: crontab()
    # Execute daily at midnight: crontab(minute=0, hour=0)
    beat_schedule = {
        'perform-daily-movements': {
            'task': 'mamut.celery_processing.tasks.daily_movements.perform_daily_movements',
            'schedule': crontab(minute=0, hour=0)
        }
    }
