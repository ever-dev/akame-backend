import json
import os
import io
import base64
import asyncio
import hashlib
import time
from aioredis import Redis
from typing import List

import zeep
import pdfkit

from loguru import logger
from databases import Database
from zeep.asyncio import AsyncTransport

from mamut.bl.document_processing import DocumentProcessing
from mamut.bl.loan_handler import LoanHandler
from mamut.bl.socket_handler import SocketHandler
from mamut.core.parameters import parameter
from mamut.core.config import PROTEC_DATA_WSDL, PROTEC_DATA_ID_ENTERPRISE, PROTEC_DATA_PASSWORD, \
    PROTEC_DATA_USER_CONSUME, PROTEC_DATA_WSDL_SIGNATURE, PROTEC_DATA_ID_ENTERPRISE_CONSUME, \
    PROTEC_DATA_PASSWORD_CONSUME
from mamut.crud.api import insert_api_transaction_log, select_api_transaction_log
from mamut.crud.cities import get_cities_db
from mamut.crud.customers import get_customer_db
from mamut.crud.departaments import get_departaments_db
from mamut.crud.document import select_documents
from mamut.crud.loan import update_loan, select_loans
from mamut.crud.loan_payment_plan import update_loan_payment_plan, select_loan_payment_plan
from mamut.models.api import ApiTransactionLogInInsert, ApiTransactionLogInDb, ApiTransactionLogInSelect
from mamut.models.city import CityInSelect, City
from mamut.models.customer import CustomerInResponse, CustomerInSelect
from mamut.models.departament import DepartamentInSelect, Departament
from mamut.models.document import DocumentInDb, DocumentInSelect
from mamut.models.loan import LoanInUpdate, LoanInSelect, LoanInDb
from mamut.models.loan_payment_plan import LoanPaymentPlanInUpdate, LoanPaymentPlanInSelect
from mamut.models.protect_data import ProtectDataOTPInSelect, ProtectDataGetAudioInSelect, \
    ProtectDataVerifyAudioInSelect, ProtectDataDocumentSignatureInSelect

path = os.path.dirname(os.path.realpath(__file__))


class ProtectDataClient:
    def __init__(self, conn: Database, redis: Redis, user_id: int):
        self.conn: Database = conn
        self.redis: Redis = redis
        self.rt: str = ""
        self.user_id = user_id
        self.user_consume = PROTEC_DATA_USER_CONSUME
        self.password = PROTEC_DATA_PASSWORD
        self.password_consume = PROTEC_DATA_PASSWORD_CONSUME
        self.wsdl = PROTEC_DATA_WSDL
        self.wsdl_signature = PROTEC_DATA_WSDL_SIGNATURE
        self.id_enterprise = PROTEC_DATA_ID_ENTERPRISE
        self.id_enterprise_consume = PROTEC_DATA_ID_ENTERPRISE_CONSUME
        self.wsdl_client = zeep.Client
        self.random_numbers = []
        self.current_loan = None

    async def start(
            self,
            select_otp: ProtectDataOTPInSelect = ProtectDataOTPInSelect(),
            select_audio: ProtectDataGetAudioInSelect = ProtectDataGetAudioInSelect(),
            select_verify_audio: ProtectDataVerifyAudioInSelect = ProtectDataVerifyAudioInSelect(),
            select_document_signature: ProtectDataDocumentSignatureInSelect = ProtectDataDocumentSignatureInSelect(),
            transaction_id: str = '',
            nip: str = '',
            only_real_time: bool = False,
            only_rand_numbers: bool = False,
            only_audio: bool = False,
            verify_audio: bool = False,
            document_signature: bool = False,
            document_signature_through_call: bool = False,
            get_document_status: bool = False,
            cancel_document: bool = False
    ):
        def handle_future_real_time(future_obj):
            self.rt = future_obj.result()[0]
            logger.info(f'[protect data] ObtenerRT response: {self.rt}')

        def handle_future_otp(future_obj):
            self.rt = future_obj.result()[0]
            logger.info(f'[protect data] ObtenerRT response: {self.rt}')
            self.get_one_time_pin(select=select_otp)

        def handle_future_rand_numbers(future_obj):
            self.rt = future_obj.result()[0]
            logger.info(f'[protect data] ObtenerRT response: {self.rt}')
            self.get_random_numbers()

        def handle_future_get_audio(future_obj):
            self.rt = future_obj.result()[0]
            logger.info(f'[protect data] ObtenerRT response: {self.rt}')
            self.get_audio(select=select_audio)

        def handle_future_verify_audio(future_obj):
            self.rt = future_obj.result()[0]
            logger.info(f'[protect data] ObtenerRT response: {self.rt}')
            self.verify_audio(select=select_verify_audio)

        def handle_future_document_signature(future_obj):
            self.rt = future_obj.result()[0]
            logger.info(f'[protect data] ObtenerRT response: {self.rt}')
            loop = self.get_running_loop()

            if loop and loop.is_running():
                loop.create_task(
                    self.document_signature(select=select_document_signature)
                )
            else:
                asyncio.run(
                    self.document_signature(select=select_document_signature)
                )

        def handle_future_document_signature_through_call(future_obj):
            self.rt = future_obj.result()[0]
            logger.info(f'[protect data] ObtenerRT response: {self.rt}')
            loop = self.get_running_loop()

            if loop and loop.is_running():
                loop.create_task(
                    self.document_signature_through_call(select=select_document_signature)
                )
            else:
                asyncio.run(
                    self.document_signature_through_call(select=select_document_signature)
                )

        def handle_future_get_document_status(future_obj):
            try:
                self.rt = future_obj.result()[0]
                logger.info(f'[protect data] ObtenerRT response: {self.rt}')
                loop = self.get_running_loop()

                if loop and loop.is_running():
                    loop.create_task(self.get_document_status(transaction_id=transaction_id))
                else:
                    asyncio.run(self.get_document_status(transaction_id=transaction_id))
            except Exception as ex:
                logger.error(f'[protect data] Error in handle_future_get_document_status {ex}')
                self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocError'})
                self.update_loan_status(loans_status_id=13)  # Protec Data error

        def handle_future_cancel_document(future_obj):
            try:
                self.rt = future_obj.result()[0]
                logger.info(f'[protect data] ObtenerRT response: {self.rt}')
                loop = self.get_running_loop()

                if loop and loop.is_running():
                    loop.create_task(self.cancel_transaction(nip=nip, transaction_id=transaction_id))
                else:
                    asyncio.run(self.cancel_transaction(nip=nip, transaction_id=transaction_id))
            except Exception as ex:
                logger.error(f'[protect data] Error in handle_future_cancel_document {ex}')

        if document_signature_through_call or get_document_status or cancel_document:
            # build parameter object with id of enterprise give from ProtecData
            request_data = {
                parameter['PD_ID']: self.id_enterprise
            }
            self.wsdl_client = await self.make_client(self.wsdl_signature)
        else:
            # build parameter object with id of enterprise give from ProtecData
            request_data = {
                parameter['ID']: self.id_enterprise
            }
            self.wsdl_client = await self.make_client(self.wsdl)

        customer_model = CustomerInSelect(
            user_id=self.user_id
        )
        customer: CustomerInResponse = await get_customer_db(self.conn, customer_model)
        current_loans: List[LoanInDb] = await select_loans(self.conn, LoanInSelect(persons_id=customer.persons_id, loans_status_id=11))  # get only loans with data credito success

        if len(current_loans) > 0:
            self.current_loan = current_loans[len(current_loans)-1]

        if self.wsdl_client:

            logger.info(f'[protect data] ObtenerRT parameter: {request_data}')
            tasks = [
                self.wsdl_client.service.ObtenerRT(**request_data)
            ]

            future = asyncio.gather(*tasks, return_exceptions=True)
            if only_real_time:
                future.add_done_callback(handle_future_real_time)
            elif only_rand_numbers:
                future.add_done_callback(handle_future_rand_numbers)
            elif only_audio:
                future.add_done_callback(handle_future_get_audio)
            elif verify_audio:
                future.add_done_callback(handle_future_verify_audio)
            elif document_signature:
                future.add_done_callback(handle_future_document_signature)
            elif document_signature_through_call:
                future.add_done_callback(handle_future_document_signature_through_call)
            elif get_document_status:
                future.add_done_callback(handle_future_get_document_status)
            elif cancel_document:
                future.add_done_callback(handle_future_cancel_document)
            else:
                future.add_done_callback(handle_future_otp)

    def get_one_time_pin(self, select: ProtectDataOTPInSelect):
        def handle_future(future_obj):
            result = str(future_obj.result()[0]).split("*")

            if result[0] == '1':  # on success response
                logger.info(f'[protect data] GenerarCodigo on success response: {result} OTP: {result[1]}')
                #  do something with the OTP code
            else:  # on error response
                logger.info(f'[protect data] GenerarCodigo on error response: {result} description {result[1]}')

        # generate a HASH SHA256
        api_hash = self.rt + self.id_enterprise + select.nip + select.email + select.phone_number + self.password
        api_hash_encrypt = self.encrypt_string(api_hash)
        logger.info(f'api_hash_encrypt: {api_hash_encrypt}')
        api_hash_encrypt_base64 = self.encrypt_base64(api_hash_encrypt)
        logger.info(f'api_hash_encrypt_base64: {api_hash_encrypt_base64}')

        # build parameter object with id of enterprise give from ProtecData
        request_data = {
            parameter['PD_ID']: self.id_enterprise,
            parameter['NIP']: select.nip,
            parameter['PD_EMAIL']: select.email,
            parameter['PD_CELLPHONE']: select.phone_number,
            parameter['PD_RT']: self.rt,
            parameter['API']: api_hash_encrypt_base64
        }

        logger.info(f'[protect data] GenerarCodigo parameter: {request_data}')
        tasks = [
            self.wsdl_client.service.GenerarCodigo(**request_data),
        ]

        future = asyncio.gather(*tasks, return_exceptions=True)
        future.add_done_callback(handle_future)

    def storage_random_numbers(self, random_numbers):
        logger.info(f'[ProtectDataClient].storage_random_numbers() random_numbers: {random_numbers}')
        loop = self.get_running_loop()

        if loop and loop.is_running():
            loop.create_task(
                insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                    api_id=1,  # ProtecData id
                    user_id=self.user_id,
                    api_log={
                        "logType": "randomNumbers",
                        "info": json.dumps(random_numbers)
                    }
                ))
            )
        else:
            asyncio.run(
                insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                    api_id=1,  # ProtecData id
                    user_id=self.user_id,
                    api_log={
                        "logType": "randomNumbers",
                        "info": json.dumps(random_numbers)
                    }
                ))
            )

    def update_loan_status(self, loans_status_id: int):
        if self.current_loan:
            loop = self.get_running_loop()
            if loop and loop.is_running():
                loop.create_task(update_loan(self.conn, LoanInUpdate(financial_loans_id=self.current_loan.financial_loans_id, loans_status_id=loans_status_id)))
            else:
                asyncio.run(update_loan(self.conn, LoanInUpdate(financial_loans_id=self.current_loan.financial_loans_id, loans_status_id=loans_status_id)))
        else:
            logger.error(f'[Protect Data] Error this user {self.user_id} no have credits approved by DataCredito')

    def send_message_to_ws(self, view: str, data):
        logger.info(f'[ProtectDataClient].send_message_to_ws() data: {data}')
        socket_handler = SocketHandler(self.user_id)

        loop = self.get_running_loop()
        if loop and loop.is_running():
            loop.create_task(socket_handler.send_message_to_user({'view': view, 'data': data}))
        else:
            asyncio.run(socket_handler.send_message_to_user({'view': view, 'data': data}))

    def get_random_numbers(self):
        def handle_future(future_obj):
            try:
                logger.info(f'[ProtectDataClient].handle_future(): {future_obj.result()}')
                self.random_numbers = str(future_obj.result()[0]).split("*")
                # storage into db the received numbers from ProtecData
                self.storage_random_numbers(self.random_numbers)
                # send numbers to user
                self.send_message_to_ws(view='ProtecDataNumbersModal', data={'count': '1', 'numbers': self.random_numbers[0]})
                time.sleep(2)
                self.send_message_to_ws(view='ProtecDataNumbersModal', data={'count': '2', 'numbers': self.random_numbers[1]})
            except Exception as ex:
                logger.error(f'PROTEC DATA error getting numbers. Exception: {ex}')
                self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocError'})
                self.update_loan_status(loans_status_id=13)  # Protec Data error

        # generate a HASH SHA256
        api_hash: str = self.rt + self.id_enterprise + self.password
        api_hash_encrypt = self.encrypt_string(api_hash)
        logger.info(f'[ProtectDataClient].get_random_numbers() api_hash_encrypt: {api_hash_encrypt}')
        api_hash_encrypt_base64 = self.encrypt_base64(api_hash_encrypt)
        logger.info(f'[ProtectDataClient].get_random_numbers() api_hash_encrypt_base64: {api_hash_encrypt_base64}')

        # build parameter object with id of enterprise give from ProtecData
        request_data = {
            parameter['PD_ID']: self.id_enterprise,
            parameter['PD_RT']: self.rt,
            parameter['API']: api_hash_encrypt_base64
        }

        logger.info(f'[ProtectDataClient].get_random_numbers() parameter: {request_data}')
        tasks = [
            self.wsdl_client.service.ObtenerNumeros(**request_data),
        ]

        future = asyncio.gather(*tasks, return_exceptions=True)
        future.add_done_callback(handle_future)

    def get_audio(self, select: ProtectDataGetAudioInSelect):
        def handle_future(future_obj):
            try:
                response = future_obj.result()
                if response[0] == 'Exitoso':
                    logger.info(f'Success --> [ProtectDataClient].handle_future(): {response}')
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'AudioSuccess', 'AudioCount': select.audio_count, 'Duration': '5000'})
                else:
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'AudioError', 'Duration': '7000'})
            except Exception as ex:
                logger.error(f'PROTEC DATA error getting audio. Exception: {ex}')
                self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'AudioError'})
                self.update_loan_status(loans_status_id=13)  # Protec Data error

        # generate a HASH SHA256
        api_hash: str = self.rt + select.nip + '_' + str(select.audio_count) + self.id_enterprise + select.file_base64 + self.password
        api_hash_encrypt = self.encrypt_string(api_hash)
        logger.info(f'[ProtectDataClient].get_audio() api_hash_encrypt: {api_hash_encrypt}')
        api_hash_encrypt_base64 = self.encrypt_base64(api_hash_encrypt)
        logger.info(f'[ProtectDataClient].get_audio() api_hash_encrypt_base64: {api_hash_encrypt_base64}')

        # build parameter object with id of enterprise give from ProtecData
        request_data = {
            parameter['NIP']: select.nip + '_' + str(select.audio_count),
            parameter['PD_ID']: self.id_enterprise,
            parameter['PD_FILE_BASE64']: select.file_base64,
            parameter['PD_RT']: self.rt,
            parameter['API']: api_hash_encrypt_base64
        }

        logger.info(f'[ProtectDataClient].get_audio() parameter: {request_data}')
        tasks = [
            self.wsdl_client.service.GetAudio(**request_data),
        ]

        future = asyncio.gather(*tasks, return_exceptions=True)
        future.add_done_callback(handle_future)

    def verify_audio(self, select: ProtectDataVerifyAudioInSelect):
        def handle_future(future_obj):
            try:
                response = str(future_obj.result()[0]).split("*")
                if response[0] == '1':
                    logger.info(f'Success --> [ProtectDataClient].verify_audio.handle_future(): {response}')
                    self.send_message_to_ws(view='ShowToastMessage',
                                            data={'Type': 'AudioVerificationSuccess', 'Duration': '7000'})

                    loop = self.get_running_loop()
                    if loop and loop.is_running():
                        loop.create_task(
                            self.start(select_document_signature=ProtectDataDocumentSignatureInSelect(
                                user_id=self.user_id
                            ), document_signature=True)
                        )
                    else:
                        asyncio.run(
                            self.start(select_document_signature=ProtectDataDocumentSignatureInSelect(
                                user_id=self.user_id
                            ), document_signature=True)
                        )
                elif response[0] == '2':
                    logger.error(f'Voice Not Recognized --> [ProtectDataClient].verify_audio.handle_future(): {response}')
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'AudioVerificationError', 'Duration': '7000'})
                    self.update_loan_status(loans_status_id=13)  # Protec Data error
                elif response[0] == '3':
                    logger.error(f'Please repeat the process --> [ProtectDataClient].verify_audio.handle_future(): {response}')
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'AudioVerificationError', 'Duration': '7000'})
                    self.update_loan_status(loans_status_id=13)  # Protec Data error
            except Exception as ex:
                logger.error(f'PROTEC DATA error verifying audio. Exception: {ex}')
                self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'AudioError'})
                self.update_loan_status(loans_status_id=13)  # Protec Data error

        # generate a HASH SHA256
        api_hash = self.rt + select.nip + self.id_enterprise + str(select.num1) + str(select.num2) + self.password
        api_hash_encrypt = self.encrypt_string(api_hash)
        logger.info(f'[ProtectDataClient].verify_audio() api_hash_encrypt: {api_hash_encrypt}')
        api_hash_encrypt_base64 = self.encrypt_base64(api_hash_encrypt)
        logger.info(f'[ProtectDataClient].verify_audio() api_hash_encrypt_base64: {api_hash_encrypt_base64}')

        # build parameter object with id of enterprise give from ProtecData
        request_data = {
            parameter['NIP']: select.nip,
            parameter['PD_ID']: self.id_enterprise,
            parameter['PD_NUM1']: select.num1,
            parameter['PD_NUM2']: select.num2,
            parameter['PD_RT']: self.rt,
            parameter['API']: api_hash_encrypt_base64
        }

        logger.info(f'[ProtectDataClient].verify_audio() parameter: {request_data}')
        tasks = [
            self.wsdl_client.service.VerificarVoz(**request_data),
        ]

        future = asyncio.gather(*tasks, return_exceptions=True)
        future.add_done_callback(handle_future)

    async def document_signature(self, select: ProtectDataDocumentSignatureInSelect):
        def handle_future(future_obj):
            try:
                response = str(future_obj.result()[0]).split("*")

                if response[0] == '1':
                    logger.info(f'Success --> [ProtectDataClient].document_signature.handle_future(). Now to to insert loan')
                    # update loan status id
                    self.update_loan_status(loans_status_id=14)  # Protec Data success
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(
                            insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocument", "info": response[1]}
                            ))
                        )
                    else:
                        asyncio.run(
                            insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocument","info": response[1]}
                            ))
                        )

                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocSuccess', 'Duration': '10000'})
                elif response[0] == '2':
                    logger.info(f'Error On Signed Document --> [ProtectDataClient].document_signature.handle_future(): {response}')
                    # update loan status id
                    self.update_loan_status(loans_status_id=13)  # Protec Data error
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocError'})
                else:
                    logger.info(f'Error --> [ProtectDataClient].document_signature.handle_future(): {response}')
                    # update loan status id
                    self.update_loan_status(loans_status_id=13)  # Protec Data error
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'signedDocError'})
            except Exception as ex:
                logger.error(f'PROTEC DATA error while signature document. Exception: {ex}')
                self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocError'})
                self.update_loan_status(loans_status_id=13)  # Protec Data error

        customer_model = CustomerInSelect(
            user_id=self.user_id
        )
        customer: CustomerInResponse = await get_customer_db(self.conn, customer_model)
        documents: List[DocumentInDb] = await select_documents(conn=self.conn, select=DocumentInSelect(name='client-contract'), customer=customer)
        html_document = json.loads(json.dumps(documents[0].content))
        customer_contact = customer.contact_information[0]

        document_processing = DocumentProcessing(pg_conn=self.conn, user_id=self.user_id, redis_conn=self.redis)
        render_document = await document_processing.template(document=html_document['html'], customer=customer)

        # get the numbers from db
        api_log: List[ApiTransactionLogInDb] = await select_api_transaction_log(self.conn, ApiTransactionLogInSelect(
            api_id=1, user_id=self.user_id
        ), 'randomNumbers')

        # noinspection PyTypeChecker
        numbers = json.loads(api_log[0].api_log["info"])
        departament_in_select = DepartamentInSelect(
            departament_id=customer.current_departament_id
        )
        departaments: List[Departament] = await get_departaments_db(conn=self.conn, select=departament_in_select)
        cities_in_select = CityInSelect(
            city_id=customer.current_city_id,
            departament_id=customer.current_departament_id
        )
        cities: List[City] = await get_cities_db(self.conn, cities_in_select)

        select.nip = customer.identification
        select.name = customer.name
        select.middle_name = customer.middle_name
        select.first_name = customer.first_name
        select.last_name = customer.last_name
        select.telephone = customer_contact.cellphone
        select.email = customer_contact.customer_email
        select.departament_name = departaments[0].name
        select.city_name = cities[0].name
        select.address = customer_contact.address
        select.document_id = documents[0].document_id
        select.document_name = documents[0].name
        select.document = render_document
        select.num1 = numbers[0]
        select.num2 = numbers[1]

        # generate a HASH SHA256
        api_hash: str = self.rt + self.id_enterprise + self.password
        api_hash_encrypt: bytes = self.encrypt_string(api_hash)
        logger.info(f'[ProtectDataClient].document_signature() api_hash_encrypt: {api_hash_encrypt}')
        api_hash_encrypt_base64: str = self.encrypt_base64(api_hash_encrypt)
        logger.info(f'[ProtectDataClient].document_signature() api_hash_encrypt_base64: {api_hash_encrypt_base64}')

        # get pdf document in bytes
        pdf = pdfkit.from_string(input=select.document, output_path=False, options={'quiet': '', 'encoding': "UTF-8"})

        # build parameter object with id of enterprise give from ProtecData
        request_data = {
            parameter['PD_ID']: self.id_enterprise,
            parameter['PD_PERSONAL_INFO']: json.dumps([{
                "IdFirmante": select.user_id,
                "Titulo": "Deudor",
                "NIP": select.nip,
                "Nombres": select.name + ' ' + select.middle_name,
                "Apeliidos": select.first_name + ' ' + select.last_name,
                "Direccion": select.departament_name + ', ' + select.city_name + ', ' + select.address,
                "Telefono": "57"+select.telephone,
                "Correo": select.email,
                "Numeros": str(select.num1) + ',' + str(select.num2),
                "ListaDocumentos": [{
                    "IdPlantilla": str(select.document_id),
                    "Descripcion": select.document_name,
                    "FirmarConUnaToma": "True",
                    "GuardarDocumentoEnWeb": "True",
                    "Archivo": self.encrypt_base64(pdf)
                }]
            }]),
            parameter['PD_RT']: self.rt,
            parameter['API']: api_hash_encrypt_base64
        }

        logger.info(f'[ProtectDataClient].document_signature() parameter: {request_data}')
        tasks = [
            self.wsdl_client.service.FirmarDocumento(**request_data),
        ]

        future = asyncio.gather(*tasks, return_exceptions=True)
        future.add_done_callback(handle_future)

    async def document_signature_through_call(self, select: ProtectDataDocumentSignatureInSelect):
        def handle_future(future_obj):
            try:
                response = str(future_obj.result()[0]).split("*")

                if response[0] == '-1':
                    logger.error(f'PROTEC DATA document_signature_through_call response -1, message: {response[1]}')
                    self.update_loan_status(loans_status_id=13)  # Protec Data error
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocError'})
                elif response[0] == '3':
                    logger.error(f'PROTEC DATA document_signature_through_call response 3, message: {response[1]}')
                    self.update_loan_status(loans_status_id=13)  # Protec Data error
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocError'})
                else:
                    logger.info(f'PROTEC DATA document_signature_through_call response number: {response[0]}, transaction id: {response[1]}')
                    number_to_display: str = response[0]
                    transaction_id: str = response[1]
                    self.send_message_to_ws(view='ProtecDataDocumentNumberModal', data={'documentNumberToShow': number_to_display, 'transactionNumber': transaction_id})
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(
                            insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "documentTransactionId", "info": transaction_id}
                            ))
                        )
                    else:
                        asyncio.run(
                            insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "documentTransactionId", "info": transaction_id}
                            ))
                        )
            except Exception as ex:
                logger.error(f'PROTEC DATA error while signature document. Exception: {ex}')
                self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocError'})
                self.update_loan_status(loans_status_id=13)  # Protec Data error

        customer_model = CustomerInSelect(
            user_id=self.user_id
        )
        customer: CustomerInResponse = await get_customer_db(self.conn, customer_model)
        # todo check which contract get
        documents: List[DocumentInDb] = await select_documents(conn=self.conn, select=DocumentInSelect(name='client-contract'), customer=customer)
        customer_contact = customer.contact_information[0]
        html_document = json.loads(json.dumps(documents[0].content))

        document_processing = DocumentProcessing(pg_conn=self.conn, user_id=self.user_id, redis_conn=self.redis)
        render_document = await document_processing.template(document=html_document['html'], customer=customer)

        departament_in_select = DepartamentInSelect(
            departament_id=customer.current_departament_id
        )
        departaments: List[Departament] = await get_departaments_db(conn=self.conn, select=departament_in_select)
        cities_in_select = CityInSelect(
            city_id=customer.current_city_id,
            departament_id=customer.current_departament_id
        )
        cities: List[City] = await get_cities_db(self.conn, cities_in_select)

        select.nip = customer.identification
        select.name = customer.name
        select.middle_name = customer.middle_name
        select.first_name = customer.first_name
        select.last_name = customer.last_name
        select.telephone = customer_contact.cellphone
        select.email = customer_contact.customer_email
        select.departament_name = departaments[0].name
        select.city_name = cities[0].name
        select.address = customer_contact.address
        select.document_id = documents[0].document_id
        select.document_name = documents[0].name
        select.document = render_document

        # get pdf document in bytes
        pdf = pdfkit.from_string(input=select.document, output_path=False, options={'quiet': '', 'encoding': "UTF-8"})

        # build parameter object with id of enterprise give from ProtecData
        request_data = {
            parameter['PD_PERSONAL_INFO']: json.dumps([{
                "IdFirmante": select.user_id,
                "Titulo": "Deudor",
                "NIP": select.nip,
                "Nombres": select.name + ' ' + select.middle_name,
                "Apeliidos": select.first_name + ' ' + select.last_name,
                "Direccion": select.departament_name + ', ' + select.city_name + ', ' + select.address,
                "Telefono": "57"+select.telephone,
                "Correo": select.email,
                "TransaccionEmpresa": select.nip,
                "ListaDocumentos": [{
                    "IdPlantilla": str(select.document_id),
                    "Descripcion": select.document_name,
                    "FirmarConUnaToma": "True",
                    "GuardarDocumentoEnWeb": "True",
                    "Archivo": self.encrypt_base64(pdf)
                }]
            }]),
            parameter['PD_GENERATE_PHONE_CALL']: 1,
            parameter['PD_USER_CONSUME']: self.user_consume,
            parameter['PD_ID']: self.id_enterprise_consume,
            parameter['PD_RT']: self.rt
        }

        # generate a HASH SHA256
        api_hash: str = self.rt + request_data[parameter['PD_PERSONAL_INFO']] + self.user_consume + self.id_enterprise_consume + self.password_consume
        api_hash_encrypt: bytes = self.encrypt_string(api_hash)
        logger.info(f'[ProtectDataClient].document_signature() api_hash_encrypt: {api_hash_encrypt}')
        api_hash_encrypt_base64: str = self.encrypt_base64(api_hash_encrypt)
        logger.info(f'[ProtectDataClient].document_signature() api_hash_encrypt_base64: {api_hash_encrypt_base64}')

        request_data[parameter['API']] = api_hash_encrypt_base64

        logger.info(f'[ProtectDataClient].document_signature() parameter: {request_data}')
        tasks = [
            self.wsdl_client.service.GenerarSolicitudMasiva(**request_data),
        ]

        future = asyncio.gather(*tasks, return_exceptions=True)
        future.add_done_callback(handle_future)

    async def get_document_status(self, transaction_id: str):
        def handle_future(future_obj):
            try:
                response = str(future_obj.result()[0]).split("*")

                if response[0] == '0':
                    logger.info(f'PROTEC DATA get_document_status response :{response[0]}, message: {response[1]}')
                    self.update_loan_status(loans_status_id=14)  # Protec Data success
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocSuccess', 'Duration': '10000'})
                    loop = self.get_running_loop()

                    if loop and loop.is_running():  # storage the client contract signed in db
                        loop.create_task(
                            insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocument", "info": response[1]}
                            ))
                        )
                    else:
                        asyncio.run(
                            insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocument", "info": response[1]}
                            ))
                        )
                elif response[0] == '1':
                    logger.info(f'PROTEC DATA get_document_status response processing document: {response[1]}')
                    self.send_message_to_ws(view='ProtecDataRestartTimer', data={})
                elif response[0] == '2':
                    logger.info(f'PROTEC DATA get_document_status response does not exists request for this transaction: {response[1]}')
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'DocumentDoesNotExist', 'Duration': '10000'})
                    loop = self.get_running_loop()
                    if loop and loop.is_running():
                        loop.create_task(insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocumentError", "info": response[0]}
                            )))
                    else:
                        asyncio.run(insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocumentError", "info": response[0]}
                            )))
                elif response[0] == '4':
                    logger.info(f'PROTEC DATA get_document_status response transaction id already expired: {response[1]}')
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'DocumentNumberExpired', 'Duration': '10000'})
                    loop = self.get_running_loop()
                    if loop and loop.is_running():
                        loop.create_task(insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocumentError", "info": response[0]}
                            )))
                    else:
                        asyncio.run(insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocumentError", "info": response[0]}
                            )))
                else:
                    logger.info(f'PROTEC DATA get_document_status response: {response[0]}, message: {response[1]}')
                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocError'})
                    loop = self.get_running_loop()
                    if loop and loop.is_running():
                        loop.create_task(insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocumentError", "info": response[0]}
                            )))
                    else:
                        asyncio.run(insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                                api_id=1, user_id=self.user_id, api_log={"logType": "signedDocumentError", "info": response[0]}
                            )))

            except Exception as ex:
                logger.error(f'PROTEC DATA error while signature document. Exception: {ex}')
                self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'SignedDocError'})
                self.update_loan_status(loans_status_id=13)  # Protec Data error

        # generate a HASH SHA256
        api_hash: str = self.rt + transaction_id + self.user_consume + self.id_enterprise_consume + self.password_consume
        api_hash_encrypt: bytes = self.encrypt_string(api_hash)
        logger.info(f'[ProtectDataClient].document_signature() api_hash_encrypt: {api_hash_encrypt}')
        api_hash_encrypt_base64: str = self.encrypt_base64(api_hash_encrypt)
        logger.info(f'[ProtectDataClient].document_signature() api_hash_encrypt_base64: {api_hash_encrypt_base64}')

        # build parameter object with id of enterprise give from ProtecData
        request_data = {
            parameter['PD_TRANSACTION_ID']: transaction_id,
            parameter['PD_USER_CONSUME']: self.user_consume,
            parameter['PD_ID']: self.id_enterprise_consume,
            parameter['PD_RT']: self.rt,
            parameter['API']: api_hash_encrypt_base64
        }

        logger.info(f'[ProtectDataClient].document_signature() parameter: {request_data}')
        tasks = [
            self.wsdl_client.service.ConsultaEstado(**request_data),
        ]

        future = asyncio.gather(*tasks, return_exceptions=True)
        future.add_done_callback(handle_future)

    async def cancel_transaction(self, nip: str, transaction_id: str):
        def handle_future(future_obj):
            response = str(future_obj.result()[0]).split("*")
            print('<<<<<<<<<<<<<<<<<<<<<<<<<')
            print(response)
            print('>>>>>>>>>>>>>>>>>>>>>>>>>>')

        # generate a HASH SHA256
        api_hash: str = self.rt + nip + transaction_id + self.user_consume + self.id_enterprise_consume + self.password_consume
        api_hash_encrypt: bytes = self.encrypt_string(api_hash)
        logger.info(f'[ProtectDataClient].document_signature() api_hash_encrypt: {api_hash_encrypt}')
        api_hash_encrypt_base64: str = self.encrypt_base64(api_hash_encrypt)
        logger.info(f'[ProtectDataClient].document_signature() api_hash_encrypt_base64: {api_hash_encrypt_base64}')

        # build parameter object with id of enterprise give from ProtecData
        request_data = {
            parameter['NIP']: nip,
            parameter['PD_TRANSACTION_ID']: transaction_id,
            parameter['PD_USER_CONSUME']: self.user_consume,
            parameter['PD_ID']: self.id_enterprise_consume,
            parameter['PD_RT']: self.rt,
            parameter['API']: api_hash_encrypt_base64
        }

        logger.info(f'[ProtectDataClient].document_signature() parameter: {request_data}')
        tasks = [
            self.wsdl_client.service.CancelarTransaccion(**request_data),
        ]

        future = asyncio.gather(*tasks, return_exceptions=True)
        future.add_done_callback(handle_future)

    async def make_client(self, endpoint):
        try:
            transport = AsyncTransport(asyncio.get_event_loop())
            async with transport.session.get(endpoint) as resp:
                content = await resp.read()
                wsdl = io.BytesIO(content)
                return zeep.Client(wsdl, transport=transport)
        except Exception as ex:
            logger.error(f'Error while connect to ProtecData WSDL {ex}')
            self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'ProtecDataErrorConnection', 'Duration': '8000'})
            return None

    @staticmethod
    def get_running_loop():
        try:
            loop = asyncio.get_running_loop()
        except RuntimeError:  # if cleanup: 'RuntimeError: There is no current event loop..'
            loop = None
        return loop

    @staticmethod
    def encrypt_string(hash_string: str) -> bytes:
        return hashlib.sha256(hash_string.encode()).digest()

    @staticmethod
    def encrypt_base64(string_bytes) -> str:
        return base64.b64encode(string_bytes).decode()
