import base64
import json
import os
import asyncio
import xmltodict
import pytz
import datetime
from aioredis import Redis

import zeep
import operator
import xml.sax.saxutils as saxutils
import math

from distutils.util import strtobool
from databases import Database
from requests import Session
from loguru import logger
from xml.etree import cElementTree as ElementTree
from zeep.transports import Transport
from zeep.wsse.username import UsernameToken

from mamut.bl.socket_handler import SocketHandler
from mamut.bl.loans_configuration import LoanConfigurationHandler
from mamut.bl.loan_handler import LoanHandler
from mamut.bl.calculator import calculate_loan
from mamut.conn.redis.redis import connect_redis_database, close_redis_connection

from mamut.core.config import DATA_CREDITO_HC2_WSDL, DATA_CREDITO_EVIDENTE_WSDL, DATA_CREDITO_PASSWORD, \
    DATA_CREDITO_OKTA_USER, DATA_CREDITO_OKTA_PASS, \
    TRIBBU_SYSTEMS_CERT, DATA_CREDITO_PRODUCT_HC2, DEBUG, APP_TIMEZONE, MAMUTSOLUCIONES_SSL_PUBLIC, \
    MAMUTSOLUCIONES_SSL_KEY, MAMUTSOLUCIONES_SSL_CRT, DATA_CREDITO_NIP, DATACREDITO_DEBUG
from mamut.core.utils import BinarySignatureTimestamp, CustomSignature, XmlDictConfig
from mamut.crud.api import insert_api_transaction_log
from mamut.crud.customers import get_customer_db, update_client_db
from mamut.models.api import ApiTransactionLogInInsert
from mamut.models.customer import CustomerInSelect, CustomerInResponse, CustomerInUpdate
from mamut.models.data_credito import DataCreditLoanHistoryInSelect, EvidenteValidarInSelect, EvidentePreguntasInSelect, EvidenteVerificarInSelect
from mamut.models.loans_configuration import LoansConfigurationInInsert
path = os.path.dirname(os.path.realpath(__file__))


class DataCreditoEvidenteOTP:
    def __init__(self, conn: Database, user_id: int, redis_conn: Redis = None):
        self.conn = conn
        self.user_id = user_id
        self.wsdl_hc2 = DATA_CREDITO_HC2_WSDL
        self.password = DATA_CREDITO_PASSWORD
        self.product_hc2 = DATA_CREDITO_PRODUCT_HC2
        self.okta_user = DATA_CREDITO_OKTA_USER
        self.okta_pass = DATA_CREDITO_OKTA_PASS
        self.wsdl_client = zeep.Client
        self.wsdl_evidente = DATA_CREDITO_EVIDENTE_WSDL
        self.param_product_evidente = '3220'
        self.product_evidente = '010'
        self.channel_evidente = '001'
        self.redis_loan_amount = 0
        self.redis_conn = redis_conn
        self.loan_handler = None

        if self.redis_conn is None:
            logger.info('Redis connection is not available.')

    @staticmethod
    def compile_async_tasks(task):
        # you can add many task to exec async
        tasks = [
            asyncio.ensure_future(task)
        ]
        return tasks

    @staticmethod
    def get_running_loop():
        try:
            loop = asyncio.get_running_loop()
        except RuntimeError:  # if cleanup: 'RuntimeError: There is no current event loop..'
            loop = None
        return loop

    @staticmethod
    def encrypt_base64(string_bytes) -> str:
        return base64.b64encode(string_bytes).decode()

    async def make_client(self, endpoint):
        try:
            public_cert = os.path.join(path, "config", MAMUTSOLUCIONES_SSL_PUBLIC)
            private_key = os.path.join(path, "config", MAMUTSOLUCIONES_SSL_KEY)

            user_name_token = UsernameToken(self.okta_user, self.okta_pass)
            signature = BinarySignatureTimestamp(key_file=private_key, certfile=public_cert)
            session = Session()
            session.verify = False
            session.cert = os.path.join(path, "config", MAMUTSOLUCIONES_SSL_CRT)
            transport = Transport(session=session)

            client = zeep.Client(endpoint, transport=transport, wsse=CustomSignature([user_name_token, signature]))
            return client
        except Exception as ex:
            logger.error(f'Error while connect to DataCredito WSDL {ex}')
            self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'EvidenteVerificarError', 'Duration': '7000'})
            return None

    def send_message_to_ws(self, view: str, data):
        logger.info(f'[DataCreditoClient].send_message_to_ws() data: {data}')
        socket_handler = SocketHandler(self.user_id)

        loop = self.get_running_loop()
        if loop and loop.is_running():
            loop.create_task(socket_handler.send_message_to_user({'view': view, 'data': data}))
        else:
            asyncio.run(socket_handler.send_message_to_user({'view': view, 'data': data}))

    # EVIDENTE integration methods
    async def generate_otp_code(self, select: EvidenteValidarInSelect):
        # build parameter object with id of enterprise give from DataCredito
        request_data = {
            'idUsuarioEntidad': self.encrypt_base64(DATA_CREDITO_NIP.encode('utf-8')),
            'xmlGenerarCodigoOTP': {
                'codProducto': self.product_evidente,
                'codParametrizacion': self.param_product_evidente,
                'nit': select.nip,
                'identificacion': {
                    'numero': select.nip,
                    'tipo': select.identificationType
                },
            }
        }

        logger.info(f'[Data Credito] Evidente Generate OTP code parameter: {request_data}')
        response = self.wsdl_client.service.generarCodigoOTP(**request_data)

        return response

    @staticmethod
    def timestamp_from_datetime(dt: datetime):
        return dt.replace(tzinfo=pytz.timezone(APP_TIMEZONE)).timestamp()
