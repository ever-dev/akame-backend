import base64
import json
import os
import asyncio
import xmltodict
import pytz
import datetime
from aioredis import Redis

import zeep
import operator
import xml.sax.saxutils as saxutils
import math

from distutils.util import strtobool
from databases import Database
from requests import Session
from loguru import logger
from xml.etree import cElementTree as ElementTree
from zeep.transports import Transport
from zeep.wsse.username import UsernameToken

from mamut.bl.socket_handler import SocketHandler
from mamut.bl.loans_configuration import LoanConfigurationHandler
from mamut.bl.loan_handler import LoanHandler
from mamut.bl.calculator import calculate_loan
from mamut.models.user import UserInSelect
from mamut.conn.redis.redis import connect_redis_database, close_redis_connection

from mamut.core.config import DATA_CREDITO_HC2_WSDL, DATA_CREDITO_EVIDENTE_WSDL, DATA_CREDITO_PASSWORD, \
    DATA_CREDITO_OKTA_USER, DATA_CREDITO_OKTA_PASS, \
    TRIBBU_SYSTEMS_CERT, DATA_CREDITO_PRODUCT_HC2, DEBUG, APP_TIMEZONE, MAMUTSOLUCIONES_SSL_PUBLIC, \
    MAMUTSOLUCIONES_SSL_KEY, MAMUTSOLUCIONES_SSL_CRT, DATA_CREDITO_NIP, DATACREDITO_DEBUG
from mamut.core.utils import BinarySignatureTimestamp, CustomSignature, XmlDictConfig
from mamut.crud.user import select_users
from mamut.crud.api import insert_api_transaction_log
from mamut.crud.customers import get_customer_db, update_client_db
from mamut.models.api import ApiTransactionLogInInsert
from mamut.models.customer import CustomerInSelect, CustomerInResponse, CustomerInUpdate
from mamut.models.data_credito import DataCreditLoanHistoryInSelect, EvidenteValidarInSelect, EvidentePreguntasInSelect, EvidenteVerificarInSelect
from mamut.models.loans_configuration import LoansConfigurationInInsert
path = os.path.dirname(os.path.realpath(__file__))


class DataCreditoClient:
    def __init__(self, conn: Database, user_id: int, redis_conn: Redis = None):
        self.conn = conn
        self.user_id = user_id
        self.wsdl_hc2 = DATA_CREDITO_HC2_WSDL
        self.password = DATA_CREDITO_PASSWORD
        self.product_hc2 = DATA_CREDITO_PRODUCT_HC2
        self.okta_user = DATA_CREDITO_OKTA_USER
        self.okta_pass = DATA_CREDITO_OKTA_PASS
        self.wsdl_client = zeep.Client
        self.wsdl_evidente = DATA_CREDITO_EVIDENTE_WSDL
        self.param_product_evidente = '3220'
        self.product_evidente = '010'
        self.channel_evidente = '001'
        self.redis_loan_amount = 0
        self.redis_conn = redis_conn
        self.loan_handler = None

        if self.redis_conn is None:
            logger.info('Redis connection is not available.')

    def storage_financial_history(self, info):
        loop = self.get_running_loop()

        if loop and loop.is_running():
            loop.create_task(
                insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                    api_id=2,  # DataCredito id
                    user_id=self.user_id,
                    api_log={
                        "logType": "financialHistory",
                        "info": json.dumps(info)
                    }
                ))
            )
        else:
            asyncio.run(
                insert_api_transaction_log(self.conn, ApiTransactionLogInInsert(
                    api_id=2,  # DataCredito id
                    user_id=self.user_id,
                    api_log={
                        "logType": "financialHistory",
                        "info": json.dumps(info)
                    }
                ))
            )

    @staticmethod
    def compile_async_tasks(task):
        # you can add many task to exec async
        tasks = [
            asyncio.ensure_future(task)
        ]
        return tasks

    @staticmethod
    def get_running_loop():
        try:
            loop = asyncio.get_running_loop()
        except RuntimeError:  # if cleanup: 'RuntimeError: There is no current event loop..'
            loop = None
        return loop

    @staticmethod
    def encrypt_base64(string_bytes) -> str:
        return base64.b64encode(string_bytes).decode()

    async def make_client(self, endpoint):
        try:
            public_cert = os.path.join(path, "config", MAMUTSOLUCIONES_SSL_PUBLIC)
            private_key = os.path.join(path, "config", MAMUTSOLUCIONES_SSL_KEY)

            user_name_token = UsernameToken(self.okta_user, self.okta_pass)
            signature = BinarySignatureTimestamp(key_file=private_key, certfile=public_cert)
            session = Session()
            session.verify = False
            session.cert = os.path.join(path, "config", MAMUTSOLUCIONES_SSL_CRT)
            transport = Transport(session=session)

            client = zeep.Client(endpoint, transport=transport, wsse=CustomSignature([user_name_token, signature]))
            return client
        except Exception as ex:
            logger.error(f'Error while connect to DataCredito WSDL {ex}')
            self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'EvidenteVerificarError', 'Duration': '7000'})
            return None

    async def start(self
                    , select_financial_history: DataCreditLoanHistoryInSelect = DataCreditLoanHistoryInSelect()
                    , is_financial_history: bool = False
                    , select_evidente_validar: EvidenteValidarInSelect = EvidenteValidarInSelect()
                    , is_evidente_validar: bool = False
                    , select_evidente_preguntas: EvidentePreguntasInSelect = EvidentePreguntasInSelect()
                    , is_evidente_preguntas: bool = False
                    , select_evidente_verificar: EvidenteVerificarInSelect = EvidenteVerificarInSelect()
                    , is_evidente_verificar: bool = False
                    ):

        def handle_future_financial_history(future):
            result = saxutils.unescape(future.result()[0])
            logger.info(f'[Data Credito] consultarHC2 on Response {result}')

            # parse XML response
            root = ElementTree.XML(result)
            xml_dict = XmlDictConfig(root)
            score_accepted = answer_accepted = identification_accepted = credits_accepted = alerts_accepted = quanto_accepted = quanto_have_new_amount_loan = False
            counter = 0
            recommend_amount = 0
            recommend_amount_round = 0

            if xml_dict.keys() & {'Informe', 'respuesta'}:
                answer: int = xml_dict.get('Informe').get('respuesta')
                if int(answer) in (13, 14):
                    logger.info(f'[Data Credito] HC2 Answer Success : {answer}')
                    answer_accepted = True

                    if xml_dict.keys() & {'Informe', 'NaturalNacional', 'Identificacion', 'estado'}:
                        identification: int = xml_dict.get('Informe').get('NaturalNacional').get('Identificacion').get('estado')
                        if int(identification) in (00, 12):
                            logger.info(f'[Data Credito] HC2 Identification Success : {identification}')
                            identification_accepted = True

                            # Acierta integration
                            if xml_dict.keys() & {'Informe', 'Score', 'puntaje'}:
                                score: float = xml_dict.get('Informe').get('Score').get('puntaje')

                                if float(score) > 530 or int(answer) == 14:
                                    logger.info(f'[Data Credito] HC2 Acierta(Score) Success : {score}')
                                    score_accepted = True

                                    # HC2 Credit Behavior
                                    if xml_dict.keys() & {'Informe', 'InfoAgregada', 'Resumen', 'Comportamiento', 'Mes', 'comportamiento'}:
                                        for behavior in root.iter('Comportamiento'):
                                            for month in behavior.iter('Mes'):
                                                code = month.get('comportamiento')
                                                if str(code).strip() in ('N', 1, '-', ''):
                                                    logger.info(f'[Data Credito] HC2 Credits Success : {code}')
                                                    credits_accepted = True
                                                else:
                                                    logger.info(f'[Data Credito] HC2 Credits Error : {code}')
                                                    # go to create all loan structures in db
                                                    loop = self.get_running_loop()

                                                    if loop and loop.is_running():
                                                        loop.create_task(self.loan_handler.create_loan(status_id=8))  # Data Credit error
                                                    else:
                                                        asyncio.run(self.loan_handler.create_loan(status_id=8))  # Data Credit error
                                                    self.send_message_to_ws(view='ShowToastMessage', data={'Type': 'HC2Error', 'Duration': '7000'})
                                                    credits_accepted = False
                                                counter = counter + 1
                                                if counter == 3:
                                                    break

                                        # HC2 Alerts
                                        if xml_dict.keys() & {'Informe', 'Alerta', 'codigo'} and credits_accepted:
                                            for alert in root.iter('Alerta'):
                                                alert_code = alert.get('codigo')
                                                source_code = alert.find('Fuente').get('codigo')

                                                if int(alert_code) in (1, 4, 6, 8, 203, 303, 304, 999) and int(source_code) in (1, 3, 99003, 99001) or (DEBUG and int(alert_code) == 12): #[embargos & demandas] alert (101, 102) source code(2)
                                                    logger.info(f'[Data Credito] HC2 Alerts Success : {alert_code} source_code: {source_code}')
                                                    alerts_accepted = True                                                               
                                                elif int(alert_code) in (2, 3, 4, 7, 9, 10, 11, 12, 301, 302, 201, 202) and int(source_code) in (1, 3, 99003, 99001): 
                                                    logger.info(f'[Data Credito] HC2 Alerts Error : {alert_code} source_code: {source_code}')
                                                    alerts_accepted = False
                                                    # go to create all loan structures in db
                                                    loop = self.get_running_loop()

                                                    if loop and loop.is_running():
                                                        loop.create_task(self.loan_handler.create_loan(status_id=8))  # Data Credit error
                                                    else:
                                                        asyncio.run(self.loan_handler.create_loan(status_id=8))  # Data Credit error
                                                    break
                                                else:
                                                    logger.info(f'[Data Credito] HC2 Alerts Error : {alert_code} source_code: {source_code}')
                                                    alerts_accepted = False
                                                    # go to create all loan structures in db
                                                    loop = self.get_running_loop()

                                                    if loop and loop.is_running():
                                                        loop.create_task(self.loan_handler.create_loan(status_id=8))  # Data Credit error
                                                    else:
                                                        asyncio.run(self.loan_handler.create_loan(status_id=8))  # Data Credit error
                                                    break
                                        # HC2 Quanto
                                        if xml_dict.keys() & {'Informe', 'productosValores'} and alerts_accepted:
                                            product_code: int = xml_dict.get('Informe').get('productosValores').get('producto')
                                            valor1_code: float = xml_dict.get('Informe').get('productosValores').get('valor1')
                                        
                                            if int(product_code) == 62 and valor1_code is not None:

                                                if self.redis_loan_amount is not None and self.redis_loan_amount > 0:
                                                    valor1_code = (float(valor1_code) * 1000)
                                                    loan_amount: float = self.redis_loan_amount

                                                    default_min_amount: float = 250000
                                                    result_max_amount: bool = False
                                                    result_less_amount: bool = False
                                                    result_recommend_amount: bool = False
                                                    valid_amount = (float(loan_amount) * 2.5)
                                                    recommend_amount = (float(valor1_code) * 0.3)

                                                    if recommend_amount > 1000000:
                                                        recommend_amount = 1000000

                                                    valor1_code = round(float(valor1_code), 2)
                                                    valid_amount = round(float(valid_amount), 2)
                                                    recommend_amount = round(float(recommend_amount), 2)
                                                    # success
                                                    result_max_amount = True if (float(valor1_code) >= float(valid_amount)) else False
                                                    # new amount
                                                    result_less_amount = True if (float(valor1_code) < float(valid_amount)) else False
                                                    # valid amount
                                                    result_recommend_amount = True if (float(recommend_amount) > float(default_min_amount)) else False
                                                    recommend_amount = math.floor(recommend_amount)
                                                    # step amount
                                                    factor = (recommend_amount / step_amount)
                                                    # new round amount
                                                    recommend_amount_round = (math.floor(factor) * step_amount)

                                                    if result_max_amount is True and loan_amount > 0:
                                                        logger.info(f'[Data Credito] HC2 Quanto Success : {product_code}, Valor amount : {valor1_code}')
                                                        quanto_accepted = True
                                                    elif result_less_amount is True and result_recommend_amount is True and loan_amount > 0:
                                                        recommend_amount_round_formatted = "${:,.2f}".format(int(recommend_amount_round))
                                                        # recommend new amount client
                                                        logger.info(f'[Data Credito] HC2 Quanto Warning : {product_code}, Valor amount : {valor1_code}, New amount : {recommend_amount}, , New amount round: {recommend_amount_round}')

                                                        quanto_accepted = False
                                                        quanto_have_new_amount_loan = True

                                                        self.send_message_to_ws(view='DataCreditoNewConditionsModal', data={'amount': recommend_amount_round, 'amount_formatted': recommend_amount_round_formatted})

                                                        # go to create all loan structures in db
                                                        loop = self.get_running_loop()

                                                        if loop and loop.is_running():
                                                            loop.create_task(self.loan_handler.create_loan(status_id=10))  # Data Credit error
                                                        else:
                                                            asyncio.run(self.loan_handler.create_loan(status_id=10))  # Data Credit error
                                                    else:
                                                        logger.info(f'[Data Credito] HC2 Quanto Error : {product_code}, Valor amount : {valor1_code}')
                                                        quanto_accepted = False
                                                        # go to create all loan structures in db
                                                        loop = self.get_running_loop()

                                                        if loop and loop.is_running():
                                                            loop.create_task(self.loan_handler.create_loan(status_id=10))  # Data Credit error
                                                        else:
                                                            asyncio.run(self.loan_handler.create_loan(status_id=10))  # Data Credit error
                                                else:
                                                    logger.info(f'[Data Credito] HC2 Quanto Error : {product_code}, Valor amount : {valor1_code}')
                                                    quanto_accepted = False                                                     
                                            else:
                                                logger.info(f'[Data Credito] HC2 Quanto Error')
                                                quanto_accepted = False                                                     
                                        else:
                                            logger.info(f'[Data Credito] HC2 Quanto Error')
                                            quanto_accepted = False                                                
                                else:
                                    logger.info(f'[Data Credito] Acierta Score Error : {score}')
                                    # go to create all loan structures in db
                                    loop = self.get_running_loop()

                                    if loop and loop.is_running():
                                        loop.create_task(self.loan_handler.create_loan(status_id=9))  # Data Credit error
                                    else:
                                        asyncio.run(self.loan_handler.create_loan(status_id=9))  # Data Credit error
                        else:
                            logger.info(f'[Data Credito] HC2 Identification Error : {identification}')
                            # go to create all loan structures in db
                            loop = self.get_running_loop()

                            if loop and loop.is_running():
                                loop.create_task(self.loan_handler.create_loan(status_id=8))  # Data Credit error
                            else:
                                asyncio.run(self.loan_handler.create_loan(status_id=8))  # Data Credit error
                elif int(answer) in (1, 2, 4, 5, 9):
                    logger.info(f'[Data Credito] HC2 Answer Error Rejected: {answer}')
                    # go to create all loan structures in db
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=13))  # Data Credit error
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=13))  # Data Credit error

                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False})
                elif int(answer) == 23:
                    logger.info(f'[Data Credito] HC2 Answer Error Try Again: {answer}')
                    # go to create all loan structures in db
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=13))  # Data Credit error
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=13))  # Data Credit error

                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False})
                elif int(answer) in (3, 7, 8, 11, 12, 15, 16, 17, 18, 19, 20, 21, 24):
                    logger.info(f'[Data Credito] HC2 Answer Error Report to Data Credito: {answer}')
                    # go to create all loan structures in db
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=13))  # Data Credit error
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=13))  # Data Credit error

                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False})
                elif int(answer) in (6, 10):
                    logger.info(f'[Data Credito] HC2 Answer Error Report to Incorrect Info: {answer}')
                    # go to create all loan structures in db
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=13))  # Data Credit error
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=13))  # Data Credit error

                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False})
                else:
                    logger.info(f'[Data Credito] HC2 Answer Error : {answer}')
                    # go to create all loan structures in db
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=13))  # Data Credit error
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=13))  # Data Credit error

                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False})
            else:
                # go to create all loan structures in db
                loop = self.get_running_loop()

                if loop and loop.is_running():
                    loop.create_task(self.loan_handler.create_loan(status_id=12))  # Data Credit error
                else:
                    asyncio.run(self.loan_handler.create_loan(status_id=12))  # Data Credit error

            # On approved all rules
            logger.info(f'score_accepted: {score_accepted} | alerts_accepted: {alerts_accepted} | answer_accepted: {answer_accepted} | identification_accepted: {identification_accepted} | quanto_accepted: {quanto_accepted}')
            if score_accepted and alerts_accepted and answer_accepted and identification_accepted and quanto_accepted:
                logger.info(f'[Data Credito] HC2 ALL RULES APPROVED userId: {self.user_id}')
                # show Data Credito process on Success
                self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': True})

                # go to create all loan structures in db with data credito status success
                # update the quote history for this client
                loop = self.get_running_loop()

                if loop and loop.is_running():
                    loop.create_task(self.loan_handler.create_loan(status_id=11))  # Data Credit success
                    loop.create_task(update_client_db(self.conn, CustomerInUpdate(
                        identification=customer.identification,
                        quota=recommend_amount_round,
                        quota_valid_until=datetime.datetime.now()
                    )))  # Data Credit success
                else:
                    asyncio.run(self.loan_handler.create_loan(status_id=11))  # Data Credit success
                    asyncio.run(update_client_db(self.conn, CustomerInUpdate(
                        identification=customer.identification,
                        quota=recommend_amount_round,
                        quota_valid_until=datetime.datetime.now()
                    )))  # Data Credit success
            else:
                logger.info(f'[Data Credito] HC2 ALL RULES REJECTED userId: {self.user_id}')
                if quanto_have_new_amount_loan is False:
                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False})
            self.storage_financial_history(xml_dict)

        def handle_future_evidente_validar(future):
            try:
                result = saxutils.unescape(future.result()[0])
            except AttributeError:
                result = future.result()[0]

            logger.info(f'[Data Credito] Evidente Validar on Response {result}')
            # parse XML response
            root = ElementTree.XML(result)
            xml_dict = XmlDictConfig(root)

            if xml_dict.keys() & {'valFechaExp', 'excluirCliente', 'regValidacion', 'resultado'}:
                date_exp_valid: bool = bool(strtobool(xml_dict.get('valFechaExp')))
                ignore_client: bool = bool(strtobool(xml_dict.get('excluirCliente')))
                reg_validation: str = str(xml_dict.get('regValidacion'))
                response_result: str = str(xml_dict.get('resultado'))

                if DATACREDITO_DEBUG:  # simulate on success response
                    logger.info(f'[Data Credito] Evidente Validar SUCCESS')
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(
                            self.evidente_preguntas(
                                select=EvidentePreguntasInSelect(userId=self.user_id, regValidacion=reg_validation))
                        )
                    else:
                        asyncio.run(
                            self.evidente_preguntas(
                                select=EvidentePreguntasInSelect(userId=self.user_id, regValidacion=reg_validation))
                        )
                    return DATACREDITO_DEBUG

                if not date_exp_valid:
                    logger.info(f'[Data Credito] Evidente date_exp_valid : {date_exp_valid}')
                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False, 'isEvidente': True})

                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                elif ignore_client:
                    logger.info(f'[Data Credito] Evidente ignore_client : {ignore_client}')
                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False})

                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                elif not reg_validation:
                    logger.info(f'[Data Credito] Evidente reg_validacion : {reg_validation}')
                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False, 'isEvidente': True})

                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                elif response_result not in ['01', '05']:
                    logger.info(f'[Data Credito] Evidente reg_validacion : {reg_validation}')
                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False})

                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                else:
                    logger.info(f'[Data Credito] Evidente Validar SUCCESS')
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(
                            self.evidente_preguntas(
                                select=EvidentePreguntasInSelect(userId=self.user_id, regValidacion=reg_validation))
                        )
                    else:
                        asyncio.run(
                            self.evidente_preguntas(
                                select=EvidentePreguntasInSelect(userId=self.user_id, regValidacion=reg_validation))
                        )

        def handle_future_evidente_preguntas(future):
            future.result()

        def handle_future_evidente_verificar(future):
            try:
                response = saxutils.unescape(future.result()[0])
                response = xmltodict.parse(response)
            except (AttributeError, xmltodict.expat.ExpatError) as ex:
                logger.info(f'[Data Credito] Evidente Verificar on Response {future.result()[0]}')
                logger.error(f'[Data Credito] Evidente Verificar on Error {ex}')

                self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False, 'isEvidente': True})

                loop = self.get_running_loop()

                if loop and loop.is_running():
                    loop.create_task(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                else:
                    asyncio.run(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                return False

            logger.info(f'[Data Credito] Evidente Verificar on Response {response}')

            if '@aprobacion' in response['Evaluacion'] and response['Evaluacion']['@aprobacion'] == 'true':
                logger.info(f'[Data Credito] Evidente Verificar [@aprobacion] Success')
                logger.info(f'[Data Credito] Start HC2 process')
                
                text_for_credit: float = user[0].is_text_for_credit_valid if user[0].is_text_for_credit_valid else None
                text_for_credit_valid_until: datetime = user[0].text_for_credit_valid_until if user[0].text_for_credit_valid_until else None
                
                loop = self.get_running_loop()
                if loop and loop.is_running():
                    loop.create_task(update_client_db(self.conn, CustomerInUpdate(
                        identification=customer.identification,
                        is_evidente=datetime.datetime.now()
                    )))  # Evidente success            
                else:                              
                    asyncio.run(update_client_db(self.conn, CustomerInUpdate(
                        identification=customer.identification,
                        is_evidente=datetime.datetime.now()
                    )))  # Evidente success 

                if (text_for_credit and text_for_credit_valid_until) and (text_for_credit_valid_until > (datetime.datetime.now() - datetime.timedelta(days=90))):
                    # show Data Credito process on Success
                    self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': True})
                    # go to create all loan structures in db with data credito status success
                    # update the quote history for this client
                    loop = self.get_running_loop()

                    if loop and loop.is_running():
                        loop.create_task(self.loan_handler.create_loan(status_id=11))  # Data Credit success
                    else:
                        asyncio.run(self.loan_handler.create_loan(status_id=11))  # Data Credit success
                else:
                    loop = self.get_running_loop()
                    if loop and loop.is_running():
                        loop.create_task(
                            self.start(select_financial_history=DataCreditLoanHistoryInSelect(
                                nip=customer.identification
                                , firstName=customer.first_name
                                , identificationType=customer.identification_type_id if customer.identification_type_id == 1 else 4
                            ), is_financial_history=True)
                        )
                    else:
                        asyncio.run(
                            self.start(select_financial_history=DataCreditLoanHistoryInSelect(
                                nip=customer.identification
                                , firstName=customer.first_name
                                , identificationType=customer.identification_type_id if customer.identification_type_id == 1 else 4
                            ), is_financial_history=True)
                        )
            else:
                logger.info(f'[Data Credito] Evidente Verificar [@aprobacion] Fail')
                self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False, 'isEvidente': True})
                loop = self.get_running_loop()

                if loop and loop.is_running():
                    loop.create_task(self.loan_handler.create_loan(status_id=7))  # Data Credit error
                else:
                    asyncio.run(self.loan_handler.create_loan(status_id=7))  # Data Credit error

        if self.redis_conn is None:
            logger.info('Redis connection is not available. Creating a new instance')
            self.redis_conn = await connect_redis_database(new_instance=True)

        self.loan_handler = LoanHandler(self.conn, self.user_id, self.redis_conn)

        customer_model = CustomerInSelect(
            user_id=self.user_id
        )
        customer: CustomerInResponse = await get_customer_db(self.conn, customer_model)
        user = await select_users(self.conn, UserInSelect(user_id=self.user_id))

        if is_financial_history:
            loan_attempt_info = await self.loan_handler.get_current_customer_loan_configuration()
            
            # step amount
            loan_config_handler = LoanConfigurationHandler(self.conn, self.redis_conn)
            active_loans_configuration = await loan_config_handler.get_active_loan_configuration(loans_configuration_id=loan_attempt_info.loans_configuration_id)
            step_amount = active_loans_configuration.step_amount

            # start client to HC2 service
            customer = await get_customer_db(self.conn, CustomerInSelect(user_id=self.user_id))

            loan_config_handler = LoanConfigurationHandler(self.conn, self.redis_conn)

            loan_config = await loan_config_handler.get_active_loan_configuration(loans_configuration_id=loan_attempt_info.loans_configuration_id)
            
            calculate_loans = calculate_loan(loan_config, loan_attempt_info.calculator_config)
            self.redis_loan_amount = float(calculate_loans.loan_amount)

            self.wsdl_client = await self.make_client(self.wsdl_hc2)
            task = self.compile_async_tasks(self.get_financial_history(select=select_financial_history))
            future_obj = asyncio.gather(*task, return_exceptions=True)
            future_obj.add_done_callback(handle_future_financial_history)
        elif is_evidente_validar:
            # start client to HC2 service
            self.wsdl_client = await self.make_client(self.wsdl_evidente)

            task = self.compile_async_tasks(self.evidente_validar(select=select_evidente_validar))
            future_obj = asyncio.gather(*task, return_exceptions=True)
            future_obj.add_done_callback(handle_future_evidente_validar)
        elif is_evidente_preguntas:
            # start client to HC2 service
            self.wsdl_client = await self.make_client(self.wsdl_evidente)

            task = self.compile_async_tasks(self.evidente_preguntas(select=select_evidente_preguntas))
            future_obj = asyncio.gather(*task, return_exceptions=True)
            future_obj.add_done_callback(handle_future_evidente_preguntas)
        elif is_evidente_verificar:
            # start client to HC2 service
            self.wsdl_client = await self.make_client(self.wsdl_evidente)

            task = self.compile_async_tasks(self.evidente_verificar(select=select_evidente_verificar))
            future_obj = asyncio.gather(*task, return_exceptions=True)
            future_obj.add_done_callback(handle_future_evidente_verificar)

    def send_message_to_ws(self, view: str, data):
        logger.info(f'[DataCreditoClient].send_message_to_ws() data: {data}')
        socket_handler = SocketHandler(self.user_id)

        loop = self.get_running_loop()
        if loop and loop.is_running():
            loop.create_task(socket_handler.send_message_to_user({'view': view, 'data': data}))
        else:
            asyncio.run(socket_handler.send_message_to_user({'view': view, 'data': data}))

    # HC2 integration methods
    async def get_wsdl_definition(self):
        interface = {}
        self.wsdl_client = await self.make_client(self.wsdl_evidente)

        def parse_elements(all_elements):
            all_elements = {}
            for name, element in all_elements:
                all_elements[name] = {}
                all_elements[name]['optional'] = element.is_optional
                if hasattr(element.type, 'elements'):
                    all_elements[name]['type'] = parse_elements(
                        element.type.elements)
                else:
                    all_elements[name]['type'] = str(element.type)

            return all_elements

        # get each services
        for service in self.wsdl_client.wsdl.services.values():
            interface[service.name] = {}
            print("service:", service.name)
            for port in service.ports.values():
                interface[service.name][port.name] = {}
                operations = {}
                print("Port :", port.name)
                print()
                operations2 = sorted(
                    port.binding._operations.values(),
                    key=operator.attrgetter('name'))

                for operation in operations2:
                    print("method :", operation.name)
                    print("  input :", operation.input.signature())
                print()
                for operation in port.binding._operations.values():
                    operations[operation.name] = {}
                    operations[operation.name]['input'] = {}
                    elements = operation.input.body.type.elements
                    operations[operation.name]['input'] = parse_elements(elements)
                interface[service.name][port.name]['operations'] = operations

        # get a specific type signature by name
        complextype = self.wsdl_client.get_type('ns0:solicitudPlus')
        print(complextype.name)
        print(complextype.signature())
        print('*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-')
        print(interface)

    async def get_financial_history(self, select: DataCreditLoanHistoryInSelect):
        # build parameter object with id of enterprise give from ProtecData
        request_data = {
            'solicitud': {
                'clave': self.password,
                'identificacion': select.nip,
                'primerApellido': select.firstName,
                'producto': self.product_hc2,
                'tipoIdentificacion': select.identificationType,
                'usuario': self.okta_user.split('-')[1]
            }
        }

        logger.info(f'[Data Credito] consultarHC2 parameter: {request_data}')
        response = self.wsdl_client.service.consultarHC2(**request_data)
        # response = """<?xml version="1.0" encoding="ISO-8859-1"?><Informes><Informe fechaConsulta="2020-10-02T23:25:10" respuesta="13" codSeguridad="CCUD878" tipoIdDigitado="1" identificacionDigitada="888888881" apellidoDigitado="PRUEBAS"><NaturalNacional nombres="JUAN" primerApellido="PRUEBAS" segundoApellido="PEREZ" nombreCompleto="PRUEBAS PEREZ JUAN" validada="true" rut="false" genero="4"><Identificacion estado="00" fechaExpedicion="1988-10-05" ciudad="BOGOTA DC" departamento="CUNDINAMAR" genero="4" numero="00888888881" /><Edad min="46" max="55" /><InfoDemografica /></NaturalNacional><Score tipo="67" puntaje="678.0" fecha="2020-10-02" poblacion="45"><Razon codigo="00099" /><Razon codigo="00000" /></Score><CuentaCartera bloqueada="false" entidad="EXPERIAN       COLOMBIA SA" numero="001234567" fechaApertura="2006-08-01" fechaVencimiento="2012-08-01" comportamiento="NNNNNNNNNNNNNNNNNNNNNNN------N----------------- " formaPago="1" probabilidadIncumplimiento="0.0" situacionTitular="0" oficina="NO INFORMO" ciudad="" codigoDaneCiudad="00000000" codSuscriptor="050098" tipoIdentificacion="2" identificacion="00900422614" sector="3" calificacionHD="true"><Caracteristicas tipoCuenta="CAB" tipoObligacion="1" tipoContrato="0" ejecucionContrato="1" mesesPermanencia="0" calidadDeudor="05" garantia="2" /><Valores /><Estados><EstadoCuenta codigo="03" fecha="2012-09-11" /><EstadoOrigen codigo="0" fecha="2006-08-01" /><EstadoPago codigo="08" meses="48" fecha="2012-09-11" /></Estados><Llave>10088888888105009805001234567000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="5954-3806" fechaApertura="2016-04-13" fechaVencimiento="2016-07-13" comportamiento="DDDDDDDDDDDDDDDDDDDDDDDDDDD432----------------- " formaPago="0" probabilidadIncumplimiento="0.0" calificacion="4" situacionTitular="0" oficina="0" ciudad="000000" codigoDaneCiudad="00000000" codSuscriptor="340114" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="false"><Caracteristicas tipoCuenta="COM" tipoObligacion="1" tipoContrato="1" ejecucionContrato="1" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2019-03-31" calificacion="4" saldoActual="278000.0" saldoMora="278000.0" disponible="-1" cuota="139000.0" cuotasMora="2" diasMora="360" periodicidad="1" totalCuotas="3" valorInicial="-1" cuotasCanceladas="0" /></Valores><Estados><EstadoCuenta codigo="05" fecha="2019-03-31" /><EstadoOrigen codigo="0" fecha="2016-04-13" /><EstadoPago codigo="47" meses="48" fecha="2019-03-31" /></Estados><Llave>100888888881340114345954-3806000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="031297116" fechaApertura="2015-07-05" fechaVencimiento="2024-12-31" comportamiento="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN " formaPago="0" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2020-07-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="51000.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-02-16" fechaLimitePago="2015-11-27" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="01" fecha="2020-07-31" /><EstadoOrigen codigo="0" fecha="2015-07-05" /><EstadoPago codigo="01" meses="48" fecha="2020-07-31" /></Estados><Llave>10088888888123000123031297116000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="031931295" fechaApertura="2015-09-02" fechaVencimiento="2024-12-31" comportamiento="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN " formaPago="0" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2020-07-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="30000.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-02-26" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="01" fecha="2020-07-31" /><EstadoOrigen codigo="0" fecha="2015-09-02" /><EstadoPago codigo="01" meses="48" fecha="2020-07-31" /></Estados><Llave>10088888888123000123031931295000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="030158493" fechaApertura="2015-03-09" fechaVencimiento="2030-12-31" comportamiento="NNNNNNN---------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-03-18" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-03-09" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123030158493000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="030692467" fechaApertura="2015-05-06" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-09-02" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-05-06" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123030692467000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="030697456" fechaApertura="2015-05-06" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-05-14" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-05-06" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123030697456000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="031394664" fechaApertura="2015-07-14" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-07-27" fechaLimitePago="2015-08-11" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-07-14" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123031394664000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="031551192" fechaApertura="2015-07-29" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-08-04" fechaLimitePago="2015-08-19" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-07-29" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123031551192000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="031579364" fechaApertura="2015-07-31" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-08-04" fechaLimitePago="2015-08-19" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-07-31" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123031579364000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="031685596" fechaApertura="2015-08-11" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-08-31" fechaLimitePago="2015-09-09" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-08-11" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123031685596000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="031827442" fechaApertura="2015-08-25" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-08-31" fechaLimitePago="2015-09-01" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-08-25" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123031827442000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="032067200" fechaApertura="2015-09-15" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-09-24" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-09-15" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123032067200000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="032071475" fechaApertura="2015-09-15" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-10-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-09-24" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-10-31" /><EstadoOrigen codigo="0" fecha="2015-09-15" /><EstadoPago codigo="08" meses="48" fecha="2015-10-31" /></Estados><Llave>10088888888123000123032071475000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="032788562" fechaApertura="2015-11-20" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-12-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-12-23" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-12-31" /><EstadoOrigen codigo="0" fecha="2015-11-20" /><EstadoPago codigo="08" meses="48" fecha="2015-12-31" /></Estados><Llave>10088888888123000123032788562000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="032788610" fechaApertura="2015-11-20" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-12-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-12-23" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-12-31" /><EstadoOrigen codigo="0" fecha="2015-11-20" /><EstadoPago codigo="08" meses="48" fecha="2015-12-31" /></Estados><Llave>10088888888123000123032788610000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="032788629" fechaApertura="2015-11-20" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2015-12-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2015-12-23" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2015-12-31" /><EstadoOrigen codigo="0" fecha="2015-11-20" /><EstadoPago codigo="08" meses="48" fecha="2015-12-31" /></Estados><Llave>10088888888123000123032788629000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="032792985" fechaApertura="2015-11-20" fechaVencimiento="2030-12-31" comportamiento="NN--------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-01-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-01-27" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-01-31" /><EstadoOrigen codigo="0" fecha="2015-11-20" /><EstadoPago codigo="08" meses="48" fecha="2016-01-31" /></Estados><Llave>10088888888123000123032792985000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="033472679" fechaApertura="2016-01-22" fechaVencimiento="2030-12-31" comportamiento="----------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-01-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-01-27" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-01-31" /><EstadoOrigen codigo="0" fecha="2016-01-22" /><EstadoPago codigo="08" meses="48" fecha="2016-01-31" /></Estados><Llave>10088888888123000123033472679000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="033472876" fechaApertura="2016-01-22" fechaVencimiento="2030-12-31" comportamiento="----------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-01-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-01-27" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-01-31" /><EstadoOrigen codigo="0" fecha="2016-01-22" /><EstadoPago codigo="08" meses="48" fecha="2016-01-31" /></Estados><Llave>10088888888123000123033472876000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="034777732" fechaApertura="2016-05-22" fechaVencimiento="2030-12-31" comportamiento="NNN-------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-08-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-08-04" fechaLimitePago="2016-06-07" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-08-31" /><EstadoOrigen codigo="0" fecha="2016-05-22" /><EstadoPago codigo="08" meses="48" fecha="2016-08-31" /></Estados><Llave>10088888888123000123034777732000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="035350722" fechaApertura="2016-07-17" fechaVencimiento="2030-12-31" comportamiento="----------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-07-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-07-28" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-07-31" /><EstadoOrigen codigo="0" fecha="2016-07-17" /><EstadoPago codigo="08" meses="48" fecha="2016-07-31" /></Estados><Llave>10088888888123000123035350722000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="036504378" fechaApertura="2016-10-26" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-11-30" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-11-02" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-11-30" /><EstadoOrigen codigo="0" fecha="2016-10-26" /><EstadoPago codigo="08" meses="48" fecha="2016-11-30" /></Estados><Llave>10088888888123000123036504378000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="036675948" fechaApertura="2016-11-11" fechaVencimiento="2030-12-31" comportamiento="----------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-11-30" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-11-21" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-11-30" /><EstadoOrigen codigo="0" fecha="2016-11-11" /><EstadoPago codigo="08" meses="48" fecha="2016-11-30" /></Estados><Llave>10088888888123000123036675948000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="036675949" fechaApertura="2016-11-11" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-12-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-11-21" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-12-31" /><EstadoOrigen codigo="0" fecha="2016-11-11" /><EstadoPago codigo="08" meses="48" fecha="2016-12-31" /></Estados><Llave>10088888888123000123036675949000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="036675954" fechaApertura="2016-11-11" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-12-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-11-21" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-12-31" /><EstadoOrigen codigo="0" fecha="2016-11-11" /><EstadoPago codigo="08" meses="48" fecha="2016-12-31" /></Estados><Llave>10088888888123000123036675954000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="036675959" fechaApertura="2016-11-11" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-12-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-11-21" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-12-31" /><EstadoOrigen codigo="0" fecha="2016-11-11" /><EstadoPago codigo="08" meses="48" fecha="2016-12-31" /></Estados><Llave>10088888888123000123036675959000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="036675988" fechaApertura="2016-11-11" fechaVencimiento="2030-12-31" comportamiento="----------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-11-30" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-11-21" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-11-30" /><EstadoOrigen codigo="0" fecha="2016-11-11" /><EstadoPago codigo="08" meses="48" fecha="2016-11-30" /></Estados><Llave>10088888888123000123036675988000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="036676194" fechaApertura="2016-11-11" fechaVencimiento="2030-12-31" comportamiento="N---------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-12-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-11-21" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-12-31" /><EstadoOrigen codigo="0" fecha="2016-11-11" /><EstadoPago codigo="08" meses="48" fecha="2016-12-31" /></Estados><Llave>10088888888123000123036676194000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="036975591" fechaApertura="2016-12-04" fechaVencimiento="2030-12-31" comportamiento="----------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2016-12-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2016-12-16" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2016-12-31" /><EstadoOrigen codigo="0" fecha="2016-12-04" /><EstadoPago codigo="08" meses="48" fecha="2016-12-31" /></Estados><Llave>10088888888123000123036975591000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="037558978" fechaApertura="2017-01-19" fechaVencimiento="2030-12-31" comportamiento="NNNNNN----------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  AGUA DE DIOS        00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2017-07-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2017-05-25" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2017-07-31" /><EstadoOrigen codigo="0" fecha="2017-01-19" /><EstadoPago codigo="08" meses="48" fecha="2017-07-31" /></Estados><Llave>10088888888123000123037558978000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="037740598" fechaApertura="2017-02-01" fechaVencimiento="2030-12-31" comportamiento="NNNNN------------------------------------------ " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2017-07-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2017-05-25" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2017-07-31" /><EstadoOrigen codigo="0" fecha="2017-02-01" /><EstadoPago codigo="08" meses="48" fecha="2017-07-31" /></Estados><Llave>10088888888123000123037740598000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="037740614" fechaApertura="2017-02-01" fechaVencimiento="2030-12-31" comportamiento="NNNNN------------------------------------------ " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2017-07-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2017-04-22" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2017-07-31" /><EstadoOrigen codigo="0" fecha="2017-02-01" /><EstadoPago codigo="08" meses="48" fecha="2017-07-31" /></Estados><Llave>10088888888123000123037740614000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="040456047" fechaApertura="2017-08-07" fechaVencimiento="2030-12-31" comportamiento="NNNN------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2018-01-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2017-11-24" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2018-01-31" /><EstadoOrigen codigo="0" fecha="2017-08-07" /><EstadoPago codigo="08" meses="48" fecha="2018-01-31" /></Estados><Llave>10088888888123000123040456047000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="040535347" fechaApertura="2017-08-12" fechaVencimiento="2030-12-31" comportamiento="NNNN------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2018-01-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2017-11-24" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2018-01-31" /><EstadoOrigen codigo="0" fecha="2017-08-12" /><EstadoPago codigo="08" meses="48" fecha="2018-01-31" /></Estados><Llave>10088888888123000123040535347000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="MOVISTAR" numero="042004372" fechaApertura="2018-02-15" fechaVencimiento="2030-12-31" comportamiento="NN--------------------------------------------- " formaPago="1" probabilidadIncumplimiento="0.0" calificacion="1" situacionTitular="0" oficina="BOGOTA  BOGOTA              00" ciudad="000000BOGOTA" codigoDaneCiudad="00000000" codSuscriptor="230001" tipoIdentificacion="2" identificacion="00830037330" sector="4" calificacionHD="true"><Caracteristicas tipoCuenta="CTC" tipoObligacion="4" tipoContrato="2" ejecucionContrato="4" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores><Valor moneda="1" fecha="2018-05-31" calificacion="1" saldoActual="0.0" saldoMora="0.0" disponible="0.0" cuota="0.0" cuotasMora="0" diasMora="0" fechaPagoCuota="2018-02-28" periodicidad="1" totalCuotas="-1" valorInicial="-1" cuotasCanceladas="-1" /></Valores><Estados><EstadoCuenta codigo="03" fecha="2018-05-31" /><EstadoOrigen codigo="0" fecha="2018-02-15" /><EstadoPago codigo="08" meses="48" fecha="2018-05-31" /></Estados><Llave>10088888888123000123042004372000000000</Llave></CuentaCartera><CuentaCartera bloqueada="false" entidad="EXPERIAN       COLOMBIA SA" numero="000CLJ524" fechaApertura="2010-06-12" fechaVencimiento="2014-12-31" comportamiento="NNNNN----NNNNNNNNNNNNNN------------------------ " formaPago="1" probabilidadIncumplimiento="0.0" situacionTitular="0" oficina="NO INFORMO" ciudad="" codigoDaneCiudad="00000000" codSuscriptor="180017" tipoIdentificacion="2" identificacion="00900422614" sector="3" calificacionHD="true"><Caracteristicas tipoCuenta="LBZ" tipoObligacion="2" tipoContrato="2" ejecucionContrato="2" mesesPermanencia="0" calidadDeudor="00" garantia="2" /><Valores /><Estados><EstadoCuenta codigo="03" fecha="2012-09-12" /><EstadoOrigen codigo="0" fecha="2012-01-31" /><EstadoPago codigo="08" meses="48" fecha="2012-09-12" /></Estados><Llave>10088888888118001740000CLJ524000000000</Llave></CuentaCartera><Consulta fecha="2020-10-02" tipoCuenta="COC" entidad="CORPORACION    INTERACTUAR" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00890984843" sector="3"><Llave>10088888888122</Llave></Consulta><Consulta fecha="2020-10-02" tipoCuenta="CCF" entidad="FINAMERICA     C.F.C." oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00860025971" sector="1"><Llave>10088888888123</Llave></Consulta><Consulta fecha="2020-10-02" tipoCuenta="SFI" entidad="MAMUT SOLUCION ES" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00901380656" sector="3"><Llave>10088888888121</Llave></Consulta><Consulta fecha="2020-09-30" tipoCuenta="CCF" entidad="FINAMERICA     C.F.C." oficina="------------------" ciudad="------------" razon="00" cantidad="02" nitSuscriptor="00860025971" sector="1"><Llave>10088888888120</Llave></Consulta><Consulta fecha="2020-09-29" tipoCuenta="COC" entidad="CORPORACION    INTERACTUAR" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00890984843" sector="3"><Llave>10088888888118</Llave></Consulta><Consulta fecha="2020-09-29" tipoCuenta="SFI" entidad="MAMUT SOLUCION ES" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00901380656" sector="3"><Llave>10088888888117</Llave></Consulta><Consulta fecha="2020-09-29" tipoCuenta="CFR" entidad="SH MA CAPITAL  SAS" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00901348699" sector="3"><Llave>10088888888119</Llave></Consulta><Consulta fecha="2020-09-29" tipoCuenta="COM" entidad="SISDECO SAS" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00901376975" sector="3"><Llave>10088888888109</Llave></Consulta><Consulta fecha="2020-09-25" tipoCuenta="TDC" entidad="DATACREDITO" oficina="------------------" ciudad="------------" razon="00" cantidad="02" nitSuscriptor="00900422614" sector="3"><Llave>10088888888114</Llave></Consulta><Consulta fecha="2020-09-24" tipoCuenta="COF" entidad="POLEN" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00901294376" sector="3"><Llave>10088888888111</Llave></Consulta><Consulta fecha="2020-09-24" tipoCuenta="COM" entidad="TECHFINANCIAL  SAS" oficina="------------------" ciudad="------------" razon="00" cantidad="02" nitSuscriptor="00901295242" sector="3"><Llave>10088888888112</Llave></Consulta><Consulta fecha="2020-09-23" tipoCuenta="SFI" entidad="DLV STARTUPS   GROUP SAS" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00901174953" sector="3"><Llave>10088888888108</Llave></Consulta><Consulta fecha="2020-09-17" tipoCuenta="CCS" entidad="C3 BROKER" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00900871733" sector="3"><Llave>10088888888110</Llave></Consulta><Consulta fecha="2020-09-15" tipoCuenta="GRM" entidad="I CREDITT" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00901162959" sector="3"><Llave>10088888888107</Llave></Consulta><Consulta fecha="2020-08-12" tipoCuenta="CTC" entidad="COMCEL S.A." oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00800153993" sector="4"><Llave>10088888888106</Llave></Consulta><Consulta fecha="2020-08-12" tipoCuenta="CDC" entidad="TELMEX COLOMBI" oficina="------------------" ciudad="------------" razon="01" cantidad="01" nitSuscriptor="00830053800" sector="4"><Llave>10088888888105</Llave></Consulta><Consulta fecha="2020-08-06" tipoCuenta="TRT" entidad="SUPERAVAL SAS" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00901369750" sector="3"><Llave>10088888888104</Llave></Consulta><Consulta fecha="2020-07-30" tipoCuenta="CCF" entidad="B. FALLABELLA" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00900047981" sector="1"><Llave>10088888888103</Llave></Consulta><Consulta fecha="2020-07-24" tipoCuenta="CCB" entidad="BANCOLOMBIA" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00890903938" sector="1"><Llave>10088888888102</Llave></Consulta><Consulta fecha="2020-07-13" tipoCuenta="TRT" entidad="SUPERAVAL SAS" oficina="------------------" ciudad="------------" razon="00" cantidad="01" nitSuscriptor="00901369750" sector="3"><Llave>10088888888101</Llave></Consulta><Alerta colocacion="2020-10-02" vencimiento="2020-10-02" modificacion="2020-10-02" codigo="012" texto="Mas de 3 consultas de diferentes entidades en los ultimos 60 dias"><Fuente codigo="990001" nombre="B.d datacredit" /><Llave>100888888885990003001</Llave></Alerta><Alerta colocacion="2020-10-02" vencimiento="2020-10-02" modificacion="2020-10-02" codigo="304" texto="No se encuentra coincidencia con listas restrictivas de id consultado al 20201002" codLista="00"><Fuente codigo="000003" nombre="Listas restrictivas" /><Llave>100888888885990003001</Llave></Alerta><Reclamo numero="2692382" numeroCuenta="000000000032788629" entidad="MOVISTAR" tipoLeyenda="1" tipo="02" texto="TERRE" fecha="2020-03-13" estado="1" ratificado="true"><Subtipo codigo="01" nombre="No actualización de la información" /></Reclamo><Reclamo numero="2692376" numeroCuenta="000000000032788562" entidad="MOVISTAR" tipoLeyenda="1" tipo="02" texto="TEST" fecha="2020-03-13" estado="1" ratificado="true"><Subtipo codigo="01" nombre="No actualización de la información" /></Reclamo><Reclamo numero="2692374" numeroCuenta="000000000031827442" entidad="MOVISTAR" tipoLeyenda="1" tipo="02" texto="567567" fecha="2020-03-13" estado="1" ratificado="true"><Subtipo codigo="02" nombre="No reporte información oportuno" /></Reclamo><Reclamo numero="2692373" numeroCuenta="000000000031579364" entidad="MOVISTAR" tipoLeyenda="1" tipo="02" texto="TEST" fecha="2020-03-13" estado="1" ratificado="true"><Subtipo codigo="02" nombre="No reporte información oportuno" /></Reclamo><Reclamo numero="2692372" numeroCuenta="000000000031394664" entidad="MOVISTAR" tipoLeyenda="5" tipo="02" texto="P. FRANDO" fecha="2020-03-13" estado="1" ratificado="false"><Subtipo codigo="01" nombre="No actualización de la información" /></Reclamo><Reclamo numero="2692370" numeroCuenta="000000000032067200" entidad="MOVISTAR" tipoLeyenda="1" tipo="02" texto="PRUEBA" fecha="2020-03-12" estado="1" ratificado="true"><Subtipo codigo="02" nombre="No reporte información oportuno" /></Reclamo><Reclamo numero="2692369" numeroCuenta="00034455954-603806" entidad="MOVISTAR" tipoLeyenda="1" tipo="02" texto="PRUEBAS MARZO 12 DE 2020" fecha="2020-03-12" estado="1" ratificado="true"><Subtipo codigo="01" nombre="No actualización de la información" /></Reclamo><Reclamo numero="2692368" numeroCuenta="000000000030158493" entidad="MOVISTAR" tipoLeyenda="1" tipo="02" texto="PRUEBA MARZO 12 DE 2020" fecha="2020-03-12" estado="1" ratificado="true"><Subtipo codigo="01" nombre="No actualización de la información" /></Reclamo><productosValores producto="62" valor1="1694" valor2="1185" valor3="2202" valor4="0" valor5="0" valor6="0" valor7="0" valor8="0" valor9="0" valor10="0" valor1smlv="1.93" valor2smlv="1.351" valor3smlv="2.509" valor4smlv="0.0" valor5smlv="0.0" valor6smlv="0.0" valor7smlv="0.0" valor8smlv="0.0" valor9smlv="0.0" valor10smlv="0.0" razon1="0099" razon2="00000" razon3="00000" razon4="00000" razon5="00000" razon6="00000" razon7="00000" razon8="00000" razon9="00000" razon10="00000" /><InfoAgregada><Resumen><Principales creditoVigentes="3" creditosCerrados="34" creditosActualesNegativos="1" histNegUlt12Meses="1" cuentasAbiertasAHOCCB="0" cuentasCerradasAHOCCB="0" consultadasUlt6meses="23" desacuerdosALaFecha="8" antiguedadDesde="2006-08-01" reclamosVigentes="8" /><Saldos saldoTotalEnMora="278.0" saldoM30="0.0" saldoM60="0.0" saldoM90="278.0" cuotaMensual="220.0" saldoCreditoMasAlto="278.0" saldoTotal="278.0"><Sector sector="1" saldo="0.0" participacion="0.0" /><Sector sector="2" saldo="0.0" participacion="0.0" /><Sector sector="3" saldo="0.0" participacion="0.0" /><Sector sector="4" saldo="278.0" participacion="100.0" /><Mes fecha="2020-07-31" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2020-06-30" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2020-05-30" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2020-04-30" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2020-03-30" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2020-02-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2020-01-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-12-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-11-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-10-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-09-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-08-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-07-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-06-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-05-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-04-29" saldoTotalMora="0" saldoTotal="0" /><Mes fecha="2019-03-29" saldoTotalMora="278000" saldoTotal="278000" /><Mes fecha="2019-02-28" saldoTotalMora="278000" saldoTotal="278000" /><Mes fecha="2019-01-28" saldoTotalMora="278000" saldoTotal="278000" /><Mes fecha="2018-12-28" saldoTotalMora="278000" saldoTotal="278000" /><Mes fecha="2018-11-28" saldoTotalMora="278000" saldoTotal="278000" /><Mes fecha="2018-10-28" saldoTotalMora="278000" saldoTotal="278000" /><Mes fecha="2018-09-28" saldoTotalMora="278000" saldoTotal="278000" /><Mes fecha="2018-08-28" saldoTotalMora="278000" saldoTotal="278000" /></Saldos><Comportamiento><Mes fecha="2020-07-31" comportamiento="N" cantidad="0" /><Mes fecha="2020-06-30" comportamiento="N" cantidad="0" /><Mes fecha="2020-05-30" comportamiento="N" cantidad="0" /><Mes fecha="2020-04-30" comportamiento="N" cantidad="0" /><Mes fecha="2020-03-30" comportamiento="N" cantidad="0" /><Mes fecha="2020-02-29" comportamiento="N" cantidad="0" /><Mes fecha="2020-01-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-12-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-11-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-10-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-09-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-08-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-07-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-06-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-05-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-04-29" comportamiento="N" cantidad="0" /><Mes fecha="2019-03-29" comportamiento="D" cantidad="1" /><Mes fecha="2019-02-28" comportamiento="D" cantidad="1" /><Mes fecha="2019-01-28" comportamiento="D" cantidad="1" /><Mes fecha="2018-12-28" comportamiento="D" cantidad="1" /><Mes fecha="2018-11-28" comportamiento="D" cantidad="1" /><Mes fecha="2018-10-28" comportamiento="D" cantidad="1" /><Mes fecha="2018-09-28" comportamiento="D" cantidad="1" /><Mes fecha="2018-08-28" comportamiento="D" cantidad="1" /></Comportamiento></Resumen><Totales><TipoCuenta codigoTipo="CTC" tipo="Cartera telefonía celular" calidadDeudor="Principal" cupo="0.0" saldo="0.0" saldoMora="0.0" cuota="81.0" /><TipoCuenta codigoTipo="COM" tipo="Cartera computadores" calidadDeudor="Principal" cupo="0.0" saldo="278.0" saldoMora="278.0" cuota="139.0" /><Total calidadDeudor="Principal" participacion="0.0" cupo="0.0" saldo="278.0" saldoMora="278.0" cuota="220.0" /><Total calidadDeudor="Codeudor" participacion="0.0" cupo="0.0" saldo="0.0" saldoMora="0.0" cuota="0.0" /></Totales><ComposicionPortafolio><TipoCuenta tipo="COM" calidadDeudor="Principal" porcentaje="1.0" cantidad="1"><Estado codigo="Dudoso recaudo" cantidad="1" /></TipoCuenta><TipoCuenta tipo="CTC" calidadDeudor="Principal" porcentaje="1.0" cantidad="2"><Estado codigo="Al dia" cantidad="2" /></TipoCuenta></ComposicionPortafolio><Cheques /><EvolucionDeuda><Trimestre fecha="2020-06-01" cuota="81000" cupoTotal="0" saldo="0" porcentajeUso="0.0" score="0.0" calificacion="D" aperturaCuentas="0" cierreCuentas="0" totalAbiertas="3" totalCerradas="34" moraMaxima="M 0" mesesMoraMaxima="0" /><Trimestre fecha="2020-03-01" cuota="81000" cupoTotal="0" saldo="0" porcentajeUso="0.0" score="0.0" calificacion="D" aperturaCuentas="0" cierreCuentas="0" totalAbiertas="3" totalCerradas="34" moraMaxima="M 0" mesesMoraMaxima="0" /><Trimestre fecha="2019-12-01" cuota="81000" cupoTotal="0" saldo="0" porcentajeUso="0.0" score="0.0" calificacion="D" aperturaCuentas="0" cierreCuentas="0" totalAbiertas="3" totalCerradas="34" moraMaxima="M 0" mesesMoraMaxima="0" /><AnalisisPromedio cuota="0.0" cupoTotal="0.0" saldo="0.0" porcentajeUso="0.0" score="0.0" calificacion="0" aperturaCuentas="0.0" cierreCuentas="0.0" totalAbiertas="0.0" totalCerradas="0.0" moraMaxima="0" /></EvolucionDeuda><HistoricoSaldos><TipoCuenta tipo="CAB"><Trimestre fecha="2020-06-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2020-03-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-12-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-09-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-06-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-03-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2018-12-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2018-09-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /></TipoCuenta><TipoCuenta tipo="COM"><Trimestre fecha="2020-06-01" totalCuentas="1" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2020-03-01" totalCuentas="1" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-12-01" totalCuentas="1" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-09-01" totalCuentas="1" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-06-01" totalCuentas="1" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-03-01" totalCuentas="1" cuentasConsideradas="1" saldo="278000" /><Trimestre fecha="2018-12-01" totalCuentas="1" cuentasConsideradas="1" saldo="278000" /><Trimestre fecha="2018-09-01" totalCuentas="1" cuentasConsideradas="1" saldo="278000" /></TipoCuenta><TipoCuenta tipo="CTC"><Trimestre fecha="2020-06-01" totalCuentas="2" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2020-03-01" totalCuentas="2" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-12-01" totalCuentas="2" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-09-01" totalCuentas="2" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-06-01" totalCuentas="2" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-03-01" totalCuentas="2" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2018-12-01" totalCuentas="2" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2018-09-01" totalCuentas="2" cuentasConsideradas="0" saldo="0" /></TipoCuenta><TipoCuenta tipo="LBZ"><Trimestre fecha="2020-06-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2020-03-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-12-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-09-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-06-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2019-03-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2018-12-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /><Trimestre fecha="2018-09-01" totalCuentas="0" cuentasConsideradas="0" saldo="0" /></TipoCuenta><Totales fecha="2020-06-01" totalCuentas="3" cuentasConsideradas="0" saldo="0.0" /><Totales fecha="2020-03-01" totalCuentas="3" cuentasConsideradas="0" saldo="0.0" /><Totales fecha="2019-12-01" totalCuentas="3" cuentasConsideradas="0" saldo="0.0" /><Totales fecha="2019-09-01" totalCuentas="3" cuentasConsideradas="0" saldo="0.0" /><Totales fecha="2019-06-01" totalCuentas="3" cuentasConsideradas="0" saldo="0.0" /><Totales fecha="2019-03-01" totalCuentas="3" cuentasConsideradas="1" saldo="278000.0" /><Totales fecha="2018-12-01" totalCuentas="3" cuentasConsideradas="1" saldo="278000.0" /><Totales fecha="2018-09-01" totalCuentas="3" cuentasConsideradas="1" saldo="278000.0" /></HistoricoSaldos><ResumenEndeudamiento /></InfoAgregada><InfoAgregadaMicrocredito><Resumen><PerfilGeneral><CreditosVigentes sectorFinanciero="0" sectorCooperativo="0" sectorReal="0" sectorTelcos="3" totalComoPrincipal="3" totalComoCodeudorYOtros="0" /><CreditosCerrados sectorFinanciero="0" sectorCooperativo="0" sectorReal="2" sectorTelcos="32" totalComoPrincipal="33" totalComoCodeudorYOtros="1" /><CreditosReestructurados sectorFinanciero="0" sectorCooperativo="0" sectorReal="0" sectorTelcos="0" totalComoPrincipal="0" totalComoCodeudorYOtros="0" /><CreditosRefinanciados sectorFinanciero="0" sectorCooperativo="0" sectorReal="0" sectorTelcos="0" totalComoPrincipal="0" totalComoCodeudorYOtros="0" /><ConsultaUlt6Meses sectorFinanciero="5" sectorCooperativo="0" sectorReal="16" sectorTelcos="2" totalComoPrincipal="0" totalComoCodeudorYOtros="0" /><Desacuerdos sectorFinanciero="0" sectorCooperativo="0" sectorReal="0" sectorTelcos="0" totalComoPrincipal="0" totalComoCodeudorYOtros="0" /><AntiguedadDesde sectorReal="2006-08-01" sectorTelcos="2015-03-09" /></PerfilGeneral><VectorSaldosYMoras poseeSectorCooperativo="false" poseeSectorFinanciero="false" poseeSectorReal="false" poseeSectorTelcos="true"><SaldosYMoras fecha="2020-07-31" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2020-06-30" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2020-05-30" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2020-04-30" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2020-03-30" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2020-02-29" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2020-01-29" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2019-12-29" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2019-11-29" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2019-10-29" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2019-09-29" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /><SaldosYMoras fecha="2019-08-29" totalCuentasMora="0" saldoDeudaTotalMora="0.0" saldoDeudaTotal="0.0" morasMaxSectorTelcos="N" morasMaximas="N" numCreditos30="0" numCreditosMayorIgual60="0" /></VectorSaldosYMoras><EndeudamientoActual><Sector codSector="4"><TipoCuenta tipoCuenta="COM"><Usuario tipoUsuario="Principal"><Cuenta estadoActual="Dudoso recaudo" calificacion="D" valorInicial="0.0" saldoActual="278.0" saldoMora="278.0" cuotaMes="139.0" comportamientoNegativo="true" totalDeudaCarteras="278.0" /></Usuario></TipoCuenta><TipoCuenta tipoCuenta="CTC"><Usuario tipoUsuario="Principal"><Cuenta estadoActual="Al día" calificacion="A" valorInicial="0.0" saldoActual="0.0" saldoMora="0.0" cuotaMes="51.0" comportamientoNegativo="false" totalDeudaCarteras="278.0" /><Cuenta estadoActual="Al día" calificacion="A" valorInicial="0.0" saldoActual="0.0" saldoMora="0.0" cuotaMes="30.0" comportamientoNegativo="false" totalDeudaCarteras="278.0" /></Usuario></TipoCuenta></Sector></EndeudamientoActual><ImagenTendenciaEndeudamiento /></Resumen><ComportamientoEntidades /><AnalisisVectores><Sector nombreSector="Sector Telcos"><Cuenta entidad="MOVISTAR" numeroCuenta="5954-3806" tipoCuenta="COM" estado="Dudoso recaudo" contieneDatos="true"><CaracterFecha fecha="2020-07-31" /><CaracterFecha fecha="2020-06-30" /><CaracterFecha fecha="2020-05-30" /><CaracterFecha fecha="2020-04-30" /><CaracterFecha fecha="2020-03-30" /><CaracterFecha fecha="2020-02-29" /><CaracterFecha fecha="2020-01-29" /><CaracterFecha fecha="2019-12-29" /><CaracterFecha fecha="2019-11-29" /><CaracterFecha fecha="2019-10-29" /><CaracterFecha fecha="2019-09-29" /><CaracterFecha fecha="2019-08-29" /><CaracterFecha fecha="2019-07-29" /><CaracterFecha fecha="2019-06-29" /><CaracterFecha fecha="2019-05-29" /><CaracterFecha fecha="2019-04-29" /><CaracterFecha fecha="2019-03-29" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2019-02-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2019-01-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2018-12-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2018-11-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2018-10-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2018-09-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2018-08-28" saldoDeudaTotalMora="D" /></Cuenta><Cuenta entidad="MOVISTAR" numeroCuenta="031297116" tipoCuenta="CTC" estado="Al día" contieneDatos="true"><CaracterFecha fecha="2020-07-31" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-06-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-05-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-04-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-03-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-02-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-01-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-12-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-11-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-10-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-09-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-08-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-07-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-06-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-05-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-04-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-03-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-02-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-01-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-12-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-11-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-10-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-09-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-08-28" saldoDeudaTotalMora="N" /></Cuenta><Cuenta entidad="MOVISTAR" numeroCuenta="031931295" tipoCuenta="CTC" estado="Al día" contieneDatos="true"><CaracterFecha fecha="2020-07-31" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-06-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-05-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-04-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-03-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-02-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-01-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-12-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-11-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-10-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-09-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-08-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-07-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-06-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-05-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-04-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-03-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-02-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-01-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-12-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-11-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-10-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-09-28" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2018-08-28" saldoDeudaTotalMora="N" /></Cuenta><MorasMaximas><CaracterFecha fecha="2018-08-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2018-09-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2018-10-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2018-11-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2018-12-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2019-01-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2019-02-28" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2019-03-29" saldoDeudaTotalMora="D" /><CaracterFecha fecha="2019-04-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-05-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-06-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-07-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-08-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-09-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-10-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-11-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2019-12-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-01-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-02-29" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-03-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-04-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-05-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-06-30" saldoDeudaTotalMora="N" /><CaracterFecha fecha="2020-07-31" saldoDeudaTotalMora="N" /></MorasMaximas></Sector></AnalisisVectores><EvolucionDeuda><Trimestres><Trimestre>2020/09</Trimestre><Trimestre>2020/06</Trimestre><Trimestre>2020/03</Trimestre><Trimestre>2019/12</Trimestre><Trimestre>2019/09</Trimestre><Trimestre>2019/06</Trimestre></Trimestres><EvolucionDeudaSector codSector="4" nombreSector="Telcos"><EvolucionDeudaTipoCuenta tipoCuenta="COM"><EvolucionDeudaValorTrimestre trimestre="2020/09" tipoCuenta="COM" num="1" cupoInicial="0.0" saldo="278.0" saldoMora="278.0" cuota="139.0" porcentajeDeuda="0.0" codMenorCalificacion="4" textoMenorCalificacion="D" /><EvolucionDeudaValorTrimestre trimestre="2020/06" tipoCuenta="COM" num="1" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="0.0" porcentajeDeuda="0.0" codMenorCalificacion="0" textoMenorCalificacion="-" /><EvolucionDeudaValorTrimestre trimestre="2020/03" tipoCuenta="COM" num="1" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="0.0" porcentajeDeuda="0.0" codMenorCalificacion="0" textoMenorCalificacion="-" /><EvolucionDeudaValorTrimestre trimestre="2019/12" tipoCuenta="COM" num="1" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="0.0" porcentajeDeuda="0.0" codMenorCalificacion="0" textoMenorCalificacion="-" /><EvolucionDeudaValorTrimestre trimestre="2019/09" tipoCuenta="COM" num="1" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="0.0" porcentajeDeuda="0.0" codMenorCalificacion="0" textoMenorCalificacion="-" /><EvolucionDeudaValorTrimestre trimestre="2019/06" tipoCuenta="COM" num="1" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="0.0" porcentajeDeuda="0.0" codMenorCalificacion="0" textoMenorCalificacion="-" /></EvolucionDeudaTipoCuenta><EvolucionDeudaTipoCuenta tipoCuenta="CTC"><EvolucionDeudaValorTrimestre trimestre="2020/09" tipoCuenta="CTC" num="2" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="81.0" porcentajeDeuda="0.0" codMenorCalificacion="1" textoMenorCalificacion="A" /><EvolucionDeudaValorTrimestre trimestre="2020/06" tipoCuenta="CTC" num="2" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="81.0" porcentajeDeuda="0.0" codMenorCalificacion="1" textoMenorCalificacion="A" /><EvolucionDeudaValorTrimestre trimestre="2020/03" tipoCuenta="CTC" num="2" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="81.0" porcentajeDeuda="0.0" codMenorCalificacion="1" textoMenorCalificacion="A" /><EvolucionDeudaValorTrimestre trimestre="2019/12" tipoCuenta="CTC" num="2" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="81.0" porcentajeDeuda="0.0" codMenorCalificacion="1" textoMenorCalificacion="A" /><EvolucionDeudaValorTrimestre trimestre="2019/09" tipoCuenta="CTC" num="2" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="81.0" porcentajeDeuda="0.0" codMenorCalificacion="1" textoMenorCalificacion="A" /><EvolucionDeudaValorTrimestre trimestre="2019/06" tipoCuenta="CTC" num="2" cupoInicial="0.0" saldo="0.0" saldoMora="0.0" cuota="81.0" porcentajeDeuda="0.0" codMenorCalificacion="1" textoMenorCalificacion="A" /></EvolucionDeudaTipoCuenta></EvolucionDeudaSector></EvolucionDeuda></InfoAgregadaMicrocredito></Informe></Informes>"""

        return response

    # EVIDENTE integration methods
    async def evidente_validar(self, select: EvidenteValidarInSelect):
        # build parameter object with id of enterprise give from DataCredito
        request_data = {
            'idUsuarioEntidad': self.encrypt_base64(DATA_CREDITO_NIP.encode('utf-8')),
            'paramProducto': self.param_product_evidente,
            'producto': self.product_evidente,
            'canal': self.channel_evidente,
            'datosValidacion': {
                'identificacion': {
                    'numero': select.nip,
                    'tipo': select.identificationType
                },
                'primerApellido': select.firstName,
                'nombres': select.name,
                'fechaExpedicion': {
                    'timestamp': int(self.timestamp_from_datetime(datetime.datetime.strptime(select.nipExpiryDate, '%Y-%m-%d')))
                }
            }
        }

        logger.info(f'[Data Credito] Evidente Validar parameter: {request_data}')
        response = self.wsdl_client.service.validar(**request_data)

        return response

    async def evidente_preguntas(self, select: EvidentePreguntasInSelect):
        # build parameter object with id of enterprise give from DataCredito Evidente Preguntas

        customer_model = CustomerInSelect(
            user_id=self.user_id
        )
        customer: CustomerInResponse = await get_customer_db(self.conn, customer_model)
        select.nip = customer.identification

        request_data = {
            'idUsuarioEntidad': self.encrypt_base64(DATA_CREDITO_NIP.encode('utf-8')),
            'paramProducto': self.param_product_evidente,
            'producto': self.product_evidente,
            'canal': self.channel_evidente,
            'solicitudCuestionario': {
                'tipoId': '1',
                'identificacion': select.nip,
                'regValidacion': select.regValidacion
            }
        }

        logger.info(f'[Data Credito] Evidente Preguntas parameter: {request_data}')
        response = saxutils.unescape(self.wsdl_client.service.preguntas(**request_data))
        # response = saxutils.unescape('<?xml version="1.0" encoding="UTF-8"?><Cuestionario id="50042833" resultado="01" registro="4414324" excluirCliente="false" alertas="false" respuestaAlerta="04" codigoAlerta="04"><Pregunta id="005012002" texto="A AGOSTO DE 2020 EL SALDO DE SU CREDITO HIPOTECARIO CON BANCO POPULAR S. A. ESTABA ENTRE:" orden="1" idRespuestaCorrecta="00" peso="1"><Respuesta id="001" texto="$56,912,001 Y $69,559,000" /><Respuesta id="002" texto="$69,559,001 Y $82,206,000" /><Respuesta id="003" texto="$82,206,001 Y $94,853,000" /><Respuesta id="004" texto="$94,853,001 Y $107,500,000" /><Respuesta id="005" texto="$107,500,001 Y $120,147,000" /><Respuesta id="006" texto="NO TENGO CREDITO DE VIVIENDA CON LA ENTIDAD" /></Pregunta><Pregunta id="008003001" texto="CON CUAL DE LAS SIGUIENTES ENTIDADES HA SOLICITADO UN PRODUCTO/SERVICIO EN LOS ULTIMOS 6 MESES?" orden="2" idRespuestaCorrecta="00" peso="3"><Respuesta id="001" texto="DANN REGIONAL COMPA&#209;IA DE FINANCIAMIENTO S.A." /><Respuesta id="002" texto="CONFIRMEZA S.A.S." /><Respuesta id="003" texto="FONDO NACIONAL DE AHORRO" /><Respuesta id="004" texto="BANCO MUNDO MUJER SA" /><Respuesta id="005" texto="COMEDAL" /><Respuesta id="006" texto="NINGUNA DE LAS ANTERIORES" /></Pregunta><Pregunta id="005013002" texto="EL VALOR DE LA CUOTA DE AGOSTO DE 2020 DE SU CREDITO DE ELECTRODOMESTICOS CON DISTRIBUIDORA RAYCO S.A ESTABA ENTRE:" orden="3" idRespuestaCorrecta="00" peso="2"><Respuesta id="001" texto="$155,001 Y $217,000" /><Respuesta id="002" texto="$217,001 Y $279,000" /><Respuesta id="003" texto="$279,001 Y $341,000" /><Respuesta id="004" texto="$341,001 Y $403,000" /><Respuesta id="005" texto="$403,001 Y $465,000" /><Respuesta id="006" texto="NO TENGO CREDITO DE ELECTRODOMESTICOS CON LA ENTIDAD" /></Pregunta><Pregunta id="004006002" texto="HACE CUANTO TIEMPO TIENE USTED UNA CUENTA DE AHORRO CON BANCO AV VILLAS S.A.?" orden="4" idRespuestaCorrecta="00" peso="2"><Respuesta id="001" texto="0 A 1 A&#209;OS" /><Respuesta id="002" texto="2 A 5 A&#209;OS" /><Respuesta id="003" texto="6 A 10 A&#209;OS" /><Respuesta id="004" texto="11 A 15 A&#209;OS" /><Respuesta id="005" texto="16 A&#209;OS O MAS" /><Respuesta id="006" texto="NO TENGO CUENTA DE AHORRO CON LA ENTIDAD" /></Pregunta></Cuestionario>')
        logger.info(f'[Data Credito] Evidente Preguntas on Response {response}')

        # parse XML response
        try:
            response = xmltodict.parse(response)
        except xmltodict.expat.ExpatError:
            pass

        ws_message = {
            'questions': []
        }

        if 'Pregunta' in response['Cuestionario']:
            preguntas = response['Cuestionario']['Pregunta']

            for pregunta in preguntas:
                items = []

                if 'Respuesta' in pregunta:
                    respuestas = pregunta['Respuesta']

                    for respuesta in respuestas:
                        items.append({'id': respuesta['@id'], 'name': respuesta['@texto']})

                ws_message['questions'].append({'id': pregunta['@id'], 'name': pregunta['@texto'], 'answer': '', 'items': items, 'order': pregunta['@orden']})
                ws_message['id'] = response['Cuestionario']['@id']
                ws_message['idRegister'] = response['Cuestionario']['@registro']

            logger.info(f'Evidente answers to send by web socket: {ws_message}')
            self.send_message_to_ws(view='DataCreditoQuestionsModal', data=ws_message)
        else:
            self.send_message_to_ws(view='DataCreditoResultModal', data={'isClientAccepted': False, 'isEvidente': True})
            loop = self.get_running_loop()

            if loop and loop.is_running():
                loop.create_task(self.loan_handler.create_loan(status_id=7))  # Data Credit error
            else:
                asyncio.run(self.loan_handler.create_loan(status_id=7))  # Data Credit error

        return response

    async def evidente_verificar(self, select: EvidenteVerificarInSelect):
        # build parameter object with id of enterprise give from DataCredito
        answers = []

        for question in select.answers.questions:
            answers.append({
                'idPregunta': str(int(question.order)),
                'idRespuesta': str(int(question.answer))
            })

        select.idCuestionario = select.answers.id
        select.regCuestionario = select.answers.idRegister

        request_data = {
            'idUsuarioEntidad': self.encrypt_base64(DATA_CREDITO_NIP.encode('utf-8')),
            'paramProducto': self.param_product_evidente,
            'producto': self.product_evidente,
            'respuestas': [{
                'idCuestionario': select.idCuestionario,
                'regCuestionario': select.regCuestionario,
                'identificacion': {
                    'numero': select.nip,
                    'tipo': select.identificationType
                },
                'respuesta': answers
            }]
        }

        # request_data['respuestas'].append(*answers)
        logger.info(f'[Data Credito] Evidente Verificar parameter: {request_data}')
        response = self.wsdl_client.service.verificar(**request_data)

        return response

    @staticmethod
    def timestamp_from_datetime(dt: datetime):
        return dt.replace(tzinfo=pytz.timezone(APP_TIMEZONE)).timestamp()
