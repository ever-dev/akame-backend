import pytz
import math
from typing import Any, List
from databases import Database
from datetime import datetime, timezone, timedelta
from mamut.core.config import ALEGRA_USER, ALEGRA_TOKEN, ALEGRA_REST, ALEGRA_COUNTRY, APP_TIMEZONE
from httpx import AsyncClient, BasicAuth, Request, Timeout
from mamut.models.alegra import AlegraContactInCreate, AlegraContactIdentification, AlegraContactIdentificationTypesEnum, AlegraContactAddress, AlegraContactName, AlegraContactInUpdate, AlegraContactNameInUpdate, AlegraContactIdentificationInUpdate, AlegraContactAddressInUpdate, AlegraItemInCreate, AlegraInvoiceInCreate, AlegraPaymentInCreate, AlegraInvoiceItem, AlegraPaymentBankAccount, AlegraPaymentMethodEnum, AlegraPaymentInvoices, AlegraPaymentTypeEnum, AlegraInvoiceItemTax, AlegraCreditNoteInCreate, AlegraCreditNoteInvoices
from mamut.core.utils import create_aliased_response
from mamut.models.customer import CustomerInResponse, CustomerInUpdate, CustomerInSelect, CustomerContactInformation
from mamut.crud.customers import update_client_db, get_customer_db
from mamut.crud.api import insert_api_transaction_log
from mamut.models.api import ApiTransactionLogInInsert
from mamut.models.loan_payment_plan import LoanPaymentPlanInDb, LoanPaymentPlanInUpdate, LoanPaymentPlanInSelect
from mamut.crud.loan_payment_plan import update_loan_payment_plan, select_loan_payment_plan
from mamut.crud.loan import select_loans, update_loan
from mamut.models.loan import LoanInSelect, LoanInUpdate
from mamut.crud.loans_configuration import select_loans_configuration
from mamut.models.loans_configuration import LoansConfigurationInSelect
from mamut.models.calculator import CalculatorInCreate
from mamut.bl.calculator import calculate_loan
from mamut.crud.loan_payment_plan_movements import insert_loan_payment_plan_movement, update_loan_payment_plan_movement
from mamut.models.loan_payment_plan_movements import LoanPaymentPlanMovementInInsert, LoanPaymentPlanMovementInUpdate
from mamut.bl.loan_handler import LoanHandler
from mamut.conn.postgresql.postgresql import connect_pg_database, close_pg_connection
from mamut.models.loan import LoanInDb


class AlegraClient:
    def __init__(self, pg_conn: Database, user_id: int):
        self.pg_conn = pg_conn
        self.__alegra_auth = BasicAuth(username=ALEGRA_USER, password=ALEGRA_TOKEN)
        self.__contacts_route = '/contacts'
        self.__items_route = '/items'
        self.__invoices_route = '/invoices'
        self.__payments_route = '/payments'
        self.__credit_note_route = '/credit-notes'
        self.user_id = user_id

    async def upsert_customer(self, mamut_customer: CustomerInResponse, create_new_connection: bool = None) -> bool:
        pg_conn_to_use = self.pg_conn
        try:
            if create_new_connection is not None and create_new_connection:
                pg_conn_to_use = await connect_pg_database(True)

            result = False
            contact_info_list: List[CustomerContactInformation] = mamut_customer.contact_information
            contact_info = contact_info_list[0]

            if mamut_customer.alegra_contact_id is None: # Create in Alegra
                # Check if the customer is already created in Alegra
                alegra_contacts = await self.get_contacts(None, mamut_customer.identification)
                customer_in_alegra = None
                if alegra_contacts is not None and len(alegra_contacts) > 0:
                    customer_in_alegra = alegra_contacts[0]

                # Create the customer in Alegra if doesn't exits
                create_result = None
                if customer_in_alegra is None:
                    alegra_contact = AlegraContactInCreate(
                        name=AlegraContactName(
                            first_name=mamut_customer.name,
                            second_name=mamut_customer.middle_name,
                            last_name=mamut_customer.first_name + ' ' + mamut_customer.last_name
                        ),
                        identification_object=AlegraContactIdentification(
                            type_=AlegraContactIdentificationTypesEnum.cedula_ciudadania if mamut_customer.identification_type_id == 1 else AlegraContactIdentificationTypesEnum.cedula_extranjeria,
                            number=mamut_customer.identification
                        ),
                        phone_primary=contact_info.telephone1 if len(contact_info.telephone1) > 0 else contact_info.cellphone,
                        mobile=contact_info.cellphone,
                        email=contact_info.customer_email,
                        address=AlegraContactAddress(
                            description=contact_info.address,
                            department=mamut_customer.current_departament,
                            city=mamut_customer.current_city,
                            country=ALEGRA_COUNTRY,
                            zip_code=None
                        )
                    )
                    create_result = await self.create_contact(alegra_contact)

                alegra_customer_id = None
                if customer_in_alegra is not None:
                    alegra_customer_id = customer_in_alegra['id']
                elif create_result is not None:
                    alegra_customer_id = create_result['id']

                try:
                    await update_client_db(pg_conn_to_use, CustomerInUpdate(
                        identification=mamut_customer.identification,
                        alegra_contact_id=alegra_customer_id
                    ))
                    result = True
                except Exception as ex:
                    print(f'Error while trying to update the customer: {str(ex)}')
                    await self.delete_contact(alegra_customer_id)
            else: # If we already have an alegra id for the customer, then update in Alegra de customer information
                alegra_contact = AlegraContactInUpdate(
                    id=mamut_customer.alegra_contact_id,
                    name=AlegraContactNameInUpdate(
                        first_name=mamut_customer.name,
                        second_name=mamut_customer.middle_name,
                        last_name=mamut_customer.first_name + ' ' + mamut_customer.last_name
                    ),
                    identification_object=AlegraContactIdentificationInUpdate(
                        type_=AlegraContactIdentificationTypesEnum.cedula_ciudadania if mamut_customer.identification_type_id == 1 else AlegraContactIdentificationTypesEnum.cedula_extranjeria,
                        number=mamut_customer.identification
                    ),
                    phone_primary=contact_info.telephone1 if len(contact_info.telephone1) > 0 else contact_info.cellphone,
                    mobile=contact_info.cellphone,
                    email=contact_info.customer_email,
                    address=AlegraContactAddressInUpdate(
                        description=contact_info.address,
                        department=mamut_customer.current_departament,
                        city=mamut_customer.current_city,
                        country=ALEGRA_COUNTRY,
                        zip_code=None
                    )
                )
                result = await self.update_contact(alegra_contact)
        finally:
            if create_new_connection is not None and create_new_connection:
                await close_pg_connection(pg_conn_to_use)

        return result


    async def create_mamut_invoice(self, payment_plan: LoanPaymentPlanInDb, items: List[AlegraInvoiceItem]=None) -> bool:
        result = False

        if payment_plan.loans_payment_plan_id is None:
            raise ValueError('Payment plan object must have value for primary key.')

        invoice_date = payment_plan.start_date.replace(tzinfo=pytz.timezone(APP_TIMEZONE)).strftime('%Y-%m-%d')
        invoice_due_date = (payment_plan.start_date + timedelta(days=(payment_plan.end_date - payment_plan.start_date).days)).replace(tzinfo=pytz.timezone(APP_TIMEZONE)).strftime('%Y-%m-%d')

        loan = (await select_loans(self.pg_conn, LoanInSelect(
            financial_loans_id=payment_plan.financial_loans_id
        )))[0]

        loan_configuration = (await select_loans_configuration(self.pg_conn, LoansConfigurationInSelect(
            loans_configuration_id=loan.loans_configuration_id
        )))[0]

        person = (await get_customer_db(self.pg_conn, CustomerInSelect(
            persons_id=loan.persons_id
        )))

        used_calculator = CalculatorInCreate(
            amount=loan.request_amount,
            payments_quantity=1,
            term_in_days=(payment_plan.end_date - payment_plan.start_date).days
        )

        loan_calculation = calculate_loan(loan_configuration, used_calculator)

        interests_item = AlegraInvoiceItem(
            id=7,
            price=float(loan_calculation.total_interests),
            quantity=1
        )

        administration_item = AlegraInvoiceItem(
            id=2,
            price=float(loan_calculation.administration),
            quantity=1,
            tax=[AlegraInvoiceItemTax(
                id='3'
            )]
        )

        digital_services_item = AlegraInvoiceItem(
            id=1,
            price=float(loan_calculation.total_digital_services),
            quantity=1,
            tax=[AlegraInvoiceItemTax(
                id='3'
            )]
        )
        
        invoice = AlegraInvoiceInCreate(
            date=invoice_date,
            due_date=invoice_due_date,
            client=person.alegra_contact_id,
            anotation='Servicios del préstamo No. ' + str(loan.financial_loans_id) + ' por ' + "${:,}".format(math.trunc(float(loan_calculation.loan_amount))) + '.',
            items=[
                interests_item,
                administration_item,
                digital_services_item
            ] if items is None else items
        )

        created_invoice = await self.create_invoice(invoice)

        if created_invoice is not None:
            try:
                fmsf_code = str(created_invoice.get('numberTemplate', {}).get('prefix', '')) + str(created_invoice.get('numberTemplate', {}).get('number', ''))
                await update_loan_payment_plan(self.pg_conn, LoanPaymentPlanInUpdate(
                    loans_payment_plan_id=payment_plan.loans_payment_plan_id,
                    alegra_id=created_invoice['id'],
                    fmsf_code=fmsf_code if len(fmsf_code) > 0 else None
                ))
                result = True
            except Exception as ex:
                print(f'Error while trying to update the payment plan: {str(ex)}')
                await self.cancel_invoice(created_invoice['id'], 'Factura anulada porque ocurrió un error mientras se actualizaba el id (payment plan) en la aplicación de Mamut.')

        return result

    async def create_mamut_credit_note(self, interests_amount: float, administration_amount: float, digital_services_amount: float, loan: LoanInDb, invoice_id: int) -> bool:
        result = False

        interests_item = AlegraInvoiceItem(
            id=7,
            price=float(interests_amount),
            quantity=1
        )

        administration_item = AlegraInvoiceItem(
            id=2,
            price=float(administration_amount),
            quantity=1,
            tax=[AlegraInvoiceItemTax(
                id='3'
            )]
        )

        digital_services_item = AlegraInvoiceItem(
            id=1,
            price=float(digital_services_amount),
            quantity=1,
            tax=[AlegraInvoiceItemTax(
                id='3'
            )]
        )

        items = []
        total = 0

        if interests_item.price > 0:
            total = total + interests_item.price
            items.append(interests_item)
    
        if administration_item.price > 0:
            total = total + administration_item.price
            items.append(administration_item)

        if digital_services_item.price > 0:
            total = total + digital_services_item.price
            items.append(digital_services_item)

        person = (await get_customer_db(self.pg_conn, CustomerInSelect(
            persons_id=loan.persons_id
        )))

        invoice_date = datetime.utcnow().replace(tzinfo=pytz.timezone(APP_TIMEZONE)).strftime('%Y-%m-%d')
        credit_note = AlegraCreditNoteInCreate(
            date=invoice_date,
            cause='Cancelacion de prestamo antes de vencimiento.',
            client=person.alegra_contact_id,
            items=items,
            invoices=[
                AlegraCreditNoteInvoices(
                    id=invoice_id,
                    amount=total
                )
            ]
        )

        created_credit_note = await self.create_credit_note(credit_note)

        return created_credit_note

    def __get_totals(self, loan_configuration, used_calculator, payment_plan):
        loan_calculation = calculate_loan(loan_configuration, used_calculator)

        total_interests = float(loan_calculation.total_interests) - payment_plan.alegra_info.get('paidInterests', 0)
        total_administration = (float(loan_calculation.administration) + (loan_configuration.vat * float(loan_calculation.administration))) - payment_plan.alegra_info.get('paidAdministration', 0)
        total_digital_services = (float(loan_calculation.total_digital_services) + (loan_configuration.vat * float(loan_calculation.total_digital_services))) - payment_plan.alegra_info.get('paidDigitalServices', 0)

        return total_interests, total_administration, total_digital_services, loan_calculation

    async def apply_mamut_payment(self, customer: CustomerInResponse, amount: float, reference_id: str, payment_timestamp: int) -> bool:
        original_amount = amount
        invoices_paid = []
        payments_ids_alegra = []
        payment_date = datetime.fromtimestamp(payment_timestamp)
        now_date = datetime.utcnow()
        remaining_payment_days = (now_date - payment_date).days
        async with self.pg_conn.transaction():
            try:
                # Update mora before apply the payments
                loan_handler = LoanHandler(self.pg_conn, user_id=customer.user_id)
                await loan_handler.update_payment_plan(user_id=customer.user_id, just_update_mora=True)

                payment_plans = await select_loan_payment_plan(self.pg_conn, LoanPaymentPlanInSelect(
                    persons_id=customer.persons_id,
                    not_payment_status_id=1
                ))

                for payment_plan in payment_plans:
                    if payment_plan.alegra_id is not None:
                        loan = (await select_loans(self.pg_conn, LoanInSelect(
                            financial_loans_id=payment_plan.financial_loans_id
                        )))[0]

                        loan_configuration = (await select_loans_configuration(self.pg_conn, LoansConfigurationInSelect(
                            loans_configuration_id=loan.loans_configuration_id
                        )))[0]


                        days_to_use = (payment_plan.end_date - payment_plan.start_date).days

                        used_calculator = CalculatorInCreate(
                            amount=loan.request_amount,
                            payments_quantity=1,
                            term_in_days=days_to_use
                        )

                        total_interests_complete, total_administration_complete, total_digital_services_complete, loan_calculation = self.__get_totals(loan_configuration, used_calculator, payment_plan)

                        # If the customer can pay the total balance (today balance) of the loan
                        # then calculate the variable amounts with days since start (from start date to today)
                        early_payment = False
                        if remaining_payment_days < days_to_use:
                            days_to_use = remaining_payment_days
                            early_payment = True

                        used_calculator = CalculatorInCreate(
                            amount=loan.request_amount,
                            payments_quantity=1,
                            term_in_days=days_to_use
                        )

                        total_interests, total_administration, total_digital_services, loan_calculation = self.__get_totals(loan_configuration, used_calculator, payment_plan)
                        
                        if payment_plan.fee_balance > loan.request_amount:
                            payment_plan.fee_balance = float(loan_calculation.total)

                        # Make the credit note required
                        if early_payment:
                            credit_note_total_interests = total_interests_complete - total_interests
                            credit_note_total_administration = total_administration_complete - total_administration
                            # Substract VAT because it will be added by Alegra in the credit note
                            credit_note_total_administration = credit_note_total_administration - (loan_configuration.vat * credit_note_total_administration)
                            credit_note_total_digital_services = total_digital_services_complete - total_digital_services
                            # Substract VAT because it will be added by Alegra in the credit note
                            credit_note_total_digital_services = credit_note_total_digital_services - (loan_configuration.vat * credit_note_total_administration)
                            await self.create_mamut_credit_note(credit_note_total_interests, credit_note_total_administration, credit_note_total_digital_services, loan, payment_plan.alegra_id)

                        # Apply payment to mora
                        old_mora = payment_plan.mora
                        payment_plan.mora = payment_plan.mora - amount
                        paid_mora = old_mora - payment_plan.mora

                        if payment_plan.mora < 0:
                            payment_plan.mora = 0
                            paid_mora = old_mora

                        amount = amount - paid_mora

                        if amount < 0:
                            amount = 0

                        # Apply payment to interests
                        old_interests = total_interests
                        new_interests = total_interests - amount
                        paid_interests = old_interests - new_interests

                        if new_interests < 0:
                            new_interests = 0
                            paid_interests = old_interests

                        amount = amount - paid_interests

                        payment_plan.alegra_info['paidInterests'] = payment_plan.alegra_info.get('paidInterests', 0) + paid_interests

                        if amount < 0:
                            amount = 0

                        # Apply payment to administration
                        old_administration = total_administration
                        new_administration = total_administration - amount
                        paid_administration = old_administration - new_administration

                        if new_administration < 0:
                            new_administration = 0
                            paid_administration = old_administration

                        amount = amount - paid_administration

                        payment_plan.alegra_info['paidAdministration'] = payment_plan.alegra_info.get('paidAdministration', 0) + paid_administration

                        if amount < 0:
                            amount = 0

                        # Apply payment to digital_services
                        old_digital_services = total_digital_services
                        new_digital_services = total_digital_services - amount
                        paid_digital_services = old_digital_services - new_digital_services

                        if new_digital_services < 0:
                            new_digital_services = 0
                            paid_digital_services = old_digital_services

                        amount = amount - paid_digital_services

                        payment_plan.alegra_info['paidDigitalServices'] = payment_plan.alegra_info.get('paidDigitalServices', 0) + paid_digital_services

                        if amount < 0:
                            amount = 0

                        # Apply payment to remaining balance
                        old_fee_balance = payment_plan.fee_balance
                        new_fee_balance = payment_plan.fee_balance - paid_mora - paid_interests - paid_administration - paid_digital_services - amount
                        paid_fee_balance = old_fee_balance - new_fee_balance

                        if new_fee_balance < 0:
                            amount = abs(new_fee_balance)
                            new_fee_balance = 0
                            paid_fee_balance = old_fee_balance
                        else:
                            amount = amount - paid_fee_balance

                        if amount < 0:
                            amount = 0
                        # ---
                        
                        new_payment_status_id = 4
                        if new_fee_balance <= 0:
                            new_payment_status_id = 1
                        payment_plan.payment_status_id = new_payment_status_id
                        # ---
                        
                        # The total capital remaining will be the requested amount if the fee balance is greather than the requested amount
                        # otherwise the remaining capital will be the fee_balance
                        total_capital_remaining = loan.request_amount if new_fee_balance > loan.request_amount else new_fee_balance
                        # ---

                        total_alegra_paid = paid_interests + paid_administration + paid_digital_services

                        total_capital_paid = paid_fee_balance - paid_mora - paid_interests - paid_administration - paid_digital_services

                        if total_capital_paid < 0:
                            total_capital_paid = 0

                        payment_plan.fee_balance = new_fee_balance

                        payment_movement_insert = LoanPaymentPlanMovementInInsert(
                            loans_payment_plan_id=payment_plan.loans_payment_plan_id,
                            amount=original_amount,
                            interest=paid_interests,
                            capital=total_capital_paid,
                            mora=paid_mora,
                            total_balance=new_fee_balance,
                            payment_date=payment_date,
                            total_capital=total_capital_remaining,
                            payment_method_id=2,
                            ach_id=reference_id,
                            alegra_id='',
                            last_modified_by=customer.user_id,
                            extra_payment=amount,
                            extra_info={
                                'paidInterests': paid_interests,
                                'paidAdministration': paid_administration,
                                'paidDigitalServices': paid_digital_services
                            }
                        )

                        payment_movement_id = await insert_loan_payment_plan_movement(self.pg_conn, payment_movement_insert)

                        if not payment_plan.paid_in_alegra:
                            invoices_paid.append({
                                'id': payment_plan.alegra_id,
                                'amount': total_alegra_paid,
                                'payment_plan_movements_id': payment_movement_id
                            })

                            # If with this payment we pay everything in Alegra, set the flag to True
                            if new_interests == 0 and new_administration == 0 and new_digital_services == 0:
                                payment_plan.paid_in_alegra = True
                        
                        await update_loan_payment_plan(self.pg_conn, LoanPaymentPlanInUpdate(
                            **payment_plan.dict()
                        ))

                        if payment_plan.fee_balance == 0: # If the only fee is already paid, set the loan to paid status
                            await update_loan(self.pg_conn, LoanInUpdate(financial_loans_id=loan.financial_loans_id, loans_status_id=4, paid_date=datetime.utcnow()))

                        if amount <= 0:
                            break
                        else:
                            original_amount = amount
        
                alegra_paid_invoices: List[AlegraPaymentInvoices] = []

                for paid_invoice in invoices_paid:
                    alegra_paid_invoices.append(AlegraPaymentInvoices(
                        **paid_invoice
                    ))

                alegra_created_payment = await self.create_payment(AlegraPaymentInCreate(
                    date=payment_date.replace(tzinfo=pytz.timezone(APP_TIMEZONE)).strftime('%Y-%m-%d'),
                    bank_account=AlegraPaymentBankAccount(
                        id=1
                    ),
                    payment_method=AlegraPaymentTypeEnum.contado,
                    invoices=alegra_paid_invoices
                ))

                if alegra_created_payment is None:
                    raise ValueError('Error while creating payments, check alegra api log for details.')

                payments_ids_alegra.append(alegra_created_payment['id'])

                for paid_invoice in invoices_paid:
                    fmsf_code = str(alegra_created_payment.get('numberTemplate', {}).get('prefix', '')) + str(alegra_created_payment.get('numberTemplate', {}).get('number', ''))
                    await update_loan_payment_plan_movement(self.pg_conn,
                    LoanPaymentPlanMovementInUpdate(
                        payment_plan_movements_id=paid_invoice['payment_plan_movements_id'],
                        alegra_id=alegra_created_payment['id'],
                        fmsf_code=fmsf_code if len(fmsf_code) > 0 else None
                    ))
            except Exception as e:
                for alegra_payment_id in payments_ids_alegra:
                    await self.delete_payment(alegra_payment_id)
                raise Exception('Error ocurred while applying payment. ' + str(e) + '. Rollback payments in Alegra.')


        return True

    async def get_contacts(self, alegra_id: str = None, identification: str = None) -> Any:
        endpoint_response = None
        endpoint_url = ALEGRA_REST + self.__contacts_route
        if alegra_id is not None:
            endpoint_url = ALEGRA_REST + self.__contacts_route + '/' + str(alegra_id)

        if identification is not None:
            endpoint_url = endpoint_url + '?identification=' + identification

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            r = await client.get(endpoint_url)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass


            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code != 404:
                endpoint_response = rjson

        return endpoint_response

    async def create_contact(self, contact: AlegraContactInCreate) -> Any:
        endpoint_response = None
        endpoint_url = ALEGRA_REST + self.__contacts_route

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            body = create_aliased_response(contact)
            r = await client.post(endpoint_url, json=body)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, data=body, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code == 201:
                endpoint_response = rjson

        return endpoint_response

    async def update_contact(self, contact: AlegraContactInUpdate) -> bool:
        endpoint_response = False
        endpoint_url = ALEGRA_REST + self.__contacts_route

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            body = self.__clean_nulls_from_dict(create_aliased_response(contact))
            r = await client.put(endpoint_url, json=body)

            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, data=body, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code == 200:
                endpoint_response = True

        return endpoint_response

    async def delete_contact(self, alegra_id: int) -> bool:
        endpoint_response = False
        endpoint_url = ALEGRA_REST + self.__contacts_route + '/' + str(alegra_id)

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            r = await client.delete(endpoint_url)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code == 200:
                endpoint_response = True
        
        return endpoint_response

    async def create_item(self, item: AlegraItemInCreate) -> Any:
        endpoint_response = None
        endpoint_url = ALEGRA_REST + self.__items_route

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            body = create_aliased_response(item)
            r = await client.post(endpoint_url, json=body)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, data=body, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code == 201:
                endpoint_response = rjson

        return endpoint_response

    async def get_items(self, alegra_id: int = None, params: dict = None) -> Any:
        endpoint_response = None
        endpoint_url = ALEGRA_REST + self.__items_route
        if alegra_id is not None:
            endpoint_url = ALEGRA_REST + self.__items_route + '/' + str(alegra_id)

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            r = await client.get(endpoint_url, params=params)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code != 404:
                endpoint_response = rjson

        return endpoint_response

    async def create_invoice(self, invoice: AlegraInvoiceInCreate) -> Any:
        endpoint_response = None
        endpoint_url = ALEGRA_REST + self.__invoices_route

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            body = create_aliased_response(invoice)
            r = await client.post(endpoint_url, json=body)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, data=body, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code == 201:
                endpoint_response = rjson

        return endpoint_response

    async def get_invoices(self, alegra_id: int = None, params: dict = None) -> Any:
        endpoint_response = None
        endpoint_url = ALEGRA_REST + self.__invoices_route
        if alegra_id is not None:
            endpoint_url = ALEGRA_REST + self.__invoices_route + '/' + str(alegra_id)

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            r = await client.get(endpoint_url, params=params)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code != 404:
                endpoint_response = rjson

        return endpoint_response

    async def cancel_invoice(self, alegra_id: int, cause: str) -> bool:
        endpoint_response = False
        endpoint_url = ALEGRA_REST + self.__invoices_route + '/' + str(alegra_id) + '/void'

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            body = {
                'cause': cause
            }
            r = await client.post(endpoint_url, json=body)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, data=body, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code == 200:
                endpoint_response = True

        return endpoint_response

    async def create_payment(self, payment: AlegraPaymentInCreate) -> Any:
        endpoint_response = None
        endpoint_url = ALEGRA_REST + self.__payments_route

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            body = create_aliased_response(payment)
            r = await client.post(endpoint_url, json=body)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, data=body, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code == 201:
                endpoint_response = rjson

        return endpoint_response

    async def delete_payment(self, alegra_id: int) -> bool:
        endpoint_response = False
        endpoint_url = ALEGRA_REST + self.__payments_route + '/' + str(alegra_id)

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            r = await client.delete(endpoint_url)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code == 200:
                endpoint_response = True
        
        return endpoint_response

    async def create_credit_note(self, invoice: AlegraInvoiceInCreate) -> Any:
        endpoint_response = None
        endpoint_url = ALEGRA_REST + self.__credit_note_route

        async with AsyncClient(auth=self.__alegra_auth, timeout=Timeout(timeout=60)) as client:
            body = create_aliased_response(invoice)
            r = await client.post(endpoint_url, json=body)
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            new_db_connection = await connect_pg_database(True)
            try:
                await insert_api_transaction_log(new_db_connection, ApiTransactionLogInInsert(
                    api_id=3,
                    user_id=self.user_id,
                    api_log=self.__craft_debug_object(client, r.request, data=body, response=r.content.decode('utf-8', errors='replace') if rjson is None else None, response_json=rjson if rjson is not None else None)
                ))
            finally:
                await close_pg_connection(new_db_connection)

            if r.status_code == 201:
                endpoint_response = rjson

        return endpoint_response

    def __clean_nulls_from_dict(self, original_dict: dict) -> dict:
        return {k: v for k, v in original_dict.items() if v is not None}

    def __craft_debug_object(self, client: AsyncClient, request: Request, params: dict=None, data: dict=None, response_json: Any=None, response: str=None) -> dict:
        return {
            'auth': client.auth.__dict__,
            'endpoint_url': str(request.url),
            'method': request.method,
            'params': params,
            'bodyPayload': data,
            'responseJson': response_json,
            'response': response
        }
