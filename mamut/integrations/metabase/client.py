import jwt
import time
from typing import List
from httpx import AsyncClient
from mamut.models.metabase import MetabaseEmbeddableDashboardInResponse, MetabaseEmbeddableDashboardUrlInResponse
from mamut.core.config import METABASE_SECRET_KEY, METABASE_SITE_URL, METABASE_TOKEN_EXPIRATION_MINUTES, METABASE_SUPERUSER_EMAIL, METABASE_SUPERUSER_PASSWORD


class MetabaseClient:
    def __init__(self):
        self.session_header = None

    def get_dashboard_url(self, dashboard_id: int) -> MetabaseEmbeddableDashboardUrlInResponse:
        token = self.get_token(dashboard_id)
        return MetabaseEmbeddableDashboardUrlInResponse(url=METABASE_SITE_URL + '/embed/dashboard/' + token + '#bordered=true&titled=true')

    async def get_dashboards_embeddable(self) -> List[MetabaseEmbeddableDashboardInResponse]:
        endpoint_response: List[MetabaseEmbeddableDashboardInResponse] = []
        await self.request_session()
        async with AsyncClient(headers=self.session_header) as client:
            r = await client.get(METABASE_SITE_URL + '/api/dashboard/embeddable')
            rjson = None

            try:
                rjson = r.json()
            except Exception:
                pass

            if r.status_code == 200:
                if len(rjson) > 0:
                    for dashboard in rjson:
                        endpoint_response.append(MetabaseEmbeddableDashboardInResponse(**dashboard))

        return endpoint_response

    async def request_session(self):
        if not await self.validate_session():
            credentials_payload = {
                'username': METABASE_SUPERUSER_EMAIL,
                'password': METABASE_SUPERUSER_PASSWORD
            }
            async with AsyncClient() as client:
                r = await client.post(METABASE_SITE_URL + '/api/session', json=credentials_payload)
                rjson = None

                try:
                    rjson = r.json()
                except Exception:
                    pass

                if r.status_code == 200:
                    self.session_header = {'X-Metabase-Session': rjson['id']}

    async def validate_session(self) -> bool:
        is_valid = False
        if self.session_header is not None:
            async with AsyncClient(headers=self.session_header) as client:
                r = await client.get(METABASE_SITE_URL + '/api/user/current', headers=self.session_header)

                if r.status_code == 200:
                    is_valid = True

        return is_valid

    def get_token(self, dashboard_id: int) -> str:
        payload = {
            'resource': {'dashboard': dashboard_id},
            'params': {
                
            },
            'exp': round(time.time()) + (60 * int(METABASE_TOKEN_EXPIRATION_MINUTES))
        }
        token = jwt.encode(payload, METABASE_SECRET_KEY, algorithm="HS256")

        token_string = token.decode('utf-8')

        return token_string
