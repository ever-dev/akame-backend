import json
from datetime import datetime, timedelta
from loguru import logger
from aioredis import Redis
from databases import Database
from mamut.models.loan_attempt import LoanAttemptInSelect, LoanAttemptInInsert, LoanAttemptInfoInInsert
from mamut.core.config import LOAN_CONFIGURATION_CACHE_EXP_SECONDS
from mamut.core.utils import create_aliased_response
from mamut.bl.loans_configuration import LoanConfigurationHandler
from mamut.models.calculator import CalculatorInCreate


class LoanHandler:
    def __init__(self, pg_conn: Database, user_id: int, redis_conn: Redis = None):
        self.pg_conn = pg_conn
        self.user_id = user_id
        self.redis_conn = redis_conn
        if self.redis_conn is None:
            logger.info('Redis connection is not available, fetch user loan configuration will be slow.')

    async def __generate_loan_attempt_info(self, calculator_config: CalculatorInCreate, loans_configuration_id: int) -> LoanAttemptInfoInInsert:
        start_date = datetime.utcnow()
        end_date = (start_date + timedelta(days=calculator_config.term_in_days))
        loan_config_handler = LoanConfigurationHandler(self.pg_conn, self.redis_conn)
        loan_config = await loan_config_handler.get_active_loan_configuration(loans_configuration_id=loans_configuration_id)
        loan_attempt_info = LoanAttemptInfoInInsert(
            calculator_config=calculator_config,
            start_date=start_date,
            end_date=end_date,
            loans_configuration_id=loan_config.loans_configuration_id
        )
        return loan_attempt_info

    async def save_current_customer_loan_configuration(self, loan_config: CalculatorInCreate, loans_configuration_id: int) -> int:
        loan_info = json.dumps(create_aliased_response(await self.__generate_loan_attempt_info(loan_config, loans_configuration_id)))
        if self.redis_conn is not None:
            await self.redis_conn.set('loan_config_user_' + str(self.user_id), loan_info, expire=LOAN_CONFIGURATION_CACHE_EXP_SECONDS)
        loan_insert = LoanAttemptInInsert(info=loan_info, owner_user_id = self.user_id, last_modified_by = self.user_id)
        loan_attempt_id = await insert_loan_attempt(self.pg_conn, loan_insert)
        return loan_attempt_id

    async def get_current_customer_loan_configuration(self) -> LoanAttemptInfoInInsert:
        loan_config = None
        if self.redis_conn is not None:
            loan_config_from_redis = await self.redis_conn.get('loan_config_user_' + str(self.user_id))
            if loan_config_from_redis is not None:
                loan_config = LoanAttemptInfoInInsert(**json.loads(loan_config_from_redis))

        if loan_config is None:
            loan_attempts = await select_loan_attempt(self.pg_conn, LoanAttemptInSelect(owner_user_id=self.user_id))
            if len(loan_attempts) > 0:
                loan_config = LoanAttemptInfoInInsert(**json.loads(loan_attempts[0].info))

        return loan_config


class UserWithoutPersonException(Exception):
    def __init__(self, message='User without person assigned'):
        super().__init__(message)
