from typing import Any
from databases import Database
from mamut.models.setting import SettingInSelect, SettingPythonTypesEnum, SettingInInsert, SettingInDb, SettingInUpdate
from mamut.crud.setting import select_setting, insert_setting, update_setting


class SettingsHandler:
    def __init__(self, pg_conn: Database, user_id: int = None):
        self.pg_conn = pg_conn
        self.user_id = user_id

    async def get_setting(self, name: str) -> Any:
        setting = await select_setting(self.pg_conn, SettingInSelect(
            setting=name
        ))

        return self.__cast_setting(setting.value, setting.python_type) if setting is not None else setting

    def __cast_setting(self, value: str, python_type: SettingPythonTypesEnum) -> Any:
        if python_type == SettingPythonTypesEnum.integer:
            return int(value)
        elif python_type == SettingPythonTypesEnum.boolean:
            return True if value == 'True' else False
        return value

    async def upsert_setting(self, name: str, value: Any):
        setting = await self.get_setting(name)

        if setting is None:
            await insert_setting(self.pg_conn, SettingInInsert(
                setting=name,
                value=str(value),
                description=self.__get_description(name),
                python_type=type(value).__name__,
                last_modified_by=self.user_id
            ))
        else:
            await update_setting(self.pg_conn, SettingInUpdate(
                setting=name,
                value=str(value),
                description=self.__get_description(name),
                python_type=type(value).__name__,
                last_modified_by=self.user_id
            ))
        return value

    def __get_description(self, name: str):
        descriptions = {
            'GENERATE_DISBURSEMENT_FILE_BUTTON_IS_ENABLE': 'This setting is used to enable or disable the generate disbursement file button.'
        }
        return descriptions[name]
