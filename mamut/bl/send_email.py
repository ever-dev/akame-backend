from string import Template
from mamut.core.smtp_handler import SMTPHandler, EmailFile
from mamut.core.config import FRONTEND_BASE_URL, TOKEN_MINUTES_EXPIRATION


class SendEmail:
    def __init__(self):
        self.smtp_handler = SMTPHandler()

    async def send_account_confirmation(self, to, language, token, username):
        subject = 'Account confirmation'

        if language == 'es':
            subject = 'Confirmación de cuenta'

        with open('./mamut/static/email/account-confirmation/email_prod.html', mode='r', encoding='utf-8') as html:
            html_content = html.read()
            template = Template(html_content)
            html_content = template.safe_substitute(token=FRONTEND_BASE_URL + '/account-confirmation/' + token.decode('utf-8'), username=username)

            await self.smtp_handler.send_html_email(to, subject, html_content, [EmailFile(path='./mamut/static/email/account-confirmation/images/mamut_horizontal_azul.png', name='mamut_logo.png')])

    async def send_disbursement_error(self, to, language, error):
        subject = 'Error in disbursement'

        if language == 'es':
            subject = 'Error en desembolso'

        with open('./mamut/static/email/denied-by-ach/email_prod.html', mode='r', encoding='utf-8') as html:
            html_content = html.read()
            template = Template(html_content)
            html_content = template.safe_substitute(error=error)

            await self.smtp_handler.send_html_email(to, subject, html_content, [EmailFile(path='./mamut/static/email/denied-by-ach/images/mamut_horizontal_azul.png', name='mamut_logo.png')])

    async def send_disbursement_success(self, to, language):
        subject = 'Disbursement successfully'

        if language == 'es':
            subject = 'Desembolso realizado'

        with open('./mamut/static/email/success-by-ach/email_prod.html', mode='r', encoding='utf-8') as html:
            html_content = html.read()
            template = Template(html_content)
            html_content = template.safe_substitute()

            await self.smtp_handler.send_html_email(to, subject, html_content, [EmailFile(path='./mamut/static/email/success-by-ach/images/mamut_horizontal_azul.png', name='mamut_logo.png')])

    async def send_payment_processed(self, to, language, amount):
        subject = 'Payment processed'

        if language == 'es':
            subject = 'Pago procesado'

        with open('./mamut/static/email/payment-processed/email_prod.html', mode='r', encoding='utf-8') as html:
            html_content = html.read()
            template = Template(html_content)
            html_content = template.safe_substitute(amount=amount)

            await self.smtp_handler.send_html_email(to, subject, html_content, [EmailFile(path='./mamut/static/email/payment-processed/images/mamut_horizontal_azul.png', name='mamut_logo.png')])

    async def send_change_password(self, to, language, token):
        subject = 'Change password'

        if language == 'es':
            subject = 'Cambiar contraseña'

        with open('./mamut/static/email/forgot-password/email_prod.html', mode='r', encoding='utf-8') as html:
            html_content = html.read()
            template = Template(html_content)
            html_content = template.safe_substitute(token=FRONTEND_BASE_URL + '/forgot-password/' + token.decode('utf-8'), minutes=TOKEN_MINUTES_EXPIRATION)

            await self.smtp_handler.send_html_email(to, subject, html_content, [EmailFile(path='./mamut/static/email/forgot-password/images/mamut_horizontal_azul.png', name='mamut_logo.png')])

    async def send_contact_form(self, to, name, message, language):
        subject = 'Contact Form - ' + name

        if language == 'es':
            subject = 'Formulario de Contacto - ' + name

        await self.smtp_handler.send_email(to, subject, message)
