import math
from mamut.models.calculator import CalculatorDetailsInResponse, CalculatorInCreate
from mamut.models.loans_configuration import LoansConfigurationInDb


def calculate_loan(loans_configuration: LoansConfigurationInDb, calculator_params: CalculatorInCreate) -> CalculatorDetailsInResponse:
    total_interests_percentage = loans_configuration.ted_interest * calculator_params.term_in_days
    
    total_interests = calculator_params.amount * total_interests_percentage

    insurance = loans_configuration.fixed_insurance
    # if calculator_params.term_in_days >= 30:
    #     insurance = (calculator_params.amount * (loans_configuration.insurance * (calculator_params.term_in_days / 30)))
    
    fixed_digital_services = 0
    if calculator_params.term_in_days < 20:
        fixed_digital_services = loans_configuration.fixed_digital_services

    variable_digital_services = (loans_configuration.digital_services_per_day * calculator_params.term_in_days)

    total_digital_services = fixed_digital_services + variable_digital_services

    disbursement = loans_configuration.disbursement

    credit_validation = loans_configuration.credit_validation
    # if calculator_params.term_in_days > 30:
    #     credit_validation = loans_configuration.credit_validation * (calculator_params.term_in_days / 30)

    promissory_note_and_custody = loans_configuration.promissory_note_and_custody

    endorsement = calculator_params.amount * loans_configuration.endorsement
    # if calculator_params.term_in_days > 30:
    #     endorsement = endorsement * (calculator_params.term_in_days / 30)

    collection = (loans_configuration.collection * calculator_params.payments_quantity)
    total_for_vat = insurance + total_digital_services + disbursement + credit_validation + promissory_note_and_custody + endorsement + collection
    vat = ((total_for_vat)*loans_configuration.vat)
    total = calculator_params.amount + total_interests + total_for_vat + vat
    payment_amount = total / calculator_params.payments_quantity

    administration = collection + disbursement + credit_validation + promissory_note_and_custody # This calculation is just for frontend

    return CalculatorDetailsInResponse(
        loan_amount=math.floor(calculator_params.amount),
        total_interests=math.floor(total_interests),
        total_interests_percentage=total_interests_percentage,
        insurance=math.floor(insurance),
        fixed_digital_services=math.floor(fixed_digital_services),
        variable_digital_services=math.floor(variable_digital_services),
        disbursement=math.floor(disbursement),
        credit_validation=math.floor(credit_validation),
        promissory_note_and_custody=math.floor(promissory_note_and_custody),
        endorsement=math.floor(endorsement),
        collection=math.floor(collection),
        vat=math.floor(vat),
        administration=math.floor(administration),
        total_digital_services=math.floor(total_digital_services),
        total=math.floor(total),
        payment_amount=math.floor(payment_amount)
    )


def get_loan_calculation_formatted(loans_configuration: LoansConfigurationInDb, calculator_params: CalculatorInCreate) -> CalculatorDetailsInResponse:
    loan_calculation = calculate_loan(loans_configuration, calculator_params)
    return CalculatorDetailsInResponse(
        loan_amount="${:,.2f}".format(float(loan_calculation.loan_amount)),
        total_interests="${:,.2f}".format(float(loan_calculation.total_interests)),
        total_interests_percentage="{:,.2f}%".format(float(loan_calculation.total_interests)),
        insurance="${:,.2f}".format(float(loan_calculation.insurance)),
        fixed_digital_services="${:,.2f}".format(float(loan_calculation.fixed_digital_services)),
        variable_digital_services="${:,.2f}".format(float(loan_calculation.variable_digital_services)),
        disbursement="${:,.2f}".format(float(loan_calculation.disbursement)),
        credit_validation="${:,.2f}".format(float(loan_calculation.credit_validation)),
        promissory_note_and_custody="${:,.2f}".format(float(loan_calculation.promissory_note_and_custody)),
        endorsement="${:,.2f}".format(float(loan_calculation.endorsement)),
        collection="${:,.2f}".format(float(loan_calculation.collection)),
        vat="${:,.2f}".format(float(loan_calculation.vat)),
        administration="${:,.2f}".format(float(loan_calculation.administration)),
        total_digital_services="${:,.2f}".format(float(loan_calculation.total_digital_services)),
        total="${:,.2f}".format(float(loan_calculation.total)),
        payment_amount="${:,.2f}".format(float(loan_calculation.payment_amount))
    )
