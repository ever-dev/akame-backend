import json
from loguru import logger
from aioredis import Redis
from databases import Database

from mamut.crud.loans_configuration import select_loans_configuration, insert_loans_configuration
from mamut.models.loans_configuration import LoansConfigurationInDb, LoansConfigurationInSelect, LoansConfigurationInInsert
from mamut.core.utils import create_aliased_response
from mamut.models.suppliers import SuppliersInSelect
from mamut.crud.suppliers import get_suppliers_db

def get_tem(tea: float):
    return ((1 + tea) ** (1 / 12)) - 1


def get_ted(tea: float):
    return ((1 + tea) ** (1 / 360)) - 1


class LoanConfigurationHandler:
    def __init__(self, pg_conn: Database, redis_conn: Redis):
        self.pg_conn: Database = pg_conn
        self.redis_conn: Redis = redis_conn

    async def save_loan_configuration(self, new_loan_config: LoansConfigurationInInsert) -> LoansConfigurationInDb:
        loan_in_db = None
        loans_configuration_id = await insert_loans_configuration(self.pg_conn, new_loan_config)
        if loans_configuration_id is not None:
            loan_in_db = LoansConfigurationInDb(**new_loan_config.dict(), loans_configuration_id=loans_configuration_id)
            await self.redis_conn.set('active_loan_config_' + str(new_loan_config.supplier_id), json.dumps(create_aliased_response(loan_in_db)))
        return loan_in_db

    async def get_active_loan_configuration(self, supplier_id: int = None, loans_configuration_id: int = None) -> LoansConfigurationInDb:
        if supplier_id is None:
            loans_configurations = None
            if loans_configuration_id is not None:
                loans_configurations = await select_loans_configuration(self.pg_conn, LoansConfigurationInSelect(
                    loans_configuration_id=loans_configuration_id
                ))

            if loans_configurations is None or len(loans_configurations) == 0:
                raise ValueError('Supplier id is None and cannot retrieve loans configurations.')

            supplier_id = loans_configurations[0].supplier_id

        active_loan_config = None
        active_loan_config_from_redis = await self.redis_conn.get('active_loan_config_' + str(supplier_id))

        if active_loan_config_from_redis is not None:
            active_loan_config = LoansConfigurationInDb(**json.loads(active_loan_config_from_redis))
            active_loan_config.term_configuration = json.loads(active_loan_config.term_configuration)

        if active_loan_config is None:
            active_loan_configs = await select_loans_configuration(self.pg_conn,
                                                                   LoansConfigurationInSelect(supplier_id=supplier_id, is_active=True))
            if len(active_loan_configs) > 0:
                active_loan_config = active_loan_configs[0]

        return active_loan_config

    async def get_default_supplier_id(self, supplier_id: int) -> int:
        if supplier_id is None: # Get the default supplier_id
            suppliers = await get_suppliers_db(self.pg_conn, SuppliersInSelect(name='Akame'))
            if suppliers is None or len(suppliers) == 0:
                raise ValueError('Default supplier not found, run database migrations.')
            supplier_id = suppliers[0].supplier_id

        return supplier_id

    async def get_supplier_by_loan_config_id(self, loan_config_id: int) -> int:
        suppliers = await get_suppliers_db(self.pg_conn, SuppliersInSelect(name='Akame'))
        if suppliers is None or len(suppliers) == 0:
            raise ValueError('Default supplier not found, run database migrations.')
        supplier_id = suppliers[0].supplier_id

        return supplier_id
