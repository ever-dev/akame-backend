from typing import List, Mapping
from databases import Database
from mamut.models.setting import SettingInSelect, SettingInInsert, SettingInUpdate, SettingInDb


async def select_setting(conn: Database, select: SettingInSelect) -> SettingInDb:
    setting: SettingInDb = None

    query = '''
    select setting.setting
            , setting.value
            , setting.description
            , setting.python_type
            , setting.last_modified_by
            , users.name as last_modified_by_name
            , setting.last_modified_date
        from app.tb_configuration as setting
        inner join financial_users.tb_users as users ON (setting.last_modified_by = users.user_id)
            where setting.setting = :setting :: TEXT
    '''

    record: Mapping = await conn.fetch_one(query=query, values=select.dict())

    if record is not None:
        setting = SettingInDb(**dict(record))

    return setting


async def insert_setting(conn: Database, insert: SettingInInsert) -> bool:
    query = '''
    insert into app.tb_configuration(
        setting
        , value
        , description
        , python_type
        , last_modified_by
        , last_modified_date
        )
        values(
            :setting :: TEXT
            , :value :: TEXT
            , :description :: TEXT
            , :python_type :: TEXT
            , COALESCE(:last_modified_by ::INT, (SELECT user_id FROM financial_users.tb_users WHERE name = 'Master'))
            , COALESCE(:last_modified_date ::TIMESTAMP, TIMEZONE('UTC', NOW()))
        )
    returning setting;
    '''
    insert_result = await conn.fetch_one(query=query, values=insert.dict())

    if insert_result is not None:
        return True

    return False


async def update_setting(conn: Database, update: SettingInUpdate):
    query = '''
    update app.tb_configuration set
        value = COALESCE(:value :: VARCHAR, value)
        , description = COALESCE(:description :: VARCHAR, description)
        , python_type = COALESCE(:python_type :: VARCHAR, python_type)
        , last_modified_by = COALESCE(:last_modified_by :: INTEGER, (SELECT user_id FROM financial_users.tb_users WHERE name = 'Master'))
        , last_modified_date = COALESCE(:last_modified_date :: TIMESTAMP, TIMEZONE('UTC', NOW()))
    where setting = :setting :: TEXT;
    '''
    await conn.execute(query=query, values=update.dict())
