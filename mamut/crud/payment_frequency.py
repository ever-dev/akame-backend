from typing import List, Mapping
from databases import Database

from mamut.models.payment_frequency import PaymentFrequency, PaymentFrequencyInSelect


async def get_payment_frequency_db(conn: Database, select: PaymentFrequencyInSelect) -> List[PaymentFrequency]:

    list_result: List[PaymentFrequency] = []

    query = '''
    select  payment_frequency_id
            ,name
            ,is_active
            ,is_deleted
            ,last_modified_by
            ,last_modified_date
    from    financial_clients.tb_payment_frequency as list
    where   (:payment_frequency_id :: INT IS NULL OR list.payment_frequency_id = :payment_frequency_id :: INT);
    '''

    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        list_result.append(PaymentFrequency(**dict(record)))

    return list_result
