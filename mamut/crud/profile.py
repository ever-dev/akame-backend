import json
from aioredis import Redis
from typing import List, Mapping
from databases import Database
from mamut.conn.postgresql.postgresql_utils import get_pg_database
from mamut.conn.redis.redis_utils import get_redis_database
from mamut.models.profile import ProfileInSelect, ProfileInDb, ProfileInInsert, ProfileInUpdate


REDIS_PROFILE_PREFIX = 'mamutprofile_'


async def load_profiles_in_redis():
    conn = await get_pg_database()
    rconn = await get_redis_database()
    profiles: List[ProfileInDb] = await select_profiles(conn, ProfileInSelect(is_active=True))
    for profile in profiles:
        await rconn.set(REDIS_PROFILE_PREFIX + str(profile.profile_id), json.dumps(profile.permissions))


async def select_profiles(conn: Database, select: ProfileInSelect) -> List[ProfileInDb]:
    profiles: List[ProfileInDb] = []

    query = '''
    select profiles.profile_id
            , profiles.name
            , profiles.description
            , profiles.permissions
            , profiles.is_active
            , profiles.last_modified_date
        from financial_users.tb_user_permission_profiles as profiles
        where profiles.is_deleted = false
            and (:profile_id :: INT IS NULL OR profiles.profile_id = :profile_id :: INT)
            and (:name :: VARCHAR IS NULL OR profiles.name = :name :: VARCHAR)
    '''

    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        profile = ProfileInDb(**dict(record))
        profile.permissions = json.loads(profile.permissions)
        profiles.append(profile)

    return profiles


async def insert_profile(conn: Database, insert: ProfileInInsert) -> int:
    query = '''
    insert into financial_users.tb_user_permission_profiles(
        name
        , description
        , permissions
        , is_active
        , is_deleted
        , last_modified_date
        )
        values(
            :name :: VARCHAR
            , :description :: VARCHAR
            , :permissions :: JSON
            , :is_active ::BOOLEAN
            , :is_deleted :: BOOLEAN
            , COALESCE(:last_modified_date ::TIMESTAMP, CURRENT_TIMESTAMP)
        )
    returning profile_id;
    '''
    insert.permissions = json.dumps(insert.permissions)
    insert_result = await conn.fetch_one(query=query, values=insert.dict())

    if insert_result is not None:
        return list(insert_result.values())[0]

    return None


async def update_profile(conn: Database, update: ProfileInUpdate):
    query = '''
    update financial_users.tb_user_permission_profiles set
        name = COALESCE(:name :: VARCHAR, name)
        , description = COALESCE(:description :: VARCHAR, description)
        , permissions = COALESCE(:permissions :: JSON, permissions)
        , is_active = COALESCE(:is_active :: BOOLEAN, is_active)
        , is_deleted = COALESCE(:is_deleted :: BOOLEAN, is_deleted)
        , last_modified_date = COALESCE(:last_modified_date :: TIMESTAMP, CURRENT_TIMESTAMP)
    where is_deleted = false and profile_id = :profile_id :: INTEGER;
    '''
    if update.permissions is not None:
        update.permissions = json.dumps(update.permissions)

    await conn.execute(query=query, values=update.dict())
    
    # Update redis profiles
    rconn = await get_redis_database() # Get redis connection
    if update.is_deleted: # If the profile is deleted, delete in redis
        await rconn.delete(REDIS_PROFILE_PREFIX + str(update.profile_id))
    
    if update.permissions is not None: # If the profile permissions are updated, update in redis
        await rconn.set(REDIS_PROFILE_PREFIX + str(update.profile_id), json.dumps(update.permissions))
