import json
from typing import List, Mapping
from databases import Database
from databases.backends.postgres import Record
from mamut.core.utils import create_aliased_response
from mamut.models.loans_configuration import LoansConfigurationInDb, LoansConfigurationInSelect, LoansConfigurationInInsert, LoansConfigurationInUpdate


async def select_loans_configuration(conn: Database, select: LoansConfigurationInSelect) -> List[LoansConfigurationInDb]:
    query = '''
    select 
        loans_configuration_id, tea_interest, tem_interest, ted_interest, insurance, fixed_insurance, digital_services_per_day, fixed_digital_services, disbursement, credit_validation, promissory_note_and_custody, endorsement, collection, vat, min_amount, max_amount, step_amount, min_days, max_days, term_configuration, is_active, is_deleted, last_modified_by, last_modified_date, tea_mora, tem_mora, ted_mora, supplier_id
    from
        FINANCIAL_LOANS.tb_LOANS_CONFIGURATION as lc 
    where
        lc.is_deleted = FALSE
        AND
        (:loans_configuration_id :: BIGINT IS NULL OR lc.loans_configuration_id = :loans_configuration_id :: BIGINT)
        AND
        (:is_active :: BOOLEAN IS NULL OR lc.is_active = :is_active :: BOOLEAN)
        AND
        (:supplier_id :: INT IS NULL OR lc.supplier_id = :supplier_id :: INT);
    '''
    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    if records is not None:
        select_result: List[LoansConfigurationInDb] = list()
        for record in records:
            loans_configuration = LoansConfigurationInDb(**dict(record))
            loans_configuration.term_configuration = json.loads(loans_configuration.term_configuration)
            select_result.append(loans_configuration)
        return select_result

    return None


async def insert_loans_configuration(conn: Database, insert: LoansConfigurationInInsert):
    insert.term_configuration = json.dumps(create_aliased_response(insert.term_configuration))
    query = '''
        INSERT INTO
        FINANCIAL_LOANS.tb_LOANS_CONFIGURATION
        (
            tea_interest,
            tem_interest,
            ted_interest,
            insurance,
            fixed_insurance,
            digital_services_per_day,
            fixed_digital_services,
            disbursement,
            credit_validation,
            promissory_note_and_custody,
            endorsement,
            collection,
            vat,
            min_amount,
            max_amount,
            step_amount,
            min_days,
            max_days,
            term_configuration,
            tea_mora,
            tem_mora,
            ted_mora,
            is_active,
            is_deleted,
            last_modified_by,
            last_modified_date,
            supplier_id
        )
    VALUES
        (
            :tea_interest :: DECIMAL,
            :tem_interest :: DECIMAL,
            :ted_interest :: DECIMAL, 
            :insurance :: DECIMAL,
            :fixed_insurance :: DECIMAL,
            :digital_services_per_day :: DECIMAL,
            :fixed_digital_services :: DECIMAL,
            :disbursement :: DECIMAL,
            :credit_validation :: DECIMAL,
            :promissory_note_and_custody :: DECIMAL,
            :endorsement :: DECIMAL,
            :collection :: DECIMAL,
            :vat :: DECIMAL,
            :min_amount :: DECIMAL,
            :max_amount :: DECIMAL,
            :step_amount :: DECIMAL,
            :min_days :: INTEGER,
            :max_days :: INTEGER,
            :term_configuration :: JSON,
            :tea_mora ::DECIMAL,
            :tem_mora ::DECIMAL,
            :ted_mora ::DECIMAL,
            :is_active ::BOOLEAN,
            :is_deleted :: BOOLEAN,
            :last_modified_by ::INT,
            COALESCE(:last_modified_date ::TIMESTAMP, CURRENT_TIMESTAMP),
            :supplier_id ::INT
        ) RETURNING loans_configuration_id;
    '''

    insert_result = await conn.fetch_one(query=query, values=insert.dict())

    if insert_result is not None:
        return list(insert_result.values())[0]

    return None


async def update_loans_configuration(conn: Database, update: LoansConfigurationInUpdate):
    query = '''
    update FINANCIAL_LOANS.tb_LOANS_CONFIGURATION
        set is_active = COALESCE(:is_active :: BOOLEAN, is_active)
    where 
        (:loans_configuration_id :: BIGINT IS NULL OR loans_configuration_id = :loans_configuration_id :: BIGINT)
        AND
        (:supplier_id :: INT IS NULL OR supplier_id = :supplier_id :: INT);
    '''

    await conn.execute(query=query, values=update.dict())
