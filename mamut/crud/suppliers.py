from typing import List, Mapping
from databases import Database


from mamut.models.suppliers import SuppliersByLoanConfigInSelect, Suppliers, SuppliersAccounts, SuppliersAccountsIdentification, SuppliersInSelect, SuppliersAccountsInSelect, SuppliersAccountsIdentificationInSelect, SuppliersInInsert, SuppliersAccountsInInsert, SuppliersAccountsIdentificationInInsert, SuppliersInUpdate, SuppliersAccountsInUpdate, SuppliersAccountsIdentificationInUpdate


async def get_suppliers_db(conn: Database, select: SuppliersInSelect) -> List[Suppliers]:
    suppliers: List[Suppliers] = []

    query = '''
    select  supplier_id
            ,name
            ,is_active
            ,is_deleted
            ,last_modified_by
            ,last_modified_date
    from    financial_loans.tb_suppliers as suppliers
    where 
    (suppliers.is_deleted = false )
    AND  
    (:supplier_id :: INT IS NULL OR suppliers.supplier_id = :supplier_id :: INT)
    AND
    (:name :: VARCHAR IS NULL OR suppliers.name = :name :: VARCHAR);
    '''

    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        suppliers.append(Suppliers(**dict(record)))

    return suppliers


async def insert_suppliers(conn: Database, insert: SuppliersInInsert) -> int:
    query = '''
    INSERT INTO financial_loans.tb_suppliers(
        name
        , is_active
        , is_deleted
        , last_modified_by
        , last_modified_date
        )
    VALUES(
        :name :: VARCHAR
        , :is_active :: BOOLEAN
        , :is_deleted :: BOOLEAN
        , :last_modified_by ::INT
        , COALESCE(:last_modified_date ::TIMESTAMP, CURRENT_TIMESTAMP)
    )
    returning supplier_id;
    '''

    insert_result = await conn.fetch_one(query=query, values=insert.dict())

    if insert_result is not None:
        return list(insert_result.values())[0]

    return None


async def update_suppliers(conn: Database, update: SuppliersInUpdate):
    query = '''
    UPDATE financial_loans.tb_suppliers SET
        name=COALESCE(:name :: VARCHAR, name)
        , is_active = COALESCE(:is_active :: BOOLEAN, is_active)
        , is_deleted = COALESCE(:is_deleted :: BOOLEAN, is_deleted)
        , last_modified_by = COALESCE(:last_modified_by :: INTEGER, last_modified_by)
        , last_modified_date = COALESCE(:last_modified_date :: TIMESTAMP, CURRENT_TIMESTAMP)
    WHERE supplier_id = :supplier_id :: INTEGER;
    '''

    await conn.execute(query=query, values=update.dict())


async def get_suppliers_accounts_db(conn: Database, select: SuppliersAccountsInSelect) -> List[SuppliersAccounts]:

    suppliers_accounts: List[SuppliersAccounts] = []

    query = '''
    select  supplier_account_id
            ,account_id
            ,supplier_id
            ,is_active
            ,is_deleted
            ,last_modified_by
            ,last_modified_date
            ,bank_city_id
            ,bank_identity_id
            ,bank_account_type_id
            ,owner
    from financial_loans.tb_suppliers_accounts suppliersAccounts
    where   
    (suppliersAccounts.is_deleted = false )
    AND      
    (:supplier_account_id :: INT IS NULL OR suppliersAccounts.supplier_account_id = :supplier_account_id :: INT)
    AND
    (:account_id :: VARCHAR IS NULL OR suppliersAccounts.account_id = :account_id :: VARCHAR)
    AND    
    (:supplier_id :: INT IS NULL OR suppliersAccounts.supplier_id = :supplier_id :: INT);
    '''


    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        suppliers_accounts.append(SuppliersAccounts(**dict(record)))

    return suppliers_accounts

async def insert_suppliers_accounts(conn: Database, insert: SuppliersAccountsInInsert) -> int:
    query = '''
    INSERT INTO financial_loans.tb_suppliers_accounts(
        account_id
        , supplier_id
        , is_active
        , is_deleted
        , last_modified_by
        , last_modified_date
        , bank_city_id
        , bank_identity_id
        , bank_account_type_id
        , owner        
        )
    VALUES(
        :account_id :: VARCHAR
        , :supplier_id :: INT
        , :is_active :: BOOLEAN
        , :is_deleted :: BOOLEAN
        , :last_modified_by ::INT
        , COALESCE(:last_modified_date ::TIMESTAMP, CURRENT_TIMESTAMP)
        , :bank_city_id :: INT
        , :bank_identity_id :: INT
        , :bank_account_type_id :: INT
        , :owner :: VARCHAR
    )
    returning supplier_account_id;
    '''

    insert_result = await conn.fetch_one(query=query, values=insert.dict())

    if insert_result is not None:
        return list(insert_result.values())[0]

    return None

async def update_suppliers_accounts(conn: Database, update: SuppliersAccountsInUpdate): 
    query = '''
    UPDATE financial_loans.tb_suppliers_accounts SET
        account_id=COALESCE(:account_id :: VARCHAR, account_id)
        , supplier_id=COALESCE(:supplier_id :: INTEGER, supplier_id)
        , is_active = COALESCE(:is_active :: BOOLEAN, is_active)
        , is_deleted = COALESCE(:is_deleted :: BOOLEAN, is_deleted)
        , last_modified_by = COALESCE(:last_modified_by :: INTEGER, last_modified_by)
        , last_modified_date = COALESCE(:last_modified_date :: TIMESTAMP, CURRENT_TIMESTAMP)
        , bank_city_id = COALESCE(:bank_city_id :: INTEGER, bank_city_id)
        , bank_identity_id = COALESCE(:bank_identity_id :: INTEGER, bank_identity_id)
        , bank_account_type_id = COALESCE(:bank_account_type_id :: INTEGER, bank_account_type_id)
        , owner = COALESCE(:owner :: VARCHAR, owner)

    WHERE supplier_account_id= :supplier_account_id :: INTEGER;
    '''

    await conn.execute(query=query, values=update.dict())

async def get_suppliers_accounts_identifications_db(conn: Database, select: SuppliersAccountsIdentificationInSelect) -> List[SuppliersAccountsIdentification]:

    suppliers_accounts_identification: List[SuppliersAccountsIdentification] = []

    query = '''
    select  supplier_account_identification_id
            ,identification
            ,identification_type_id
            ,supplier_id
            ,is_active
            ,is_deleted
            ,last_modified_by
            ,last_modified_date
    from financial_loans.tb_suppliers_accounts_identifications suppliersAccountsIdentifications
    where
    (suppliersAccountsIdentifications.is_deleted = false )
    AND      
    (:supplier_account_identification_id :: INT IS NULL OR suppliersAccountsIdentifications.supplier_account_identification_id = :supplier_account_identification_id :: INT)
    AND
    (:identification :: VARCHAR IS NULL OR suppliersAccountsIdentifications.identification = :identification :: VARCHAR)    
    AND
    (:identification_type_id :: INT IS NULL OR suppliersAccountsIdentifications.identification_type_id = :identification_type_id :: INT)
    AND    
    (:supplier_id :: INT IS NULL OR suppliersAccountsIdentifications.supplier_id = :supplier_id :: INT);
    '''
    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        suppliers_accounts_identification.append(SuppliersAccountsIdentification(**dict(record)))

    return suppliers_accounts_identification

async def insert_suppliers_accounts_identifications(conn: Database, insert: SuppliersAccountsIdentificationInInsert) -> int:
    query = '''
    INSERT INTO financial_loans.tb_suppliers_accounts_identifications(
        identification
        , supplier_id
        , identification_type_id
        , is_active
        , is_deleted
        , last_modified_by
        , last_modified_date
        )
    VALUES(
        :identification :: VARCHAR
        , :supplier_id :: INT
        , :identification_type_id :: INT
        , :is_active :: BOOLEAN
        , :is_deleted :: BOOLEAN
        , :last_modified_by ::INT
        , COALESCE(:last_modified_date ::TIMESTAMP, CURRENT_TIMESTAMP)
    )
    returning supplier_account_identification_id;
    '''

    insert_result = await conn.fetch_one(query=query, values=insert.dict())

    if insert_result is not None:
        return list(insert_result.values())[0]

    return None

async def update_suppliers_accounts_identifications(conn: Database, update: SuppliersAccountsIdentificationInUpdate):
    query = '''
    UPDATE financial_loans.tb_suppliers_accounts_identifications SET
        identification=COALESCE(:identification :: VARCHAR, identification)
        , identification_type_id=COALESCE(:identification_type_id :: INTEGER, identification_type_id)
        , supplier_id=COALESCE(:supplier_id :: INTEGER, supplier_id)
        , is_active = COALESCE(:is_active :: BOOLEAN, is_active)
        , is_deleted = COALESCE(:is_deleted :: BOOLEAN, is_deleted)
        , last_modified_by = COALESCE(:last_modified_by :: INTEGER, last_modified_by)
        , last_modified_date = COALESCE(:last_modified_date :: TIMESTAMP, CURRENT_TIMESTAMP)
    WHERE supplier_account_identification_id= :supplier_account_identification_id :: INTEGER;
    '''

    await conn.execute(query=query, values=update.dict())


async def get_supplier_by_loan_config_db(conn: Database, select: SuppliersByLoanConfigInSelect) -> List[Suppliers]:

    suppliers: List[Suppliers] = []

    query = '''
    select  suppliers.supplier_id
            ,suppliers.name
            ,suppliers.is_active
            ,suppliers.is_deleted
            ,suppliers.last_modified_by
            ,suppliers.last_modified_date
            ,config.loans_configuration_id 
    from    financial_loans.tb_suppliers as suppliers
    inner join financial_loans.tb_loans_configuration as config on config.supplier_id = suppliers.supplier_id
    where
    (config.loans_configuration_id = :loan_config_id :: INT)
    '''

    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        suppliers.append(Suppliers(**dict(record)))

    return suppliers
