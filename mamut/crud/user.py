import json
from typing import List, Mapping
from databases import Database

from mamut.models.user import UserInSelect, UserInDb, UserInInsert, UserInUpdate, UserTokenInSelect


async def validate_credentials(conn: Database, select: UserTokenInSelect) -> UserInDb:
    user: UserInDb = None

    query = '''
    select users.user_id
            , users.profile_id
            , profiles.name profile_name
            , users.name
            , profiles.permissions
            , users.consecutive_failed_login_attempts
            , users.is_active
            , users.last_modified_date
            , users.email
        from financial_users.tb_users as users
        inner join financial_users.tb_user_permission_profiles as profiles ON (users.profile_id = profiles.profile_id)
        where users.is_active = true
            and users.is_deleted = false
            and users.is_confirmed = true
            and profiles.is_active = true
            and profiles.is_deleted = false
            and LOWER(users.name) = LOWER(:name :: VARCHAR)
            and users.password = public.crypt(:password :: TEXT, users.password)
    '''

    record: Mapping = await conn.fetch_one(query=query, values=select.dict())

    if record is not None:
        user = UserInDb(**dict(record))
        user.permissions = json.loads(user.permissions)

    return user


async def select_users(conn: Database, select: UserInSelect) -> List[UserInDb]:
    users: List[UserInDb] = []
    query = '''
    select users.user_id
            , profiles.profile_id
            , profiles.name profile_name
            , users.name
            , users.consecutive_failed_login_attempts
            , users.is_active
            , users.last_modified_by
            , users2.name as last_modified_by_name
            , users.last_modified_date
            , users.email
            , users.is_confirmed
        from financial_users.tb_users as users
        inner join financial_users.tb_users as users2 ON (users.user_id = users2.user_id)
        inner join financial_users.tb_user_permission_profiles as profiles ON (profiles.profile_id = users.profile_id)
        where users.is_deleted = false and users.is_active = true
            and (:user_id :: INT IS NULL OR users.user_id = :user_id :: INT)
            and (:profile_id :: INT IS NULL OR users.profile_id = :profile_id :: INT)
            and (:name :: VARCHAR IS NULL OR LOWER(users.name) = LOWER(:name :: VARCHAR))
            and (:email :: VARCHAR IS NULL OR users.email = :email :: VARCHAR)
    '''

    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        users.append(UserInDb(**dict(record)))

    return users


async def select_users_for_signup(conn: Database, select: UserInSelect) -> List[UserInDb]:
    users: List[UserInDb] = []

    query = '''
    select users.user_id
            , profiles.profile_id
            , profiles.name profile_name
            , users.name
            , users.consecutive_failed_login_attempts
            , users.is_active
            , users.last_modified_by
            , users2.name as last_modified_by_name
            , users.last_modified_date
            , users.email
            , users.is_confirmed
        from financial_users.tb_users as users
        inner join financial_users.tb_users as users2 ON (users.user_id = users2.user_id)
        inner join financial_users.tb_user_permission_profiles as profiles ON (profiles.profile_id = users.profile_id)
        where users.is_deleted = false
            and (:user_id :: INT IS NULL OR users.user_id = :user_id :: INT)
            and (:profile_id :: INT IS NULL OR users.profile_id = :profile_id :: INT)
            and ((:name :: VARCHAR IS NULL OR LOWER(users.name) = LOWER(:name :: VARCHAR))
            or (:email :: VARCHAR IS NULL OR users.email = :email :: VARCHAR))
    '''

    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        users.append(UserInDb(**dict(record)))

    return users


async def insert_user(conn: Database, insert: UserInInsert) -> int:
    query = '''
    insert into financial_users.tb_users(
        profile_id
        , name
        , password
        , consecutive_failed_login_attempts
        , is_active
        , is_deleted
        , last_modified_by
        , last_modified_date
        , email
        , is_confirmed
        )
        values(
            :profile_id :: INTEGER
            , :name :: VARCHAR
            , public.crypt(:password :: TEXT, public.gen_salt('bf'))
            , :consecutive_failed_login_attempts :: INTEGER
            , :is_active :: BOOLEAN
            , :is_deleted :: BOOLEAN
            , :last_modified_by ::INT
            , COALESCE(:last_modified_date ::TIMESTAMP, CURRENT_TIMESTAMP)
            , :email :: VARCHAR
            , :is_confirmed :: BOOLEAN
        )
    returning user_id;
    '''

    insert_result = await conn.fetch_one(query=query, values=insert.dict())

    if insert_result is not None:
        return list(insert_result.values())[0]

    return None


async def update_user(conn: Database, update: UserInUpdate):
    query = '''
    update financial_users.tb_users set
        profile_id = COALESCE(:profile_id :: INTEGER, profile_id)
        , name = COALESCE(:name :: VARCHAR, name)
        , password = COALESCE(public.crypt(:password :: TEXT, public.gen_salt('bf')), password)
        , consecutive_failed_login_attempts = COALESCE(:consecutive_failed_login_attempts :: INTEGER, consecutive_failed_login_attempts)
        , is_active = COALESCE(:is_active :: BOOLEAN, is_active)
        , is_deleted = COALESCE(:is_deleted :: BOOLEAN, is_deleted)
        , last_modified_by = COALESCE(:last_modified_by :: INTEGER, last_modified_by)
        , last_modified_date = COALESCE(:last_modified_date :: TIMESTAMP, CURRENT_TIMESTAMP)
        , email = COALESCE(:email :: VARCHAR, email)
        , is_confirmed = COALESCE(:is_confirmed :: BOOLEAN, is_confirmed)
    where is_deleted = false and user_id = :user_id :: INTEGER;
    '''

    await conn.execute(query=query, values=update.dict())
