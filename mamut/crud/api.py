import json
from typing import List, Mapping

from databases import Database
from mamut.models.api import ApiTransactionLogInInsert, ApiTransactionLogInSelect, ApiTransactionLogInDb


async def insert_api_transaction_log(conn: Database, insert: ApiTransactionLogInInsert):
    query = '''
    insert into API.tb_api_transaction_logs
    (
        api_id
        , api_log
        , user_id
        , create_date
    )
    values
    (
        :api_id :: INTEGER
        , :api_log :: JSON
        , :user_id :: INTEGER
        , CURRENT_TIMESTAMP
    )
    returning log_id;
    '''

    insert.api_log = json.dumps(insert.api_log)

    result = await conn.fetch_one(query=query, values=insert.dict())

    if result is not None:
        return list(result.values())[0]

    return None


async def select_api_transaction_log(conn: Database, select: ApiTransactionLogInSelect, log_type: str) -> List[ApiTransactionLogInDb]:
    api_logs: List[ApiTransactionLogInDb] = []

    query = '''
    select
        log_id
        , api_id
        , api_log
        , user_id
        , create_date
    from
        API.tb_api_transaction_logs
    where
        api_id = :api_id :: INTEGER
        and user_id = :user_id :: INTEGER
        and api_log ->> 'logType' = '{log_type}'	
    order by log_id desc
    fetch first 1 rows only;
    '''.format(log_type=log_type)

    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        json_data = ApiTransactionLogInDb(**dict(record))
        json_data.api_log = json.loads(json_data.api_log)
        api_logs.append(json_data)

    return api_logs
