from typing import List, Mapping
from databases import Database
from mamut.models.persons import PersonsInSelect, PersonsInUpdate, PersonsInResponse


async def select_persons(conn: Database, select: PersonsInSelect) -> List[PersonsInResponse]:

    persons: List[PersonsInResponse] = []

    query = '''
    SELECT
        persons.persons_id,
        persons.identification,
        persons.name,
        persons.middle_name,
        persons.first_name,
        persons.last_name,
        persons.date_of_birth,
        persons.is_fraudulent,
        users.user_id,
        users.is_active
    FROM
        financial_clients.tb_persons as persons
    INNER JOIN
        financial_users.tb_users as users
    ON (persons.user_id = users.user_id)
    WHERE
        (:persons_id :: BIGINT IS NULL OR persons.persons_id = :persons_id :: BIGINT)
        AND
        (:identification :: VARCHAR IS NULL OR persons.identification LIKE '%' || :identification :: VARCHAR || '%')
        AND
        (:persons_id_greater_than :: BIGINT IS NULL OR persons.persons_id > :persons_id_greater_than :: BIGINT)
    ORDER BY persons.persons_id DESC
    LIMIT :rows_limit :: BIGINT;
    '''

    records: List[Mapping] = await conn.fetch_all(query=query, values=select.dict())

    for record in records:
        persons.append(PersonsInResponse(**dict(record)))

    return persons


async def update_person(conn: Database, update: PersonsInUpdate):
    query = '''
    update financial_clients.tb_persons set
        is_fraudulent = COALESCE(:is_fraudulent :: BOOLEAN, is_fraudulent)
        , last_modified_by = COALESCE(:modifier_user_id :: INTEGER, (SELECT user_id FROM financial_users.tb_users WHERE name = 'Master'))
        , last_modified_date = COALESCE(:last_modified_date :: TIMESTAMP, TIMEZONE('UTC', NOW()))
    where persons_id = :persons_id :: BIGINT;
    '''
    await conn.execute(query=query, values=update.dict())
