from typing import Optional
from enum import Enum
from mamut.models.rwmodel import RWModel
from datetime import datetime


class SettingPythonTypesEnum(str, Enum):
    boolean = 'bool'
    string = 'str'
    integer = 'int'


class SettingInSelect(RWModel):
    setting: str


class SettingInInsert(RWModel):
    setting: str
    value: str
    description: str
    python_type: SettingPythonTypesEnum
    last_modified_by: Optional[int] = None
    last_modified_date: Optional[datetime] = None


class SettingInUpdate(RWModel):
    setting: str
    value: Optional[str] = None
    description: Optional[str] = None
    python_type: Optional[SettingPythonTypesEnum] = None
    last_modified_by: Optional[int] = None


class SettingInDb(SettingInInsert):
    last_modified_by_name: str
