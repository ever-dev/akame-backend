from typing import Optional, Union, Any
from mamut.models.rwmodel import RWModel


class ApiInUpdate(RWModel):
    api_id: int
    api_info: Union[str, Any] = None


class ApiTransactionLogInDb(RWModel):
    log_id: int
    api_log: Union[str, Any] = None


class ApiTransactionLogInInsert(RWModel):
    api_id: int
    user_id: int
    api_log: Union[str, Any] = None


class ApiTransactionLogInSelect(RWModel):
    api_id: int
    user_id: int
