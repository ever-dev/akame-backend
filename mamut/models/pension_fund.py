from datetime import date
from typing import Optional
from mamut.models.rwmodel import RWModel


class PensionFundInResponse(RWModel):
    pension_fund_id: int
    name: str
    is_active: bool


class PensionFund(RWModel):
    pension_fund_id: int
    name: str
    is_active: bool
    is_deleted: Optional[bool]
    last_modified_by: Optional[int]
    last_modified_date: Optional[date]


class PensionFundInSelect(RWModel):
    pension_fund_id: Optional[int] = None
