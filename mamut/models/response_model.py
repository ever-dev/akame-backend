from pydantic.generics import GenericModel
from typing import Optional, Any, TypeVar, Generic, Union
from mamut.models.rwmodel import RWModel
from mamut.models.response_code import ResponseCode


T = TypeVar('T')


class ResponseModel(GenericModel, Generic[T]):
    data: Optional[T] = None
    details: ResponseCode
