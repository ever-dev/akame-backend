from typing import Optional, Union, Any
from mamut.models.rwmodel import RWModel
from datetime import datetime
from mamut.models.calculator import CalculatorInCreate


class LoanAttemptInfoInInsert(RWModel):
    calculator_config: CalculatorInCreate
    start_date: datetime
    end_date: datetime
    loans_configuration_id: int


class LoanAttemptInInsert(RWModel):
    info: Union[str, LoanAttemptInfoInInsert]
    owner_user_id: int
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[int]
    last_modified_date: Optional[datetime] = None


class LoanAttemptInDb(LoanAttemptInInsert):
    last_modified_by_name: Optional[str]
    loan_attempt_id: Optional[int]


class LoanAttemptInSelect(RWModel):
    loan_attempt_id: Optional[int] = None
    owner_user_id: Optional[int] = None

class CustomerLoanInSelect(RWModel):
    loan_attempt_id: Optional[int] = None
    owner_user_id: Optional[int] = None