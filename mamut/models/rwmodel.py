from datetime import datetime, timezone
from pydantic import BaseConfig, BaseModel


def to_lower_camel_case(string: str):
    strings = string.split('_')
    return strings[0].lower() + ''.join(word.capitalize() for word in strings[1:])


class RWModel(BaseModel):
    class Config(BaseConfig):
        arbitrary_types_allowed = True
        allow_population_by_field_name = True
        alias_generator = to_lower_camel_case
        json_encoders = {
            datetime: lambda dt: dt.replace(tzinfo=timezone.utc)
            .isoformat()
            .replace("+00:00", "Z")
        }
        fields = {
            'from_': 'from',
            'type_': 'type'
        }
