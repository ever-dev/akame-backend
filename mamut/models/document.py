from typing import Optional, Union, Any
from mamut.models.rwmodel import RWModel
from datetime import datetime


class DocumentInInsert(RWModel):
    name: str
    content: Union[str, Any] = None
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[int]
    last_modified_date: Optional[datetime] = None


class DocumentInDb(DocumentInInsert):
    last_modified_by_name: Optional[str]
    document_id: Optional[int]


class DocumentInCreate(RWModel):
    name: str
    content: Union[str, Any] = None


class DocumentInSelect(RWModel):
    document_id: Optional[int] = None
    name: Optional[str] = None


class DocumentInUpdate(RWModel):
    document_id: int
    name: Optional[str] = None
    content: Union[str, Any] = None
    is_active: Optional[bool] = None
    is_deleted: Optional[bool] = None
    last_modified_by: Optional[int] = None
    last_modified_date: Optional[datetime] = None
