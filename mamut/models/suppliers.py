from datetime import date
from typing import Optional
from mamut.models.rwmodel import RWModel
from datetime import datetime

class Suppliers(RWModel):
    supplier_id: int
    name: str
    is_active: bool
    is_deleted: Optional[bool]
    last_modified_by: Optional[int]
    last_modified_date: Optional[date]
    receives_money: Optional[bool]

class SuppliersInSelect(RWModel):
    supplier_id: Optional[int] = None
    name: Optional[str] = None

class SuppliersInInsert(RWModel):
    name: str
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: int    
    last_modified_date: Optional[datetime] = None
    receives_money: Optional[bool] = False

class SuppliersInCreate(RWModel):
    name: str
    receives_money: Optional[bool] = False

class SuppliersInUpdate(RWModel):
    supplier_id: int    
    name: Optional[str] = None
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[int] = None
    last_modified_date: Optional[datetime] = None
    receives_money: Optional[bool] = False

class SuppliersInDb(SuppliersInInsert):
    pass

class SuppliersAccounts(RWModel):
    supplier_account_id: Optional[int] = None
    account_id: str
    supplier_id: int
    is_active: bool
    is_deleted: Optional[bool]
    last_modified_by: Optional[int]
    last_modified_date: Optional[date]
    bank_city_id: int
    bank_identity_id: int
    bank_account_type_id: int
    owner: str
    loan_config_id: Optional[int] = None

class SuppliersAccountsInSelect(RWModel):
    supplier_account_id: Optional[int] = None
    account_id: Optional[str] = None
    supplier_id: Optional[int] = None

class SuppliersAccountsInInsert(RWModel):
    account_id: str
    supplier_id: int
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: int    
    last_modified_date: Optional[datetime] = None
    bank_city_id: int
    bank_identity_id: int
    bank_account_type_id: int
    owner: str    

class SuppliersAccountsInCreate(RWModel):
    account_id: str
    supplier_id: int
    bank_city_id: int
    bank_identity_id: int
    bank_account_type_id: int
    owner: str

class SuppliersAccountsInUpdate(RWModel):
    supplier_account_id: int
    account_id: Optional[str] = None
    supplier_id: Optional[int] = None
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[int] = None
    last_modified_date: Optional[datetime] = None
    bank_city_id: Optional[int] = None
    bank_identity_id: Optional[int] = None
    bank_account_type_id: Optional[int] = None
    owner: Optional[str] = None  

class SuppliersAccountsInDb(SuppliersAccountsInInsert):
    pass

class SuppliersAccountsIdentification(RWModel):
    supplier_account_identification_id : Optional[int] = None
    identification : Optional[str] = None
    identification_type_id : Optional[int] = None
    supplier_id : Optional[int] = None
    is_active: bool
    is_deleted: Optional[bool]
    last_modified_by: Optional[int]
    last_modified_date: Optional[date]

class SuppliersAccountsIdentificationInSelect(RWModel):
    supplier_account_identification_id : Optional[int] = None
    identification: Optional[str] = None
    identification_type_id : Optional[int] = None
    supplier_id : Optional[int] = None

class SuppliersAccountsIdentificationInInsert(RWModel):
    identification : str
    identification_type_id : int
    supplier_id : int
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: int    
    last_modified_date: Optional[datetime] = None

class SuppliersAccountsIdentificationInCreate(RWModel):
    identification : str
    identification_type_id : int
    supplier_id : int

class SuppliersAccountsIdentificationInUpdate(RWModel):
    supplier_account_identification_id : int
    identification : Optional[str] = None
    identification_type_id : Optional[int] = None
    supplier_id : Optional[int] = None
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[int] = None
    last_modified_date: Optional[datetime] = None

class SuppliersAccountsIdentificationInDb(SuppliersAccountsIdentificationInInsert):
    pass

class SuppliersByLoanConfigInSelect(RWModel):
    loan_config_id: Optional[int] = None

