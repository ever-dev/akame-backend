from typing import Optional
from mamut.models.rwmodel import RWModel


class TransportTypeInResponse(RWModel):
    transport_type_id: int
    name: str
    is_active: bool


class TransportType(RWModel):
    transport_type_id: int
    name: str
    is_active: bool


class TransportTypeInSelect(RWModel):
    transport_type_id: Optional[int] = None
