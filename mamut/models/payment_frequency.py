from datetime import date
from typing import Optional
from mamut.models.rwmodel import RWModel


class PaymentFrequencyInResponse(RWModel):
    payment_frequency_id: int
    name: str
    is_active: bool


class PaymentFrequency(RWModel):
    payment_frequency_id: int
    name: str
    is_active: bool
    is_deleted: Optional[bool]
    last_modified_by: Optional[int]
    last_modified_date: Optional[date]


class PaymentFrequencyInSelect(RWModel):
    payment_frequency_id: Optional[int] = None
