from typing import Optional, Union, List
from mamut.models.rwmodel import RWModel
from mamut.models.loans_configuration import LoansConfigurationTerm


class CalculatorInReponse(RWModel):
    total: float


class CalculatorDetailsInResponse(RWModel):
    loan_amount: Union[str, float]
    total_interests: Union[str, float]
    total_interests_percentage: Union[str, float]
    insurance: Union[str, float]
    variable_digital_services: Union[str, float]
    fixed_digital_services: Union[str, float]
    disbursement: Union[str, float]
    credit_validation: Union[str, float]
    promissory_note_and_custody: Union[str, float]
    endorsement: Union[str, float]
    collection: Union[str, float]
    vat: Union[str, float]
    administration: Union[str, float]
    total_digital_services: Union[str, float]
    total: Union[str, float]
    payment_amount: Union[str, float]


class CalculatorInCreate(RWModel):
    amount: float
    payments_quantity: int
    term_in_days: int


class CalculatorConfigurationInResponse(RWModel):
    min_amount: float
    max_amount: float
    step_amount: float
    min_days: int
    max_days: int
    term_configuration: List[LoansConfigurationTerm]
