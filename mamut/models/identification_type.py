from datetime import date
from typing import Optional
from mamut.models.rwmodel import RWModel


class IdentificationTypeInResponse(RWModel):
    identification_type_id: int
    name: str
    is_active: bool


class IdentificationType(RWModel):
    identification_type_id: int
    name: str
    is_active: bool
    # is_deleted: Optional[bool]
    # last_modified_by: Optional[int]
    # last_modified_date: Optional[date]


class IdentificationTypeInSelect(RWModel):
    identification_type_id: Optional[int] = None
