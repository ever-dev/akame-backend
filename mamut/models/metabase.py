from mamut.models.rwmodel import RWModel


class MetabaseEmbeddableDashboardInResponse(RWModel):
    id: int
    name: str


class MetabaseEmbeddableDashboardUrlInResponse(RWModel):
    url: str
