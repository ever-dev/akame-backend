from typing import Optional, Union, Any
from mamut.models.rwmodel import RWModel
from datetime import datetime


class LoanPaymentPlanInInsert(RWModel):
    financial_loans_id: int
    start_date: datetime
    end_date: datetime
    payment_status_id: int
    fee_amount: float
    interest: float
    capital: float
    loan_balance: float
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[int]
    last_modified_date: Optional[datetime] = None
    mora: float
    fee_balance: float
    alegra_id: Optional[str] = None


class LoanPaymentPlanInDb(LoanPaymentPlanInInsert):
    paid_in_alegra: bool
    alegra_info: Union[str, dict]
    last_modified_by_name: Optional[str]
    loans_payment_plan_id: Optional[int]


class LoanPaymentPlanInSelect(RWModel):
    financial_loans_id: Optional[int] = None
    loans_payment_plan_id: Optional[int] = None
    payment_status_id: Optional[int] = None
    not_payment_status_id: Optional[int] = None
    persons_id: Optional[int] = None
    older_than_end_date: Optional[datetime] = None
    for_mora_update: Optional[bool] = None


class LoanPaymentPlanInUpdate(RWModel):
    loans_payment_plan_id: int
    payment_status_id: Optional[int] = None
    fee_amount: Optional[float] = None
    interest: Optional[float] = None
    capital: Optional[float] = None
    loan_balance: Optional[float] = None
    mora: Optional[float] = None
    fee_balance: Optional[float] = None
    is_active: Optional[bool] = None
    is_deleted: Optional[bool] = None
    last_modified_by: Optional[int] = None
    last_modified_date: Optional[datetime] = None
    alegra_id: Optional[str] = None
    alegra_info: Optional[Union[str, dict]] = None
    fmsf_code: Optional[str] = None


class LoanPaymentPlanInDelete(RWModel):
    loans_payment_plan_id: int
