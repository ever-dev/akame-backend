from typing import Optional
from mamut.models.rwmodel import RWModel


class EconomicIncomeTypeInResponse(RWModel):
    economic_income_type_id: int
    name: str
    is_active: bool


class EconomicIncomeType(RWModel):
    economic_income_type_id: int
    name: str
    is_active: bool


class EconomicIncomeTypeInSelect(RWModel):
    economic_income_type_id: Optional[int] = None
