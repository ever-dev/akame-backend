from typing import Optional, Union, Any, List
from mamut.models.rwmodel import RWModel
from datetime import datetime
from mamut.models.customer import CustomerInResponse


class LoanInInsert(RWModel):
    loans_status_id: int
    start_date: datetime
    end_date: datetime
    amount_per_quote: float
    request_amount: float
    interest_total: float
    capital_amount: float
    total_amount: float
    persons_id: int
    loans_configuration_id: int
    interest_per_quote: float
    interest_amount_per_quote: float
    interest_amount_total: float
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[int]
    last_modified_date: Optional[datetime] = None
    is_text_for_credit_valid: Optional[bool]


class LoanInDb(LoanInInsert):
    last_modified_by_name: Optional[str]
    financial_loans_id: Optional[int]
    disbursement_date: Optional[datetime] = None
    paid_date: Optional[datetime] = None
    today_balance: Optional[float] = None
    days_since_start: Optional[int] = None
    supplier_id: Optional[int] = None
    supplier_name: Optional[str] = None
    supplier_account: Optional[str] = None
    supplier_account_bank_city_ach_code: Optional[str] = None
    supplier_account_bank_ach_code: Optional[str] = None
    supplier_account_bank_account_type_ach_code: Optional[str] = None
    supplier_identification: Optional[str] = None
    supplier_identification_ach_code: Optional[str] = None
    customer: Optional[CustomerInResponse] = None
    is_text_for_credit_valid: Optional[bool]


class LoanInCreate(RWModel):
    start_date: datetime
    term_in_days: int
    persons_id: int
    loans_configuration_id: int


class LoanInSelect(RWModel):
    financial_loans_id: Optional[int] = None
    loans_status_id: Optional[int] = None
    persons_id: Optional[int] = None
    disbursement_in_file: Optional[bool] = None
    disbursement_confirmed: Optional[bool] = None
    disbursement_completed: Optional[bool] = None
    for_today_balance_update: Optional[bool] = None


class LoanInUpdate(RWModel):
    financial_loans_id: int
    disbursement_in_file: Optional[bool] = None
    disbursement_confirmed: Optional[bool] = None
    disbursement_completed: Optional[bool] = None
    disbursement_status: Optional[str] = None
    disbursement_denied_reason: Optional[str] = None
    loans_status_id: Optional[int] = None
    disbursement_date: Optional[datetime] = None
    paid_date: Optional[datetime] = None
    today_balance: Optional[float] = None
    days_since_start: Optional[int] = None


class LoanInDelete(RWModel):
    financial_loans_id: int


class LoanInResponse(RWModel):
    financial_loans_id: Optional[int] = None
    loans_status_id: Optional[int] = None
    status_name: Optional[str]
    start_date: Optional[datetime] = None
    start_date_format: Optional[str] = None
    end_date: Optional[datetime] = None
    end_date_format: Optional[str] = None
    disbursement_date: Optional[datetime] = None
    disbursement_date_format: Optional[str] = None
    amount_per_quote: Optional[float] = None
    request_amount: Optional[float] = None
    request_amount_format: Optional[str] = None
    capital_amount: Optional[float] = None
    total_amount: Optional[float] = None 
    total_amount_format: Optional[str] = None 
    today_balance: Optional[float] = None 
    today_balance_format: Optional[str] = None     
    approved_date: Optional[datetime] = None
    approved_date_format: Optional[str] = None
    last_modified_date: Optional[datetime] = None
    last_modified_date_format: Optional[str] = None
    loans_configuration_id: Optional[int] = None
