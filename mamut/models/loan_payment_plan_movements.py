from typing import Optional, Union, Any
from mamut.models.rwmodel import RWModel
from datetime import datetime


class LoanPaymentPlanMovementInInsert(RWModel):
    loans_payment_plan_id: int
    amount: float
    interest: float
    capital: float
    mora: float
    total_balance: float
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[int]
    last_modified_date: Optional[datetime] = None
    payment_date: datetime
    description: Optional[str] = None
    total_capital: float
    payment_method_id: int
    ach_id: str
    alegra_id: str
    extra_payment: Optional[float] = None
    extra_info: Optional[Union[str, dict]] = None


class LoanPaymentPlanMovementInDb(LoanPaymentPlanMovementInInsert):
    last_modified_by_name: Optional[str]
    payment_plan_movements_id: Optional[int]


class LoanPaymentPlanMovementInSelect(RWModel):
    payment_plan_movements_id: Optional[int] = None
    loans_payment_plan_id: Optional[int] = None
    persons_id: Optional[int] = None
    ach_id: Optional[str] = None


class LoanPaymentPlanMovementInUpdate(RWModel):
    payment_plan_movements_id: int
    amount: Optional[float] = None
    interest: Optional[float] = None
    capital: Optional[float] = None
    mora: Optional[float] = None
    total_balance: Optional[float] = None
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[int]
    last_modified_date: Optional[datetime] = None
    payment_date: Optional[datetime] = None
    description: Optional[str] = None
    total_capital: Optional[float] = None
    payment_method_id: Optional[int] = None
    ach_id: Optional[str] = None
    alegra_id: Optional[str] = None
    fmsf_code: Optional[str] = None
