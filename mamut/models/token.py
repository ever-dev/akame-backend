from typing import Optional, Union
from mamut.models.rwmodel import RWModel
from datetime import datetime


class Token(RWModel):
    user_id: int
    profile_id: int
    name: str
