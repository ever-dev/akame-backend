from typing import Optional

from mamut.models.rwmodel import RWModel


class HelpContactForm(RWModel):
    fullName: Optional[str]
    email: Optional[str]
    message: Optional[str]
