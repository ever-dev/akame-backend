from pydantic import Json
from datetime import datetime
from mamut.models.rwmodel import RWModel
from typing import Optional, Union, List


class LoansConfigurationTerm(RWModel):
    term_in_days: int
    payment_days: List[int]


class LoansConfigurationInCreate(RWModel):
    tea_interest: float
    insurance: float
    fixed_insurance: float
    digital_services_per_day: float
    fixed_digital_services: float
    disbursement: float
    credit_validation: float
    promissory_note_and_custody: float
    endorsement: float
    collection: float
    vat: float
    min_amount: float
    max_amount: float
    step_amount: float
    min_days: int
    max_days: int
    term_configuration: Optional[Union[List[LoansConfigurationTerm], str]] = None
    tea_mora: Optional[float]
    supplier_id: int


class LoansConfigurationInInsert(LoansConfigurationInCreate):
    tem_interest: float
    ted_interest: float
    tem_mora: Optional[float]
    ted_mora: Optional[float]
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Union[int, str]
    last_modified_date: Optional[datetime] = None


class LoansConfigurationInDb(LoansConfigurationInInsert):
    loans_configuration_id: int


class LoansConfigurationInSelect(RWModel):
    loans_configuration_id: Optional[int] = None
    is_active: Optional[bool] = None
    supplier_id: Optional[int] = None


class LoansConfigurationInUpdate(LoansConfigurationInSelect):
    pass
