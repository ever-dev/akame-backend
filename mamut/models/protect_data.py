from typing import Optional, Union, Any
from mamut.models.rwmodel import RWModel


class ProtectDataOTPInSelect(RWModel):
    nip: Optional[str] = None
    email: Optional[str] = None
    phone_number: Optional[str] = None


class ProtectDataGetAudioInSelect(RWModel):
    audio_count: Optional[int] = None
    nip: Optional[str] = None
    file_base64: Optional[str] = None


class ProtectDataVerifyAudioInSelect(RWModel):
    nip: Optional[str] = None
    num1: Optional[int] = None
    num2: Optional[int] = None


class ProtectDataDocumentSignatureInSelect(RWModel):
    user_id: Optional[int] = None
    nip: Optional[str] = None
    name: Optional[str] = None
    middle_name: Optional[str] = None
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    telephone: Optional[str] = None
    email: Optional[str] = None
    departament_name: Optional[str] = None
    city_name: Optional[str] = None
    address: Optional[str] = None

    num1: Optional[int] = None
    num2: Optional[int] = None
    document_id: Optional[int] = None
    document_name: Optional[str] = None
    document: Optional[str] = None


class ProtectDataDataToSign(RWModel):
    id_firmante: Optional[int] = None
    nip: Optional[str] = None
    nombres: Optional[str] = None
    apellidos: Optional[str] = None
    correo: Optional[str] = None
    telefono: Optional[str] = None
    direccion: Optional[str] = None
    titulo: Optional[str] = None
    id_plantilla: Optional[int] = None
    firmar_con_una_toma: Optional[bool] = None
    guardar_documento_en_web: Optional[bool] = None
    descripcion: Optional[str] = None
    archivo: Optional[str] = None
