from typing import Optional, Union, Any
from mamut.models.rwmodel import RWModel
from datetime import date


class Employee(RWModel):
    job_name: str
    company_type_id: int
    job_start_date: date
    contract_type_id: int
    job_title_position: str
    monthly_income: float
    payment_frequency_id: int


class JobInSelect(RWModel):
    identification: Optional[str]
    persons_id: Optional[int]
    user_id: Optional[int]


class BusinessEstablishment(RWModel):
    dedication_business: str
    business_name: str
    business_start_date: date
    departament_id: int
    city_id: int
    address: str
    monthly_income: float
    employee_count: int
    affiliated_with_chamber_of_commerce: bool


class Freelancer(RWModel):
    activity_name: str
    work_in_enterprise: str
    enterprise_name: str
    enterprise_name_2: Optional[str]
    enterprise_name_3: Optional[str]
    job_start_date: date
    monthly_income: float


class Pensioner(RWModel):
    pension_fund_id: int
    pension_start_date: date
    monthly_income: float
    payment_frequency_id: int


class JobInUpdate(RWModel):
    identification: Optional[str]
    job_information: Optional[Union[Employee, BusinessEstablishment, Freelancer, Pensioner , str]] = None


class JobInResponse(RWModel):
    identification: str
    job_information: Optional[Union[Employee, BusinessEstablishment, Freelancer, Pensioner, str]] = None
