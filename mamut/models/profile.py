from typing import Optional, Union, Any
from mamut.models.rwmodel import RWModel
from datetime import datetime


class ProfileInInsert(RWModel):
    name: str
    description: str
    permissions: Any
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_date: Optional[datetime] = None


class ProfileInDb(ProfileInInsert):
    profile_id: int


class ProfileInCreate(RWModel):
    name: str
    description: str
    permissions: Any


class ProfileInSelect(RWModel):
    profile_id: Optional[int] = None
    name: Optional[str] = None


class ProfileInUpdate(RWModel):
    profile_id: int
    name: Optional[str] = None
    description: Optional[str] = None
    permissions: Optional[Any] = None
    is_active: Optional[bool] = None
    is_deleted: Optional[bool] = None
    last_modified_date: Optional[datetime] = None
