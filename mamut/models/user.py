from pydantic import EmailStr
from typing import Optional, Union, Any
from mamut.models.rwmodel import RWModel
from datetime import datetime


class UserInInsert(RWModel):
    profile_id: int
    name: str
    password: Optional[str] = None
    consecutive_failed_login_attempts: Optional[int] = 0
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_date: Optional[datetime] = None
    email: Optional[EmailStr] = None
    is_confirmed: Optional[bool] = False


class UserInDb(UserInInsert):
    permissions: Optional[Any] = None
    profile_name: Optional[str]
    user_id: int


class UserInCreate(RWModel):
    profile_id: int
    name: str
    password: str
    email: EmailStr


class UserInSelect(RWModel):
    user_id: Optional[int] = None
    profile_id: Optional[int] = None
    name: Optional[str] = None
    email: Optional[str] = None


class UserInUpdate(RWModel):
    user_id: int
    profile_id: Optional[int] = None
    name: Optional[str] = None
    password: Optional[str] = None
    consecutive_failed_login_attempts: Optional[int] = None
    is_active: Optional[bool] = None
    is_deleted: Optional[bool] = None
    last_modified_by: Optional[int] = None
    last_modified_date: Optional[datetime] = None
    email: Optional[EmailStr] = None
    is_confirmed: Optional[bool] = None


class UserTokenInCreate(RWModel):
    name: str
    password: str
    tfc: Optional[bool] = False


class UserTokenInSelect(RWModel):
    name: str
    password: str


class UserTokenInResponse(RWModel):
    token: str
    permissions: Optional[Any] = None
    seconds_when_generated: int
    seconds_until_expire: int
    need_retry_sign_process: bool = False


class SignupInCreate(RWModel):
    profile_id: Optional[int]
    profile_name: Optional[str]
    name: str
    password: str
    email: EmailStr
