from typing import Optional, Union, List
from mamut.models.rwmodel import RWModel
from datetime import datetime, date
from mamut.models.user import UserInUpdate


class PersonsInSelect(RWModel):
    persons_id: Optional[int]
    persons_id_greater_than: Optional[int]
    identification: Optional[str]
    rows_limit: Optional[int]


class PersonsInUpdate(RWModel):
    persons_id: int
    is_fraudulent: Optional[bool]
    modifier_user_id: Optional[int]
    last_modified_date: Optional[datetime]


class PersonWithUserInUpdate(RWModel):
    person: PersonsInUpdate
    user: UserInUpdate


class PersonsInResponse(RWModel):
    user_id: int
    persons_id: int
    identification: str
    name: str
    middle_name: str
    first_name: str
    last_name: str
    date_of_birth: Optional[date]
    is_fraudulent: bool
    is_active: bool
