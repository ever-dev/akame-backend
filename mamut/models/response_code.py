from typing import Optional, Any
from mamut.models.rwmodel import RWModel


class ResponseCode(RWModel):
    code: int
    code_name: str
    description: str
    is_error: bool
    exception: Optional[Any] = None
