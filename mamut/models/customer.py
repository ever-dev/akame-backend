from typing import Optional, Union, List
from mamut.models.rwmodel import RWModel
from datetime import datetime, date


class CustomerContactInformation(RWModel):
    telephone1: str
    cellphone: str
    customer_email: str
    close_person_name: str
    close_person_middle_name: str
    close_person_first_name: str
    close_person_last_name: str
    close_person_cellphone: str
    address: str
    facebook_account: str
    instagram_account: str
    twitter_account: str


class CustomerInSelect(RWModel):
    identification: Optional[str]
    persons_id: Optional[int]
    user_id: Optional[int]


class CustomerInInsert(RWModel):
    identification_type_id: int
    identification: str
    identification_expiry_date: Optional[date]
    persons_type_id: int
    person_status_id: int
    name: str
    middle_name: str
    first_name: str
    last_name: str
    date_of_birth: Optional[date]
    born_departament_id: int
    born_city_id: int
    current_departament_id: int
    current_city_id: int
    contact_information: Optional[Union[List[CustomerContactInformation], str]] = None
    bank_city_id: int
    bank_entity_id: int
    bank_account_type_id: int
    bank_account_number: str
    economic_income_type_id: int
    has_second_economic_income: bool
    economic_income_type_id_2: Optional[int]
    alegra_contact_id: Optional[int]
    transport_type_id: int
    vehicle_owner: Optional[bool]
    plate_number: Optional[str]
    approved_date: Optional[datetime]
    is_active: Optional[bool]
    is_deleted: Optional[bool]
    last_modified_by: Optional[Union[int, str]]
    last_modified_date: Optional[datetime]
    user_id: Optional[int]


class CustomerInUpdate(RWModel):
    identification_type_id: Optional[int]
    identification: Optional[str]
    identification_expiry_date: Optional[date]
    persons_type_id: Optional[int] = 2
    person_status_id: Optional[int] = 1
    name: Optional[str]
    middle_name: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    date_of_birth: Optional[date]
    born_departament_id: Optional[int]
    born_city_id: Optional[int]
    current_departament_id: Optional[int]
    current_city_id: Optional[int]
    contact_information: Optional[Union[List[CustomerContactInformation], str]] = None
    bank_city_id: Optional[int]
    bank_entity_id: Optional[int]
    bank_account_type_id: Optional[int]
    bank_account_number: Optional[str]
    economic_income_type_id: Optional[int]
    has_second_economic_income: Optional[bool]
    economic_income_type_id_2: Optional[int]
    alegra_contact_id: Optional[int]
    transport_type_id: Optional[int]
    vehicle_owner: Optional[bool]
    plate_number: Optional[str]
    approved_date: Optional[date]
    user_id: Optional[int]
    quota: Optional[float]
    quota_valid_until: Optional[datetime]
    is_evidente: Optional[datetime]


class CustomerInUpsert(RWModel):
    identification_type_id: Optional[int]
    identification: Optional[str]
    identification_expiry_date: Optional[date]
    persons_type_id: Optional[int] = 2
    person_status_id: Optional[int] = 1
    name: str
    middle_name: str
    first_name: str
    last_name: str
    date_of_birth: Optional[date]
    born_departament_id: int
    born_city_id: int
    current_departament_id: int
    current_city_id: int
    contact_information: Optional[Union[List[CustomerContactInformation], str]] = None
    bank_city_id: int
    bank_entity_id: int
    bank_account_type_id: int
    bank_account_number: str
    economic_income_type_id: int
    has_second_economic_income: bool
    economic_income_type_id_2: Optional[int]
    transport_type_id: int
    vehicle_owner: Optional[bool]
    plate_number: Optional[str]
    approved_date: Optional[date]
    user_id: Optional[int]
    is_active: Optional[bool] = True
    is_deleted: Optional[bool] = False
    last_modified_by: Optional[Union[int, str]]
    last_modified_date: Optional[datetime]


class CustomerInDb(CustomerInInsert):
    born_departament: Optional[str]
    born_city: Optional[str]
    current_departament: Optional[str]
    current_city: Optional[str]
    persons_id: Optional[str]


class CustomerInResponse(CustomerInUpdate):
    born_departament: Optional[str]
    born_city: Optional[str]
    current_departament: Optional[str]
    current_city: Optional[str]
    bank_ach_code: Optional[str]
    bank_city_ach_code: Optional[str]
    identification_ach_code: Optional[str]
    bank_account_type_ach: Optional[str]
    bank_departament_id: Optional[int]
    is_fraudulent: Optional[bool]
    persons_id: int
