from fastapi import FastAPI
from fastapi import APIRouter
from typing import List, Any
from mamut.core.utils import get_response
from mamut.api.router import router as api_router
from mamut.models.response_model import ResponseModel
from starlette.middleware.cors import CORSMiddleware
from fastapi.exceptions import RequestValidationError
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR
from mamut.core.config import API_PREFIX, DEBUG, PROJECT_NAME, VERSION
from starlette.exceptions import HTTPException as StarletteHTTPException
from mamut.conn.postgresql.postgresql import connect_pg_database, close_pg_connection
from mamut.conn.redis.redis import connect_redis_database, close_redis_connection
from mamut.crud.profile import load_profiles_in_redis


def get_application() -> FastAPI:
    application = FastAPI(title=PROJECT_NAME, debug=DEBUG, version=VERSION)

    application.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @application.on_event('startup')
    async def startup_event():
        await connect_pg_database()
        await connect_redis_database()
        await load_profiles_in_redis()

    @application.on_event('shutdown')
    async def shutdown_event():
        await close_pg_connection()
        await close_redis_connection()

    @application.exception_handler(StarletteHTTPException)
    async def custom_http_exception_handler(request, exc):
        return get_response(None, 'GENERAL_EXCEPTION', exc.detail, exc.status_code)

    @application.exception_handler(RequestValidationError)
    async def validation_exception_handler(request, exc):
        return get_response(exc.errors(), 'GENERAL_EXCEPTION', None, HTTP_500_INTERNAL_SERVER_ERROR)

    router = APIRouter()
    @router.get("/", response_model=ResponseModel[List[Any]], tags=['health'])
    async def api_health():
            return get_response('Welcome to the API.', 'OBTAINED_SUCCESSFULLY')


    application.include_router(router)
    application.include_router(api_router, prefix=API_PREFIX)

    return application


app = get_application()
