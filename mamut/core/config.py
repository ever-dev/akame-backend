import os
import sys
import logging
from typing import List
from loguru import logger
from databases import DatabaseURL
from starlette.config import Config
from mamut.core.logging import InterceptHandler
from starlette.datastructures import CommaSeparatedStrings, Secret


API_PREFIX = '/api'

JWT_TOKEN_PREFIX = 'Bearer'
VERSION = '0.0.1'

MAMUT_CONFIG_ENVIRONMENT: str = os.getenv('MAMUT_CONFIG_ENVIRONMENT', '')

if MAMUT_CONFIG_ENVIRONMENT != '':
    config = Config('.env.' + MAMUT_CONFIG_ENVIRONMENT)
else:
    raise Exception('You need to specify an environment with the `MAMUT_CONFIG_ENVIRONMENT` environment variable.') 

DEBUG: bool = config('DEBUG', cast=bool, default=False)
DATACREDITO_DEBUG: bool = config('DATACREDITO_DEBUG', cast=bool, default=False)

POSTGRESQL_URL: DatabaseURL = config('PGDB_URL', cast=DatabaseURL)
MIN_CONNECTIONS_COUNT: int = config('MIN_CONNECTIONS_COUNT', cast=int, default=10)
MAX_CONNECTIONS_COUNT: int = config('MAX_CONNECTIONS_COUNT', cast=int, default=10)

REDIS_URL: str = config('REDIS_URL')

SECRET_KEY: Secret = config('SECRET_KEY', cast=Secret)

PROJECT_NAME: str = config('PROJECT_NAME', default='Mamut CO Backend')

# SMTP Configuration
SMTP_HOST: str = config('SMTP_HOST')
SMTP_PORT: int = config('SMTP_PORT')
SMTP_TLS: bool = config('SMTP_TLS', cast=bool, default=False)
SMTP_USER: str = config('SMTP_USER')
SMTP_PASSWORD: str = config('SMTP_PASSWORD')
SMTP_FROM_NAME: str = config('SMTP_FROM_NAME')
SMTP_SSL: bool = config('SMTP_SSL', cast=bool, default=False)

TOKEN_MINUTES_EXPIRATION: int = config('TOKEN_MINUTES_EXPIRATION', cast=int)

PROTEC_DATA_WSDL: str = config('PROTEC_DATA_WSDL')
PROTEC_DATA_WSDL_SIGNATURE: str = config('PROTEC_DATA_WSDL_SIGNATURE')
PROTEC_DATA_ID_ENTERPRISE_CONSUME: str = config('PROTEC_DATA_ID_ENTERPRISE_CONSUME')
PROTEC_DATA_PASSWORD: str = config('PROTEC_DATA_PASSWORD')
PROTEC_DATA_PASSWORD_CONSUME: str = config('PROTEC_DATA_PASSWORD_CONSUME')
PROTEC_DATA_USER_CONSUME: str = config('PROTEC_DATA_USER_CONSUME')
PROTEC_DATA_ID_ENTERPRISE: str = config('PROTEC_DATA_ID_ENTERPRISE')

DATA_CREDITO_HC2_WSDL: str = config('DATA_CREDITO_HC2_WSDL')
DATA_CREDITO_EVIDENTE_WSDL: str = config('DATA_CREDITO_EVIDENTE_WSDL')
DATA_CREDITO_PASSWORD: str = config('DATA_CREDITO_PASSWORD')
DATA_CREDITO_PRODUCT_HC2: str = config('DATA_CREDITO_PRODUCT_HC2')
DATA_CREDITO_OKTA_USER: str = config('DATA_CREDITO_OKTA_USER')
DATA_CREDITO_OKTA_PASS: str = config('DATA_CREDITO_OKTA_PASS')
DATA_CREDITO_NIP: str = config('DATA_CREDITO_NIP')

ALEGRA_USER: str = config('ALEGRA_USER')
ALEGRA_TOKEN: str = config('ALEGRA_TOKEN')
ALEGRA_REST: str = config('ALEGRA_REST')
ALEGRA_COUNTRY: str = config('ALEGRA_COUNTRY')

ENTERPRISE_ID: str = config('ENTERPRISE_ID')
ENTERPRISE_NAME: str = config('ENTERPRISE_NAME')

METABASE_SITE_URL: str = config('METABASE_SITE_URL')
METABASE_SECRET_KEY: str = config('METABASE_SECRET_KEY')
METABASE_TOKEN_EXPIRATION_MINUTES: int = config('METABASE_TOKEN_EXPIRATION_MINUTES')
METABASE_SUPERUSER_EMAIL: str = config('METABASE_SUPERUSER_EMAIL')
METABASE_SUPERUSER_PASSWORD: str = config('METABASE_SUPERUSER_PASSWORD')

FRONTEND_BASE_URL: str = config('FRONTEND_BASE_URL')

LOAN_CONFIGURATION_CACHE_EXP_SECONDS: int = config('LOAN_CONFIGURATION_CACHE_EXP_SECONDS', cast=int)

TRIBBU_SYSTEMS_CERT: str = config('TRIBBU_SYSTEMS_CERT')

RABBIT_MQ_URL: str = config('RABBIT_MQ_URL')

APP_TIMEZONE: str = config('APP_TIMEZONE')

APP_STATIC_PASSWORD: str = config('APP_STATIC_PASSWORD')

MAMUTSOLUCIONES_SSL_CRT: str = config('MAMUTSOLUCIONES_SSL_CRT')
MAMUTSOLUCIONES_SSL_PUBLIC: str = config('MAMUTSOLUCIONES_SSL_PUBLIC')
MAMUTSOLUCIONES_SSL_KEY: str = config('MAMUTSOLUCIONES_SSL_KEY')

SOCKET_SERVER_NOTIFICATIONS_URL: str = config('SOCKET_SERVER_NOTIFICATIONS_URL')

# Logging configuration
LOGGING_LEVEL = logging.DEBUG if DEBUG else logging.INFO
logging.basicConfig(
    level=LOGGING_LEVEL
)
logger.configure(handlers=[{'sink': sys.stderr, 'level': LOGGING_LEVEL}])
