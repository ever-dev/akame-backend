codes = {
    'GENERAL_EXCEPTION': {
        'code': 999,
        'description': 'An error has occurred while processing your request.',
        'is_error': True
    },
    'OBTAINED_SUCCESSFULLY': {
        'code': 1000,
        'description': 'The data was obtained successfully.',
        'is_error': False
    },
    'OBTAINED_UNSUCCESSFULLY': {
        'code': 1001,
        'description': 'An error has ocurred while obtaining the data.',
        'is_error': True
    },
    'INSERTED_SUCCESSFULLY': {
        'code': 1002,
        'description': 'The data was inserted successfully.',
        'is_error': False
    },
    'INSERTED_UNSUCCESSFULLY': {
        'code': 1003,
        'description': 'An error has ocurred while inserting the data.',
        'is_error': True
    },
    'UPDATED_SUCCESSFULLY': {
        'code': 1004,
        'description': 'The data was updated successfully.',
        'is_error': False
    },
    'UPDATED_UNSUCCESSFULLY': {
        'code': 1005,
        'description': 'An error has ocurred while updating the data.',
        'is_error': True
    },
    'DELETED_SUCCESSFULLY': {
        'code': 1006,
        'description': 'The data was deleted successfully.',
        'is_error': False
    },
    'DELETED_UNSUCCESSFULLY': {
        'code': 1007,
        'description': 'An error has ocurred while deleting the data.',
        'is_error': True
    },
    'INVALID_OPERATION': {
        'code': 1010,
        'description': 'Invalid operation.',
        'is_error': True
    },
    'ALREADY_EXISTS': {
        'code': 1011,
        'description': 'The registry already exists.',
        'is_error': True
    },
    'NOT_ENOUGH_PERMISSIONS': {
        'code': 1025,
        'description': 'You don\'t have enough permissions to access this resource.',
        'is_error': True
    },
    'EXPIRED_TOKEN': {
        'code': 4010,
        'description': 'The token is expired.',
        'is_error': True
    },
    'INVALID_TOKEN': {
        'code': 4011,
        'description': 'The token is invalid.',
        'is_error': True
    },
    'REVOKED_TOKEN': {
        'code': 4012,
        'description': 'The token has been revoked.',
        'is_error': True
    },
    'NO_AUTH_HEADER': {
        'code': 4013,
        'description': 'Authorization header is not present.',
        'is_error': True
    },
    'TOKEN_GENERATED': {
        'code': 4014,
        'description': 'The token has been generated.',
        'is_error': False
    },
    'TOKEN_RENEWED': {
        'code': 4015,
        'description': 'The token has been renewed.',
        'is_error': False
    },
    'TOKEN_DELETED': {
        'code': 4016,
        'description': 'The token has been deleted.',
        'is_error': False
    },
    'INVALID_CREDENTIALS': {
        'code': 4017,
        'description': 'Invalid credentials.',
        'is_error': True
    },
    'NOT_AUTHORIZED': {
        'code': 4018,
        'description': 'Not authorized.',
        'is_error': True
    },
    'QUOTA_EXCEEDED': {
        'code': 4019,
        'description': 'Quota exceeded.',
        'is_error': False
    },
    'DATACREDITO_REJECTED': {
        'code': 4020,
        'description': 'Datacredito rejected.',
        'is_error': False
    },
    'REJECTED_BY_TIME': {
        'code': 4021,
        'description': 'Rejected by time.',
        'is_error': False
    },
    'REJECTED_BY_EVIDENTE': {
        'code': 4022,
        'description': 'Rejected by evidente.',
        'is_error': False
    }, 
    'MIN_QUOTA_EXCEEDED': {
        'code': 4023,
        'description': 'Min Quota exceeded.',
        'is_error': False
    }
}
