import jwt
import json
import operator
from functools import reduce
from typing import List
from jwt import PyJWTError
from fastapi import Depends, Header
from mamut.models.token import Token
from datetime import datetime, timedelta
from mamut.core.config import SECRET_KEY, TOKEN_MINUTES_EXPIRATION
from starlette.exceptions import HTTPException
from starlette.status import HTTP_401_UNAUTHORIZED
from mamut.crud.profile import REDIS_PROFILE_PREFIX
from mamut.conn.redis.redis_utils import get_redis_database

ALGORITHM = "HS256"


def create_access_token(*, data: dict, expires_delta: timedelta = None) -> bytes:
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=TOKEN_MINUTES_EXPIRATION)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, str(SECRET_KEY), algorithm=ALGORITHM)
    return encoded_jwt


class CheckToken:
    def __init__(self, *, permissions: List[str] = list(), operator: str = 'and'):
        self.permissions = permissions
        self.operator = operator

    def check_token(self, authorization: str) -> Token:
        token_decoded: Token = None
        token_prefix, token = authorization.split(" ")
        if token_prefix != 'Bearer':
            raise HTTPException(
                status_code=HTTP_401_UNAUTHORIZED, detail="Invalid authorization type."
            )

        try:
            payload = jwt.decode(token, str(SECRET_KEY), algorithms=[ALGORITHM])
            token_decoded = Token(**payload)
        except PyJWTError:
            raise HTTPException(
                status_code=HTTP_401_UNAUTHORIZED, detail="Could not validate credentials."
            )

        return token_decoded

    async def check_permissions(self, token_decoded: Token):
        if len(self.permissions) > 0:
            rconn = await get_redis_database() # Get redis connection
            current_profile_permissions = await rconn.get(REDIS_PROFILE_PREFIX + str(token_decoded.profile_id)) # Get permissions for the current profile in the token

            if current_profile_permissions is None: # Raise an exception if the token is not found in redis
                raise HTTPException(
                    status_code=HTTP_401_UNAUTHORIZED, detail="Profile is not available in redis."
                )
            
            try:
                current_profile_permissions = json.loads(current_profile_permissions)
            except json.decoder.JSONDecodeError:
                raise HTTPException(
                    status_code=HTTP_401_UNAUTHORIZED, detail=f"The permissions stored in redis of the key `{REDIS_PROFILE_PREFIX + str(token_decoded.profile_id)}` could not be decoded."
                )
            
            has_permission = False
            for permit in self.permissions:
                permit_parts = permit.split('.')
                permit_value = False
                try:
                    permit_value = reduce(operator.getitem, permit_parts, current_profile_permissions)
                    if not isinstance(permit_value, bool):
                        raise TypeError
                    has_permission = permit_value
                except KeyError:
                    if self.operator == 'and':
                        has_permission = False
                        break
                except TypeError:
                    raise HTTPException(
                        status_code=HTTP_401_UNAUTHORIZED, detail=f"A specified permit is not a boolean attribute."
                    )

                if self.operator == 'or' and permit_value:
                    break

            if not has_permission:
                raise HTTPException(
                    status_code=HTTP_401_UNAUTHORIZED, detail="Not enough permissions."
                )

    async def __call__(self, *, authorization: str = Header(...)) -> Token:
        token_decoded: Token = self.check_token(authorization)
        await self.check_permissions(token_decoded)

        return token_decoded
