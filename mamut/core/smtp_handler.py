import asyncio
import aiosmtplib
from typing import List
from email.mime.text import MIMEText
from mamut.models.rwmodel import RWModel
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from mamut.core.config import SMTP_HOST, SMTP_PORT, SMTP_TLS, SMTP_USER, SMTP_PASSWORD, SMTP_FROM_NAME, SMTP_SSL


class EmailFile(RWModel):
    path: str
    name: str


class SMTPHandler:
    def __init__(self):
        self.smtp = aiosmtplib.SMTP(hostname=SMTP_HOST, port=SMTP_PORT, use_tls=SMTP_TLS or SMTP_SSL)

    async def __connect(self):
        await self.smtp.connect()
        if SMTP_TLS:
            await self.smtp.starttls()
        
        await self.smtp.login(SMTP_USER, SMTP_PASSWORD)

    async def send_html_email(self, to, subject, html, files: List[EmailFile]=[]):
        await self.__connect()

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = SMTP_FROM_NAME + '<' + SMTP_USER + '>'
        msg['To'] = ', '.join(to)

        self.__attach_files(msg, files)

        msg.attach(MIMEText(html, 'html', 'utf-8'))

        await self.smtp.sendmail(SMTP_USER, to, msg.as_string().encode('utf-8'))

        await self.__disconnect()

    async def send_email(self, to, subject, message):
        await self.__connect()
        cc = [to]
        contact_mail: str = "contacto@mamutsoluciones.co"

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = SMTP_FROM_NAME + '<' + SMTP_USER + '>'
        msg['To'] = contact_mail
        msg['CC'] = ', '.join(cc)

        msg.attach(MIMEText(message))

        await self.smtp.sendmail(SMTP_USER, [contact_mail] + cc, msg.as_string().encode('utf-8'))

        await self.__disconnect()

    def __attach_files(self, msg: MIMEMultipart, files: List[EmailFile]=[]):
        for f in files:
            with open(f.path, 'rb') as file_handler:
                part = MIMEApplication(
                    file_handler.read(),
                    Name=f.name
                )
                part.add_header('Content-Disposition', f'attachment; filename="{f.name}"')
                part.add_header('Content-ID', '<' + f.name + '>')
                msg.attach(part)

    async def __disconnect(self):
        await self.smtp.quit()
