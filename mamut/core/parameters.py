parameter = {
    'ID': 'ID',
    'API': 'API',
    'NIP': 'NIP',
    'PD_ID': 'IDEmpresa',
    'PD_EMAIL': 'Correo',
    'PD_CELLPHONE': 'Celular',
    'PD_RT': 'RT',
    'PD_FILE_BASE64': 'FileBase64',
    'PD_NUM1': 'Numero1',
    'PD_NUM2': 'Numero2',
    'PD_PERSONAL_INFO': 'DatosPersona',
    'PD_GENERATE_PHONE_CALL': 'GenerarLlamada',
    'PD_USER_CONSUME': 'UsrConsumo',
    'PD_TRANSACTION_ID': 'IDTransaccion'
}

